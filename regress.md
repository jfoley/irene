## Robust Performance Comparison: QL vs. SDM

Sig-Test: randomized, threshold set to 0.050000

    # Galago System
    run-id            map           ndcg20        P20  
    Galago-QL         0.252         0.414         0.364    
    Galago-SDM        0.259  *      0.418         0.370    

    # Irene System
    run-id            map           ndcg20        P20  
    Irene-QL          0.253         0.415         0.367    
    Irene-SDM         0.259  *      0.419         0.373    

