const history = History.useQueries(History.createHistory)();

function postJSON(path, input, onDone, onError) {
    let request = new XMLHttpRequest();
    request.open('POST', path, true);
    request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    request.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status >= 200 && this.status < 400) {
                onDone(JSON.parse(this.responseText));
            } else {
                onError(this);
            }
        }
    };
    request.send(JSON.stringify(input));
}

class SearchEngine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            q: this.props.getP.q,
            n: 10,
            off: 0,
            results: null,
            waiting: false
        }
    }
    componentDidMount() {
        this.submitSearch();
    }
    submitSearch() {
        let results = this.state.results;
        let q = this.state.q;
        if(!q) return;
        if(results !== null && results.q === q) {
            return;
        }
        
        this.setState({waiting: true});
        postJSON(serverURL+"/search", {q: this.state.q}, succ => {
            console.log(succ);
            this.setState({waiting: false, results: succ});
        }, err => {
            this.setState({waiting: false, error: err});
        });
        console.log("Search");
    }
    renderResults() {
        let results = this.state.results;
        
        return <table>
            <thead>{this.renderResultHeader()}</thead>
            <tbody>{
                _.map(results.docs, (doc) => {
                    return this.renderResult(doc);
                })
            }</tbody>
        </table>
    }
    renderResultHeader() {
        return <tr>
            <th>Rank</th>
            <th>Day</th>
            <th>Mon</th>
            <th>Year</th>
            <th>Title</th>
        </tr>
    }
    renderResult(docP) {
        let mos = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"]
        let date = new Date(docP.storedTimeStamp*1000);
        return <tr key={docP.id}>
            <td>{docP.rank}</td>
            <td>{mos[date.getUTCMonth()]}</td>
            <td>{date.getUTCDay()+1}</td>
            <td>{date.getUTCFullYear()}</td>
            <td><div>
                <label className="subject"><b>{docP.title}</b></label><br />
                <p>{docP.body}</p>
            </div></td>
        </tr>
    }
    renderQueryBox() {
        return <div className="queryBox">
            <input type="text"
                   disabled={this.state.waiting}
                   value={this.state.q}
                   onChange={evt => this.setState({q: evt.target.value})}
                   onKeyPress={evt => {if(evt.which == 13) this.submitSearch();} }
                   size="30" />
            <input type="button"
                   disabled={this.state.waiting}
                   onClick={evt => this.submitSearch()}
                   value="Search"/>
        </div>
    }
    render() {
        let results = this.state.results;
        if(results == null) {
            return <div className="landingPage">
                <h1>Search Engine</h1>
                {this.renderQueryBox()}
            </div>;
        }
        
        return <div>
            <div className="topQueryBox">{this.renderQueryBox()}</div>
            <div className="results">{this.renderResults()}</div>
        </div>;
    }
}

function onLocation(loc) {
    console.log("History.listen", loc);
    let rc = document.getElementById("container");
    ReactDOM.render(<SearchEngine path={loc.pathname} getP={loc.query}/>, rc);
}

history.listen(onLocation);
onLocation(history.getCurrentLocation());

