package edu.umass.cs.jfoley.irene.expr.macro;

import edu.umass.cs.jfoley.irene.expr.IreneQueryLanguage;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.junit.Assert;
import org.junit.Test;
import org.lemurproject.galago.utility.Parameters;

/**
 * @author jfoley
 */
public class WeightMacroChainTest {
  IreneQueryLanguage lang = new IreneQueryLanguage(null, Parameters.create());

  @Test
  public void testWeightParams() {
    float[] exp = new float[] {.5f, .3f, .2f};
    float eps = 0.00001f;
    QExpr newMethod = new QExpr("combine")
        .addChild(QExpr.text("a").setWeight(5))
        .addChild(QExpr.text("b").setWeight(3))
        .addChild(QExpr.text("c").setWeight(2));

    QExpr xf = lang.applyMacros(newMethod);
    //System.out.println(Arrays.toString(xf.getTransformedWeightVector()));
    Assert.assertArrayEquals(exp, xf.getTransformedWeightVector(), eps);

    QExpr oldMethod = new QExpr("combine")
        .setConfig("0", 5.0, "1", 3.0, "2", 2.0)
        .addChild(QExpr.text("a"))
        .addChild(QExpr.text("b"))
        .addChild(QExpr.text("c"));

    Assert.assertArrayEquals(exp, lang.applyMacros(oldMethod).getTransformedWeightVector(), eps);

    try {
      QExpr bothMethodsIllegal = new QExpr("combine")
          .setConfig("0", 5.0, "1", 3.0, "2", 2.0)
          .addChild(QExpr.text("a").setWeight(5))
          .addChild(QExpr.text("b").setWeight(3))
          .addChild(QExpr.text("c").setWeight(2));
      lang.applyMacros(bothMethodsIllegal);
      Assert.fail("Should get an error for over-specifying weights...");
    } catch (Exception e) {
      // Pass test.
    }
  }

  @Test
  public void testFlatten() {
    float[] exp = new float[] {.5f, .3f, .2f};
    float eps = 0.00001f;
    QExpr newMethod = new QExpr("combine")
        .addChild(QExpr.text("a"))
        .addChild(new QExpr("combine")
            .addChild(QExpr.text("b").setWeight(6))
            .addChild(QExpr.text("c").setWeight(4)));

    QExpr xf = lang.applyMacros(newMethod);
    //System.out.println(Arrays.toString(xf.getTransformedWeightVector()));
    Assert.assertArrayEquals(exp, xf.getTransformedWeightVector(), eps);
  }

  @Test
  public void testFlatten2() {
    float[] exp = new float[] {.75f, .125f, .125f};
    float eps = 0.00001f;
    QExpr newMethod = new QExpr("combine")
        .addChild(QExpr.text("a").setWeight(3))
        .addChild(new QExpr("combine")
            .addChild(QExpr.text("b").setWeight(4))
            .addChild(QExpr.text("c").setWeight(4)));

    QExpr xf = lang.applyMacros(newMethod);
    //System.out.println(Arrays.toString(xf.getTransformedWeightVector()));
    Assert.assertArrayEquals(exp, xf.getTransformedWeightVector(), eps);
  }

  @Test
  public void testFlattenRepeatedly() {
    float[] exp = new float[] {.75f, .125f, .125f};
    float eps = 0.00001f;
    QExpr newMethod = new QExpr("combine")
        .addChild(QExpr.text("a").setWeight(3))
        .addChild(new QExpr("combine")
            .addChild(QExpr.text("b").setWeight(4))
            .addChild(QExpr.text("c").setWeight(4)));

    QExpr xf = lang.applyMacros(lang.applyMacros(newMethod));
    //System.out.println(Arrays.toString(xf.getTransformedWeightVector()));
    Assert.assertArrayEquals(exp, xf.getTransformedWeightVector(), eps);
  }

}