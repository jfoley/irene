package edu.umass.cs.jfoley.irene.expr.macro;

import edu.umass.cs.jfoley.irene.expr.IreneQueryLanguage;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.junit.Test;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author jfoley
 */
public class SDMMacroTest {
  @Test
  public void testSDMMacro() throws IOException {
    QExpr sdmQ = new QExpr("sdm")
        .addChild(new QExpr("text", "a"))
        .addChild(new QExpr("text", "b"))
        .addChild(new QExpr("text", "c"));

    QExpr expected = new QExpr("combine")
        .setConfig("0", 0.8, "1", 0.15, "2", 0.05)
        .addChild(new QExpr("combine")
            .addChild(new QExpr("text", "a"))
            .addChild(new QExpr("text", "b"))
            .addChild(new QExpr("text", "c")))
        .addChild(new QExpr("combine")
            .addChild(new QExpr("od")
                .setConfig("width", 1)
                .addChild(new QExpr("text", "a"))
                .addChild(new QExpr("text", "b")))
            .addChild(new QExpr("od")
                .setConfig("width", 1)
                .addChild(new QExpr("text", "b"))
                .addChild(new QExpr("text", "c"))))
        .addChild(new QExpr("combine")
            .addChild(new QExpr("uw")
                .setConfig("width", 8)
                .addChild(new QExpr("text", "a"))
                .addChild(new QExpr("text", "b")))
            .addChild(new QExpr("uw")
                .setConfig("width", 8)
                .addChild(new QExpr("text", "b"))
                .addChild(new QExpr("text", "c")))
        );


    SDMMacro macro = new SDMMacro(Parameters.create());
    macro.setFactory(new IreneQueryLanguage());
    QExpr expandedSDM = macro.transform(sdmQ);
    assertNotNull(expandedSDM);

    assertEquals(expected.toJSON().toPrettyString(), expandedSDM.toJSON().toPrettyString());
  }

}