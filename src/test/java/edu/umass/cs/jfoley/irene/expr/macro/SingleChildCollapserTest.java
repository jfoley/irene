package edu.umass.cs.jfoley.irene.expr.macro;

import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.expr.IreneQueryModel;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import edu.umass.cs.jfoley.irene.index.IreneIndexer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.store.RAMDirectory;
import org.junit.Test;

import java.io.IOException;

import static edu.umass.cs.jfoley.irene.eval.bool.BoolEvalTest.shortDoc;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author jfoley
 */
public class SingleChildCollapserTest {

  @Test
  public void testSimple() throws IOException {
    try (IreneIndexer indexer = new IreneIndexer(new RAMDirectory(), false, new WhitespaceAnalyzer())) {
      shortDoc(indexer, "doc0", "a b c");
      indexer.commit();
      final IreneIndex fakeIndex = indexer.open();

      QExpr syn = new QExpr("syn")
          .addChild(QExpr.fieldMatch("text", "hello"));

      final QExpr qExpr = fakeIndex.transform(syn);
      assertEquals(QExpr.fieldMatch("text", "hello"), qExpr);
    }
  }

  @Test
  public void testTwoLayer() throws IOException {
    try (IreneIndexer indexer = new IreneIndexer(new RAMDirectory(), false, new WhitespaceAnalyzer())) {
      shortDoc(indexer, "doc0", "a b c");
      indexer.commit();
      final IreneIndex fakeIndex = indexer.open();

      QExpr syn = new QExpr("od")
          .addChild(new QExpr("syn")
              .addChild(QExpr.fieldMatch("text", "hello")));

      final QExpr qExpr = fakeIndex.transform(syn);
      assertEquals(QExpr.fieldMatch("text", "hello"), qExpr);
    }
  }

  @Test
  public void testTwoLayer2() throws IOException {
    try (IreneIndexer indexer = new IreneIndexer(new RAMDirectory(), false, new WhitespaceAnalyzer())) {
      shortDoc(indexer, "doc0", "a b c");
      indexer.commit();
      final IreneIndex fakeIndex = indexer.open();

      // look for the phrase "hello (world|universe)"
      QExpr syn = new QExpr("od")
          .addChild(new QExpr("syn")
              .addChild(QExpr.fieldMatch("text", "hello")))
          .addChild(new QExpr("syn")
              .addChild(QExpr.fieldMatch("text", "world"))
              .addChild(QExpr.fieldMatch("text", "universe"))
          );

      QExpr expected = new QExpr("od")
          .addChild(QExpr.fieldMatch("text", "hello"))
          .addChild(new QExpr("syn")
              .addChild(QExpr.fieldMatch("text", "world"))
              .addChild(QExpr.fieldMatch("text", "universe"))
          );

      final QExpr qExpr = fakeIndex.transform(syn.copy());
      assertEquals(expected, qExpr);
      final IreneQueryModel exprModel = fakeIndex.prepare(syn.copy());
      assertNotNull(exprModel);
    }
  }
}