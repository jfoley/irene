package edu.umass.cs.jfoley.irene.eval.leaf;

import edu.umass.cs.jfoley.irene.CommonTestIndex;
import edu.umass.cs.jfoley.irene.CommonTestIndexResource;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.junit.ClassRule;
import org.junit.Test;
import org.lemurproject.galago.core.retrieval.Results;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * @author jfoley
 */
public class IreneOrderedWindowTest {
  @ClassRule
  public static CommonTestIndexResource resource = new CommonTestIndexResource();

  @Test
  public void testPhraseCalculation() throws IOException {
    try (CommonTestIndex tmp = new CommonTestIndex()) {

      QExpr phrase = QExpr.create("od");
      phrase.children.addAll(Arrays.asList(
          new QExpr("text", "brown"),
          new QExpr("text", "fox")
      ));

      TopDocs search = tmp.index.search(phrase, 3);
      int count = tmp.index.count(phrase);

      /*for (int i = 0; i < search.scoreDocs.length; i++) {
        System.out.println(search.scoreDocs[i].toString());
      }*/
      // TODO: infer movement
      assertEquals(1, count);
      assertEquals(1, search.totalHits);
      assertEquals(1.0, search.scoreDocs[0].score, 0.00001);
    }
  }

  @Test
  public void testPhraseCalcSharing() throws IOException {
    CommonTestIndex tmp = resource.getIndex();

    QExpr phrase = QExpr.create("od")
        .addChild(new QExpr("text", "brown"))
        .addChild(new QExpr("text", "fox"));

    QExpr phrase2 = QExpr.create("od")
        .addChild(new QExpr("text", "brown"))
        .addChild(new QExpr("text", "dog"));

    QExpr countSum = QExpr.create("countsum")
        .addChild(phrase)
        .addChild(phrase2);

    TopDocs search = tmp.index.search(countSum, 5);
    int count = tmp.index.count(countSum);
    assertEquals(2, count);
    assertEquals(2, search.totalHits);
  }

  @Test
  public void testPhraseStats() {
    CommonTestIndex tmp = resource.getIndex();

    QExpr dirichletPhrase = QExpr.create("dirichlet")
        .addChild(QExpr.create("od")
            .addChild(new QExpr("text", "brown"))
            .addChild(new QExpr("text", "fox")));

    Results gResults = tmp.galago.transformAndExecuteQuery(StructuredQuery.parse("#dirichlet(#od:1(brown fox))"), Parameters.parseArray("requested", 5));

    TopDocs search = tmp.index.search(dirichletPhrase, 5);
    int count = tmp.index.count(dirichletPhrase);
    assertEquals(1, count);
    assertEquals(1, search.totalHits);

    assertEquals(1, gResults.scoredDocuments.size());
    for (int i = 0; i < gResults.scoredDocuments.size(); i++) {
      ScoredDocument gdoc = gResults.scoredDocuments.get(i);
      ScoreDoc ldoc = search.scoreDocs[i];
      assertEquals(ldoc.score, gdoc.score, 0.00001);
    }
  }

}