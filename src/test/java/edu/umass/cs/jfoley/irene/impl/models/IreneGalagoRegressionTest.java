package edu.umass.cs.jfoley.irene.impl.models;

import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.collections.util.ListFns;
import edu.umass.cs.jfoley.irene.CommonTestIndex;
import edu.umass.cs.jfoley.irene.CommonTestIndexResource;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.junit.ClassRule;
import org.junit.Test;
import org.lemurproject.galago.core.retrieval.Results;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @author jfoley
 */
public class IreneGalagoRegressionTest {
  @ClassRule
  public static CommonTestIndexResource resource = new CommonTestIndexResource();

  // Test standard query likelihood
  @Test
  public void exprQLTest() throws IOException {
    CommonTestIndex testIndex = resource.getIndex();

    for (Pair<String, String> kv : ListFns.pairs(new ArrayList<>(testIndex.getTerms()))) {
      Node galagoQ = new Node("combine");
      galagoQ.add(Node.Text(kv.left));
      galagoQ.add(Node.Text(kv.right));

      QExpr node = new QExpr("combine");
      node.children.add(new QExpr("dirichlet", new QExpr("text", kv.left)));
      node.children.add(new QExpr("dirichlet", new QExpr("text", kv.right)));

      TopDocs search = testIndex.index.search(node, testIndex.getNumDocuments());
      Map<String, Double> docF = testIndex.galago.transformAndExecuteQuery(galagoQ).asDocumentFeatures();

      assertEquals(search.totalHits, docF.size());

      for (int i = 0; i < search.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = search.scoreDocs[i];
        //System.out.println(testIndex.index.explain(node, scoreDoc.doc));
        assertEquals(docF.get(testIndex.name(scoreDoc.doc)), scoreDoc.score, 0.0001);
      }

      QExpr fnode = new QExpr("dql");
      fnode.addChild(new QExpr("text", kv.left));
      fnode.addChild(new QExpr("text", kv.right));
      TopDocs fsearch = testIndex.index.search(node, testIndex.getNumDocuments());
      for (int i = 0; i < fsearch.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = fsearch.scoreDocs[i];
        //System.out.println(testIndex.index.explain(node, scoreDoc.doc));
        assertEquals(docF.get(testIndex.name(scoreDoc.doc)), scoreDoc.score, 0.0001);
      }

      // Test MaxScore-equiv:
      TopDocs sfsearch = testIndex.index.smartSearch(node, testIndex.getNumDocuments());
      for (int i = 0; i < sfsearch.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = sfsearch.scoreDocs[i];
        assertEquals(docF.get(testIndex.name(scoreDoc.doc)), scoreDoc.score, 0.0001);
      }
    }
  }

  // Test Query Likelihood with weights:
  @Test
  public void exprQLWTest() throws IOException {
    CommonTestIndex testIndex = resource.getIndex();

    for (Pair<String, String> kv : ListFns.pairs(new ArrayList<>(testIndex.getTerms()))) {
      Node galagoQ = new Node("combine");
      galagoQ.getNodeParameters().set("0", 7.0);
      galagoQ.getNodeParameters().set("1", 3.0);
      galagoQ.add(Node.Text(kv.left));
      galagoQ.add(Node.Text(kv.right));

      QExpr node = new QExpr("combine");
      node.setConfig("0", 7.0, "1", 3.0);
      node.children.add(new QExpr("dirichlet", new QExpr("text", kv.left)));
      node.children.add(new QExpr("dirichlet", new QExpr("text", kv.right)));

      TopDocs search = testIndex.index.search(node, testIndex.getNumDocuments());
      Map<String, Double> docF = testIndex.galago.transformAndExecuteQuery(galagoQ).asDocumentFeatures();

      assertEquals(search.totalHits, docF.size());

      for (int i = 0; i < search.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = search.scoreDocs[i];
        //System.out.println(testIndex.index.explain(node, scoreDoc.doc));
        assertEquals(docF.get(testIndex.name(scoreDoc.doc)), scoreDoc.score, 0.0001);
      }

      QExpr fnode = new QExpr("dql");
      fnode.setConfig("0", 7.0, "1", 3.0);
      fnode.addChild(new QExpr("text", kv.left));
      fnode.addChild(new QExpr("text", kv.right));
      TopDocs fsearch = testIndex.index.search(node, testIndex.getNumDocuments());
      for (int i = 0; i < fsearch.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = fsearch.scoreDocs[i];
        //System.out.println(testIndex.index.explain(node, scoreDoc.doc));
        assertEquals(docF.get(testIndex.name(scoreDoc.doc)), scoreDoc.score, 0.0001);
      }

      // Test MaxScore-equiv:
      TopDocs sfsearch = testIndex.index.smartSearch(node, testIndex.getNumDocuments());
      for (int i = 0; i < sfsearch.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = sfsearch.scoreDocs[i];
        assertEquals(docF.get(testIndex.name(scoreDoc.doc)), scoreDoc.score, 0.0001);
      }
    }
  }

  // Test scoring of all docs no matter what:
  @Test
  public void exprQLAllTest() throws IOException {
    CommonTestIndex testIndex = resource.getIndex();

    for (Pair<String, String> kv : ListFns.pairs(new ArrayList<>(testIndex.getTerms()))) {
      Parameters gqp = Parameters.create();
      gqp.put("working", testIndex.allNames());
      Node galagoQ = new Node("combine");
      galagoQ.add(Node.Text(kv.left));
      galagoQ.add(Node.Text(kv.right));

      QExpr node = new QExpr("combine");
      node.config.put("scoreAll", true);
      node.children.add(new QExpr("dirichlet", new QExpr("text", kv.left)));
      node.children.add(new QExpr("dirichlet", new QExpr("text", kv.right)));


      TopDocs search = testIndex.index.search(node, testIndex.getNumDocuments());

      final Results results = testIndex.galago.transformAndExecuteQuery(galagoQ, gqp);
      Map<String, Double> docF = results.asDocumentFeatures();

      assertEquals(docF.size(), search.totalHits);

      for (int i = 0; i < search.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = search.scoreDocs[i];
        //System.out.println(testIndex.index.explain(node, scoreDoc.doc));
        assertEquals(docF.get(testIndex.name(scoreDoc.doc)), scoreDoc.score, 0.0001);
      }

      QExpr fnode = new QExpr("dql");
      fnode.config.set("scoreAll", true);
      fnode.addChild(new QExpr("text", kv.left));
      fnode.addChild(new QExpr("text", kv.right));
      TopDocs fsearch = testIndex.index.search(node, testIndex.getNumDocuments());
      for (int i = 0; i < fsearch.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = fsearch.scoreDocs[i];
        //System.out.println(testIndex.index.explain(node, scoreDoc.doc));
        assertEquals(docF.get(testIndex.name(scoreDoc.doc)), scoreDoc.score, 0.0001);
      }

    }
  }

  @Test
  public void exprSDMTest() throws IOException {
    CommonTestIndex testIndex = resource.getIndex();

    for (Pair<String, String> kv : ListFns.pairs(new ArrayList<>(testIndex.getTerms()))) {
      Node galagoQ = new Node("sdm");
      galagoQ.add(Node.Text(kv.left));
      galagoQ.add(Node.Text(kv.right));

      QExpr node = new QExpr("sdm");
      node.children.add(new QExpr("text", kv.left));
      node.children.add(new QExpr("text", kv.right));

      TopDocs search = testIndex.index.search(node, testIndex.getNumDocuments());

      Map<String, Double> docF = testIndex.galago.transformAndExecuteQuery(galagoQ).asDocumentFeatures();

      assertEquals(search.totalHits, docF.size());

      for (int i = 0; i < search.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = search.scoreDocs[i];
        //System.out.println(testIndex.index.explain(node, scoreDoc.doc));
        assertEquals(docF.get(testIndex.name(scoreDoc.doc)), scoreDoc.score, 0.0001);
      }
    }
  }


  // Test query likelihood with linear smoothing
  @Test
  public void exprQLLinearTest() throws IOException {
    CommonTestIndex testIndex = resource.getIndex();

    double lambda = 0.9;
    for (Pair<String, String> kv : ListFns.pairs(new ArrayList<>(testIndex.getTerms()))) {
      Node galagoQ = new Node("combine");
      galagoQ.add(Node.Text(kv.left));
      galagoQ.add(Node.Text(kv.right));
      Parameters qp = Parameters.create();
      qp.put("scorer", "linear");
      qp.put("lambda", lambda);

      QExpr node = new QExpr("combine");
      node.children.add(new QExpr("linear", new QExpr("text", kv.left)).setConfig("lambda", lambda));
      node.children.add(new QExpr("linear", new QExpr("text", kv.right)).setConfig("lambda", lambda));

      TopDocs search = testIndex.index.search(node, testIndex.getNumDocuments());
      Map<String, Double> docF = testIndex.galago.transformAndExecuteQuery(galagoQ, qp).asDocumentFeatures();

      assertEquals(search.totalHits, docF.size());

      for (int i = 0; i < search.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = search.scoreDocs[i];
        //System.out.println(testIndex.index.explain(node, scoreDoc.doc));
        assertEquals(docF.get(testIndex.name(scoreDoc.doc)), scoreDoc.score, 0.0001);
      }
    }
  }

}