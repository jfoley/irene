package edu.umass.cs.jfoley.irene.eval.bool;

import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.collections.util.ListFns;
import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import edu.umass.cs.jfoley.irene.index.IreneIndexer;
import edu.umass.cs.jfoley.irene.processing.BitmapMatchesCollectorManager;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.store.RAMDirectory;
import org.junit.Test;
import org.roaringbitmap.RoaringBitmap;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * @author jfoley
 */
public class BoolEvalTest {
  public static void shortDoc(IreneIndexer indexer, String id, String body) {
    indexer.pushDocument(
        new StringField("id", id, Field.Store.YES),
        new TextField("body", body, Field.Store.YES)
    );
  }

  public List<String> ids(boolean shouldCompile, IreneIndex index, QExpr query) {
    query.setConfig("shouldCompileBooleans", shouldCompile);
    List<String> names = ListFns.map(index.matching(query), x -> index.getField(x, "id"));
    names.sort(Comparator.naturalOrder());
    return names;
  }

  public RoaringBitmap bits(boolean shouldCompile, IreneIndex index, QExpr query) {
    query.setConfig("shouldCompileBooleans", shouldCompile);
    return index.collect(query, new BitmapMatchesCollectorManager());
  }

  @Test
  public void testBooleans() throws IOException {
    RAMDirectory dir = new RAMDirectory();
    try (IreneIndexer indexer = new IreneIndexer(dir, false, new WhitespaceAnalyzer())) {

      shortDoc(indexer, "doc0", "a b c");
      shortDoc(indexer, "doc1", "a b");
      shortDoc(indexer, "doc2", "a");
      shortDoc(indexer, "doc3", "a c");
      shortDoc(indexer, "doc4", "b c");
      shortDoc(indexer, "doc5", "b");
      shortDoc(indexer, "doc6", "c");
      shortDoc(indexer, "doc7", "x");

      indexer.commit();
      final IreneIndex index = indexer.open();

      for (boolean c : Arrays.asList(true, false)) {
        QExpr hasA = new QExpr("text", "a");
        QExpr hasB = new QExpr("text", "b");
        QExpr hasC = new QExpr("text", "c");
        QExpr hasX = new QExpr("text", "x");
        QExpr isDoc0 = QExpr.fieldMatch("id", "doc0");

        assertEquals(Arrays.asList("doc0", "doc1"), ids(c, index, QExpr.create("and").addChild(hasA).addChild(hasB)));
        assertEquals(Arrays.asList("doc0", "doc1", "doc2", "doc3", "doc4", "doc5"), ids(c, index, QExpr.create("or").addChild(hasA).addChild(hasB)));
        assertEquals(Arrays.asList("doc0"), ids(c, index, QExpr.create("and").addChild(hasA).addChild(hasB).addChild(isDoc0)));
        assertEquals(Arrays.asList("doc0"), ids(c, index, QExpr.create("and").addChild(hasA).addChild(hasB).addChild(hasC).addChild(isDoc0)));
        assertEquals(Arrays.asList("doc7"), ids(c, index, QExpr.create("and").addChild(hasX)));
        assertEquals(Arrays.asList("doc7"), ids(c, index, QExpr.create("or").addChild(hasX)));
        assertEquals(Arrays.asList("doc0", "doc3", "doc4", "doc6"), ids(c, index, QExpr.create("or").addChild(hasC)));
        assertEquals(Arrays.asList("doc0", "doc3", "doc4", "doc6"), ids(c, index, QExpr.create("and").addChild(hasC)));
      }
    }
  }

  @Test
  public void testBooleanPrecompute() throws IOException {
    RAMDirectory dir = new RAMDirectory();
    try (IreneIndexer indexer = new IreneIndexer(dir, false, new WhitespaceAnalyzer())) {

      Set<Character> uniqTerms = new HashSet<>();
      for (int i = 0; i < 512; i++) {
        StringBuilder text = new StringBuilder();
        final char[] chars = Integer.toBinaryString(i).toCharArray();
        int idx = 0;
        for (int bit = chars.length - 1; bit >= 0; bit--, idx++) {
          if(chars[bit] == '1') {
            char c = (char) ('a'+idx);
            uniqTerms.add(c);
            text.append(c).append(' ');
          }
        }
        shortDoc(indexer, "doc"+i, text.toString());
      }

      indexer.commit();
      final IreneIndex index = indexer.open();

      List<Character> candidates = new ArrayList<>(uniqTerms);
      for (boolean shouldCompile : Arrays.asList(true, false)) {
        for (Pair<Character, Character> pair : ListFns.pairs(candidates)) {
          QExpr lhs = new QExpr("text", Character.toString(pair.left));
          QExpr rhs = new QExpr("text", Character.toString(pair.right));
          RoaringBitmap leftHits = bits(shouldCompile, index, lhs);
          RoaringBitmap rightHits = bits(shouldCompile, index, rhs);
          RoaringBitmap andHits = bits(shouldCompile, index, QExpr.create("and").addChild(lhs).addChild(rhs));
          RoaringBitmap orHits = bits(shouldCompile, index, QExpr.create("or").addChild(lhs).addChild(rhs));

          RoaringBitmap tmp = new RoaringBitmap();
          tmp.or(leftHits);
          tmp.and(rightHits);
          assertArrayEquals(andHits.toArray(), tmp.toArray());
          tmp.clear();
          tmp.or(leftHits);
          tmp.or(rightHits);
          assertArrayEquals(orHits.toArray(), tmp.toArray());

          lhs.config.set("shouldCompileBooleans", shouldCompile);
          rhs.config.set("shouldCompileBooleans", shouldCompile);

          /*
          // left limited to right
          final ExprModel lhsAsQuery = index.prepare(lhs);
          lhsAsQuery.allowed = rightHits;
          final RoaringBitmap shouldBeAnAnd = index.searcher.search(lhsAsQuery, new BitmapMatchesCollectorManager());
          assertArrayEquals(andHits.toArray(), shouldBeAnAnd.toArray());

          // right limited to left
          final ExprModel rhsAsQuery = index.prepare(rhs);
          rhsAsQuery.allowed = leftHits;
          final RoaringBitmap shouldBeAnAnd2 = index.searcher.search(rhsAsQuery, new BitmapMatchesCollectorManager());
          assertArrayEquals(andHits.toArray(), shouldBeAnAnd2.toArray());

          // right and right
          rhsAsQuery.allowed = rightHits;
          final RoaringBitmap shouldBeJustRight = index.searcher.search(rhsAsQuery, new BitmapMatchesCollectorManager());
          assertArrayEquals(rightHits.toArray(), shouldBeJustRight.toArray());
          */
        }
      }
    }
  }


}