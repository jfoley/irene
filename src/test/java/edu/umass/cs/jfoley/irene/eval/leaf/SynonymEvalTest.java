package edu.umass.cs.jfoley.irene.eval.leaf;

import edu.umass.cs.jfoley.irene.CommonTestIndex;
import edu.umass.cs.jfoley.irene.CommonTestIndexResource;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.search.TopDocs;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * @author jfoley
 */
public class SynonymEvalTest {

  @ClassRule
  public static CommonTestIndexResource resource = new CommonTestIndexResource();

  @Test
  public void testPhraseCalculation() throws IOException {
    try (CommonTestIndex tmp = new CommonTestIndex()) {

      QExpr phrase = QExpr.create("od");
      // #od(brown #syn(fox dog))
      phrase.children.addAll(Arrays.asList(
          new QExpr("text", "brown"),
          new QExpr("syn")
              .addChild(new QExpr("text", "fox"))
              .addChild(new QExpr("text", "dog"))
      ));

      TopDocs search = tmp.index.search(phrase, 3);
      int count = tmp.index.count(phrase);

      /*for (int i = 0; i < search.scoreDocs.length; i++) {
        System.out.println(search.scoreDocs[i].toString());
      }*/
      // TODO: infer movement
      assertEquals(2, count);
      assertEquals(2, search.totalHits);
      assertEquals(1.0, search.scoreDocs[0].score, 0.00001);
      assertEquals(1.0, search.scoreDocs[1].score, 0.00001);
    }
  }

  @Test
  public void testPhraseOnlyMissingTerms() throws IOException {
    try (CommonTestIndex tmp = new CommonTestIndex()) {

      QExpr phrase = QExpr.create("od");
      // #od(brown #syn(fox dog))
      phrase.children.addAll(Arrays.asList(
          new QExpr("text", "brown"),
          new QExpr("syn")
              .addChild(new QExpr("text", "math"))
              .addChild(new QExpr("text", "queijo"))
      ));

      TopDocs search = tmp.index.search(phrase, 3);
      int count = tmp.index.count(phrase);

      /*for (int i = 0; i < search.scoreDocs.length; i++) {
        System.out.println(search.scoreDocs[i].toString());
      }*/
      // TODO: infer movement
      assertEquals(0, count);
      assertEquals(0, search.totalHits);
    }
  }
}