package edu.umass.cs.jfoley.irene;

import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.lemurproject.galago.core.retrieval.query.Node;

import java.io.IOException;
import java.util.Map;

/**
 * @author jfoley
 */
public class BM25Test {
  @ClassRule
  public static CommonTestIndexResource resource = new CommonTestIndexResource();

  @Test
  public void regressGalagoSingle() throws IOException {
    CommonTestIndex testIndex = resource.getIndex();
    IreneIndex index = testIndex.index;

    for (String term : testIndex.getTerms()) {
      QExpr bm25q = new QExpr("bm25");
      bm25q.addChild(QExpr.fieldMatch(CommonTestIndex.contentsField, term));

      TopDocs search = index.search(bm25q, testIndex.getNumDocuments());
      Assert.assertEquals(testIndex.df.get(term).intValue(), search.totalHits);

      Node gquery = new Node("bm25");
      gquery.add(Node.Text(term));
      Map<String, Double> gres = testIndex.galago.transformAndExecuteQuery(gquery).asDocumentFeatures();
      for (int i = 0; i < search.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = search.scoreDocs[i];
        String name = testIndex.name(scoreDoc.doc);
        double galagoScore = gres.get(name);
        Assert.assertEquals(galagoScore, scoreDoc.score, 0.0001);
      }
    }
  }
}
