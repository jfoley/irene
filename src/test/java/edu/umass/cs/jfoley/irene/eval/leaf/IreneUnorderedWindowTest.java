package edu.umass.cs.jfoley.irene.eval.leaf;

import ciir.jfoley.chai.collections.list.IntList;
import edu.umass.cs.jfoley.irene.CommonTestIndex;
import edu.umass.cs.jfoley.irene.CommonTestIndexResource;
import edu.umass.cs.jfoley.irene.eval.phrase.PositionsIterator;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.search.TopDocs;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * @author jfoley
 */
public class IreneUnorderedWindowTest {
  @ClassRule
  public static CommonTestIndexResource resource = new CommonTestIndexResource();

  @Test
  public void testHardCoded() throws IOException {
    PositionsIterator a = new PositionsIterator(new IntList(Arrays.asList(1, 7, 11, 15, 30, 100)));
    PositionsIterator b = new PositionsIterator(new IntList(Arrays.asList(   6,     14,     99)));

    assertEquals(4, IreneUnorderedWindow.countIter(Arrays.asList(a,b), 4));
  }

  @Test
  public void testIntegration() throws IOException {
    CommonTestIndex tmp = resource.getIndex();

    QExpr phrase = QExpr.create("uw");
    phrase.children.addAll(Arrays.asList(
        new QExpr("text", "brown"),
        new QExpr("text", "fox")
    ));

    TopDocs search = tmp.index.search(phrase, 3);
    int count = tmp.index.count(phrase);
    assertEquals(2, count);
    assertEquals(2, search.totalHits);
    assertEquals(1.0, search.scoreDocs[0].score, 0.00001);
    assertEquals(1.0, search.scoreDocs[1].score, 0.00001);
  }

  @Test
  public void testIntegrationShared() throws IOException {
    CommonTestIndex tmp = resource.getIndex();

    QExpr phrase = QExpr.create("uw")
        .addChild(new QExpr("text", "brown"))
        .addChild(new QExpr("text", "fox"));

    QExpr phrase2 = QExpr.create("od")
        .addChild(new QExpr("text", "brown"))
        .addChild(new QExpr("text", "fox"));

    QExpr countSum = QExpr.create("countsum")
        .addChild(phrase)
        .addChild(phrase2);

    //System.err.println(tmp.index.explain(countSum.copy(), 0));
    //System.err.println(tmp.index.explain(countSum.copy(), 1));
    //System.err.println(tmp.index.explain(countSum.copy(), 2));

    int count = tmp.index.count(countSum.copy());
    assertEquals(1, tmp.index.count(phrase2.copy())); // 1 doc with "brown fox" in it
    assertEquals(2, tmp.index.count(phrase.copy()));

    TopDocs search = tmp.index.search(countSum.copy(), 3);
    assertEquals(2, count);
    assertEquals(2, search.totalHits);
    assertEquals(2.0, search.scoreDocs[0].score, 0.00001);
    assertEquals(1.0, search.scoreDocs[1].score, 0.00001);
  }

  @Test
  public void testGetsCountsOkay() throws IOException {
    CommonTestIndex tmp = resource.getIndex();

    QExpr phrase = QExpr.create("uw")
        .addChild(new QExpr("text", "brown"))
        .addChild(new QExpr("text", "fox"));

    QExpr phrase2 = QExpr.create("od")
        .addChild(new QExpr("text", "brown"))
        .addChild(new QExpr("text", "fox"));

    QExpr uni = QExpr.create("countsum")
        .addChild(new QExpr("text", "brown"))
        .addChild(new QExpr("text", "fox"));

    QExpr countSum = QExpr.create("countsum")
        .addChild(phrase)
        .addChild(phrase2)
        .addChild(uni);

    TopDocs search = tmp.index.search(countSum, 3);
    int count = tmp.index.count(countSum);
    assertEquals(2, count);
    assertEquals(2, search.totalHits);
    //System.out.println(tmp.index.explain(countSum, 0));
    assertEquals(4.0, search.scoreDocs[0].score, 0.00001);
    assertEquals(3.0, search.scoreDocs[1].score, 0.00001);
  }
}