package edu.umass.cs.jfoley.irene.repr;

import edu.umass.cs.jfoley.irene.CommonTestIndex;
import edu.umass.cs.jfoley.irene.CommonTestIndexResource;
import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.eval.GalagoBM25EvalNode;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * @author jfoley.
 */
public class TermVectorTest {
  @ClassRule
  public static CommonTestIndexResource resource = new CommonTestIndexResource();

  @Test
  public void testBM25Vec() throws IOException {
    CommonTestIndex testIndex = resource.getIndex();
    IreneIndex index = testIndex.index;

    // for each term:
    for (String term : testIndex.cf.keySet()) {
      QExpr bm25 = new QExpr("combine")
          .addChild(new QExpr("bm25")
              .addChild(QExpr.fieldMatch(CommonTestIndex.contentsField, term)));

      // assert that our bm25 vector is equivalent to calculating the score slowly:
      TopDocs termResults = index.search(bm25, 3);
      for (ScoreDoc scoreDoc : termResults.scoreDocs) {
        TermVector vec = TermVector.bm25(index, index.pullTerms(scoreDoc.doc), GalagoBM25EvalNode.DefaultB, GalagoBM25EvalNode.DefaultK);

        assertEquals(term+" bm25 in "+vec, scoreDoc.score, (float) vec.get(term), 1e-5);
      }
    }
  }
}