package edu.umass.cs.jfoley.irene;

import org.junit.rules.ExternalResource;

import java.io.IOException;

/**
 * @author jfoley
 */
public class CommonTestIndexResource extends ExternalResource {
  CommonTestIndex index = null;
  @Override
  protected void after() {
    try {
      if(index != null) {
        index.close();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void before() {
    try {
      index = new CommonTestIndex();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public CommonTestIndex getIndex() {
    return index;
  }
}
