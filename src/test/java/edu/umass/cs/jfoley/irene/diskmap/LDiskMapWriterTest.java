package edu.umass.cs.jfoley.irene.diskmap;

import ciir.jfoley.chai.io.TemporaryDirectory;
import org.junit.Test;
import org.lemurproject.galago.utility.CmpUtil;

import java.io.IOException;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import static org.junit.Assert.assertArrayEquals;

/**
 * @author jfoley
 */
public class LDiskMapWriterTest {

  @Test
  public void ldiskMapWriterTest() throws IOException {
    TreeMap<byte[], byte[]> random = new TreeMap<>(new CmpUtil.ByteArrComparator());

    Random rand = new Random(13);

    try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
      try (LDiskMapWriter writer = new LDiskMapWriter(tmpdir.get())) {
        byte[] key = new byte[20];
        byte[] val = new byte[200];
        rand.nextBytes(key);
        rand.nextBytes(val);

        random.put(key, val);
        writer.put(key, val);
      }

      try (LDiskMapReader reader = new LDiskMapReader(tmpdir.get())) {
        for (Map.Entry<byte[], byte[]> entry : random.entrySet()) {
          assertArrayEquals(entry.getValue(), reader.get(entry.getKey()));
        }

      }
    }
  }

}