package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.CommonTestIndex;
import edu.umass.cs.jfoley.irene.CommonTestIndexResource;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.search.TopDocs;
import org.junit.ClassRule;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author jfoley
 */
public class IreneWrappedScorerTest {
  @ClassRule
  public static CommonTestIndexResource resource = new CommonTestIndexResource();

  @Test
  public void testRequireLuceneMatches() {
    CommonTestIndex tmp = resource.getIndex();

    QExpr boolQuery = new QExpr("require")
        .addChild(new QExpr("lucene-match", "+fox -language"))
        .addChild(new QExpr("dirichlet").addChild(new QExpr("text", "the")));

    TopDocs search = tmp.index.search(boolQuery, 5);
    assertEquals(1, search.totalHits);
    assertEquals(0, search.scoreDocs[0].doc);
    assertEquals(tmp.queryLikelihood("the", 0, 1500), search.scoreDocs[0].score, 0.0001);
  }

  @Test
  public void testRequireIntRangeMatches() {
    CommonTestIndex tmp = resource.getIndex();

    QExpr intRange = new QExpr("int-range").setConfig(
        "min", 0,
        "max", 0,
        "field", CommonTestIndex.docNumberField
    );
    QExpr boolQuery = new QExpr("require")
        //.addChild(new QExpr("lucene-match", "docNo:[0 TO 0]")) // doesn't work.
        .addChild(intRange)
        .addChild(new QExpr("dirichlet").addChild(new QExpr("text", "the")));

    TopDocs search = tmp.index.search(boolQuery, 5);
    assertEquals(1, search.totalHits);
    assertEquals(0, search.scoreDocs[0].doc);
    assertEquals(tmp.queryLikelihood("the", 0, 1500), search.scoreDocs[0].score, 0.0001);
  }


}