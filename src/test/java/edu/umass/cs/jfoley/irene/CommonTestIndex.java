package edu.umass.cs.jfoley.irene;

import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.collections.util.MapFns;
import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.jfoley.irene.impl.IreneSimilarity;
import gnu.trove.map.hash.TIntIntHashMap;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.store.FSDirectory;
import org.lemurproject.galago.core.index.mem.MemoryIndex;
import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.utility.Parameters;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;

/**
 * @author jfoley
 */
public class CommonTestIndex implements Closeable {
  public static final String doc1 = "the quick brown fox jumped over the lazy dog";
  public static final String doc2 = "language modeling is the best";
  public static final String doc3 = "the fox jumped the language of the brown dog";

  public static final String idField = "id";
  public static final String docNumberField = "docNo";
  public static final String contentsField = "body";

  public static final List<String> docs = new ArrayList<>();
  static {
    docs.add(doc1);
    docs.add(doc2);
    docs.add(doc3);
  }

  List<String> names;
  final TemporaryDirectory tmpdir;
  public final IreneIndex index;
  final MemoryIndex galagoIndex;
  public final LocalRetrieval galago;

  int totalTokens = 0;
  public Map<String, Integer> cf = new HashMap<>();
  public Map<String, Integer> df = new HashMap<>();
  public Map<Integer, Map<String, Integer>> bagsOfWords = new HashMap<>();
  public TIntIntHashMap lengths = new TIntIntHashMap();

  public CommonTestIndex() throws IOException {
    tmpdir = new TemporaryDirectory();
    try {
      this.galagoIndex = new MemoryIndex(Parameters.parseArray("corpus", true));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    build();
    this.galago = new LocalRetrieval(galagoIndex);
    this.index = new IreneIndex(tmpdir.getPath());
    this.index.analyzer = new WhitespaceAnalyzer();
    index.searcher.setSimilarity(new IreneSimilarity());
  }

  private void build() throws IOException {
    names = new ArrayList<>();

    try (final FSDirectory dir = FSDirectory.open(tmpdir.get().toPath())) {
      final IndexWriterConfig cfg = new IndexWriterConfig(new WhitespaceAnalyzer());
      cfg.setSimilarity(new IreneSimilarity());
      cfg.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
      try (IndexWriter writer = new IndexWriter(dir, cfg)) {
        for (int i = 0; i < docs.size(); i++) {
          final String name = name(i);
          names.add(name);

          String text = docs.get(i);
          List<IndexableField> doc = new ArrayList<>();
          doc.add(new StringField(idField, name, Field.Store.YES));
          doc.add(new IntField(docNumberField, i, Field.Store.YES));
          doc.add(new TextField(contentsField, text, Field.Store.YES));
          writer.addDocument(doc);

          List<String> tokens = AnalyzerUtil.tokenize(cfg.getAnalyzer(), text);

          Document gdoc = new Document();
          gdoc.name = name;
          gdoc.identifier = i;
          gdoc.terms = tokens;
          gdoc.text = text;
          gdoc.tags = Collections.emptyList();
          galagoIndex.process(gdoc);

          Map<String, Integer> bag = bagsOfWords.computeIfAbsent(i, x -> new HashMap<>());
          lengths.put(i, tokens.size());

          for (String token : ListFns.unique(tokens)) {
            MapFns.addOrIncrement(df, token, 1);
          }
          for (String token : tokens) {
            MapFns.addOrIncrement(cf, token, 1);
            MapFns.addOrIncrement(bag, token, 1);
            totalTokens++;
          }
        }
      }
    }
  }

  public float queryLikelihood(String term, int doc, double mu) {
    double bg = cf.get(term) / getCollectionFrequency();
    int tf = bagsOfWords.get(doc).get(term);
    float length = lengths.get(doc);
    return (float) (Math.log(tf + mu * bg) - Math.log(length + mu));
  }

  @Override
  public void close() throws IOException {
    tmpdir.close();
  }

  public double getCollectionFrequency() {
    return totalTokens;
  }

  public Collection<String> getTerms() {
    return cf.keySet();
  }

  public int getNumDocuments() {
    return bagsOfWords.size();
  }

  public String name(int doc) {
    return "doc"+doc;
  }

  public List<String> allNames() {
    return names;
  }
}
