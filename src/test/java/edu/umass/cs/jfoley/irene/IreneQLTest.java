package edu.umass.cs.jfoley.irene;

import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * @author jfoley
 */
public class IreneQLTest {
  @ClassRule
  public static CommonTestIndexResource resource = new CommonTestIndexResource();

  @Test
  public void test() throws IOException {
    CommonTestIndex testIndex = resource.getIndex();
    IreneIndex index = testIndex.index;

    assertNotEquals(-1L, index.searcher.getIndexReader().getSumTotalTermFreq("the"));

    //index.searcher.setSimilarity(new IreneSimilarity());

    for (String term : testIndex.cf.keySet()) {
      QExpr qlm = new QExpr("combine")
          .addChild(new QExpr("dirichlet")
              .setConfig("mu", 0)
              .addChild(QExpr.fieldMatch(CommonTestIndex.contentsField, term)));

      TopDocs termResults = index.search(qlm, 3);

      assertEquals(testIndex.df.get(term).intValue(), termResults.totalHits);
      for (ScoreDoc scoreDoc : termResults.scoreDocs) {
        Integer tf = testIndex.bagsOfWords.get(scoreDoc.doc).get(term);
        assertNotNull(tf);
        float length = testIndex.lengths.get(scoreDoc.doc);
        assertEquals(Math.log(tf) - Math.log(length), scoreDoc.score, 0.0001);
      }

      // term-results:
      double mu = 1500;
      QExpr qlm1500 = new QExpr("combine")
          .addChild(new QExpr("dirichlet")
              .setConfig("mu", 1500)
              .addChild(QExpr.fieldMatch(CommonTestIndex.contentsField, term)));
      termResults = index.search(qlm1500, 3);

      double bg = testIndex.cf.get(term) / testIndex.getCollectionFrequency();

      assertEquals(testIndex.df.get(term).intValue(), termResults.totalHits);
      for (ScoreDoc scoreDoc : termResults.scoreDocs) {
        Integer tf = testIndex.bagsOfWords.get(scoreDoc.doc).get(term);
        assertNotNull(tf);
        float length = testIndex.lengths.get(scoreDoc.doc);

        assertEquals(Math.log(tf + mu * bg) - Math.log(length + mu), scoreDoc.score, 0.0001);
      }
    }
  }

  @Test
  public void test2() throws IOException {
    CommonTestIndex testIndex = resource.getIndex();
    IreneIndex index = testIndex.index;

    double mu = 1500;
    QExpr qlm = new QExpr("combine")
        .addChild(new QExpr("dirichlet")
            .setConfig("mu", mu)
            .addChild(QExpr.fieldMatch(CommonTestIndex.contentsField, "quick")))
        .addChild(new QExpr("dirichlet")
            .setConfig("mu", mu)
            .addChild(QExpr.fieldMatch(CommonTestIndex.contentsField, "dog")));

    // Test multiple query thing:
    TopDocs termResults = index.search(qlm, 3);

    for (ScoreDoc scoreDoc : termResults.scoreDocs) {
      Integer lhs_tf = testIndex.bagsOfWords.get(scoreDoc.doc).getOrDefault("quick", 0);
      assertNotNull(lhs_tf);
      Integer rhs_tf = testIndex.bagsOfWords.get(scoreDoc.doc).getOrDefault("dog", 0);
      assertNotNull(rhs_tf);

      float length = testIndex.lengths.get(scoreDoc.doc);

      double lhs_score = Math.log(lhs_tf + mu * (testIndex.cf.get("quick") / testIndex.getCollectionFrequency())) - Math.log(length + mu);
      double rhs_score = Math.log(rhs_tf + mu * (testIndex.cf.get("dog") / testIndex.getCollectionFrequency())) - Math.log(length + mu);
      assertEquals(lhs_score * .5 + rhs_score * .5, scoreDoc.score, 0.0001);
    }

  }

  @Test
  public void testSimple() throws IOException {
    IreneIndex index = resource.getIndex().index;
    assertNotEquals(-1L, index.searcher.getIndexReader().getSumTotalTermFreq("the"));
  }


  @Test
  public void testLinearSmoothing() throws IOException {
    CommonTestIndex testIndex = resource.getIndex();
    IreneIndex index = testIndex.index;

    for (String term : testIndex.cf.keySet()) {
      QExpr qlm = new QExpr("combine")
          .addChild(new QExpr("linear")
              .setConfig("lambda", 1)
              .addChild(QExpr.fieldMatch(CommonTestIndex.contentsField, term)));

      //Explanation explain = index.explain(qlm, 0);
      //System.out.println(explain);

      TopDocs termResults = index.search(qlm, 3);

      assertEquals(testIndex.df.get(term).intValue(), termResults.totalHits);
      for (ScoreDoc scoreDoc : termResults.scoreDocs) {
        Integer tf = testIndex.bagsOfWords.get(scoreDoc.doc).get(term);
        assertNotNull(tf);
        float length = testIndex.lengths.get(scoreDoc.doc);
        assertEquals(Math.log(tf) - Math.log(length), scoreDoc.score, 0.0001);
      }
    }
  }
}