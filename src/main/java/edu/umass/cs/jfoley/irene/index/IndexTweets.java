package edu.umass.cs.jfoley.irene.index;

import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.time.Debouncer;
import edu.umass.cs.jfoley.irene.IreneEnglishAnalyzer;
import edu.umass.cs.jfoley.irene.impl.IreneSimilarity;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author jfoley
 */
public class IndexTweets {
  public static final Logger logger = Logger.getLogger(IndexTweets.class.getName());

  static final SimpleDateFormat datePattern = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.US);
  static {
    datePattern.setLenient(true);
  }
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    if(argp.get("log", false)) {
      logger.setUseParentHandlers(false);
      FileHandler lfh = new FileHandler("indexing-errors.log");
      SimpleFormatter formatter = new SimpleFormatter();
      lfh.setFormatter(formatter);
      logger.addHandler(lfh);
    }

    long total = argp.get("total", 3153724L);
    long count = 0;
    Debouncer msg = null;

    try (final Directory dir = FSDirectory.open(Paths.get(argp.get("output", "/mnt/scratch3/jfoley/signal-news/tweets.irene")))) {
      final IndexWriterConfig cfg = new IndexWriterConfig(new IreneEnglishAnalyzer());
      try (LinesIterable sdocs = LinesIterable.fromFile(argp.get("input", "/mnt/scratch3/jfoley/signal-news/tweets.all.jsonl.gz"))) {
        cfg.setSimilarity(new IreneSimilarity());
        System.out.println("Similarity: "+cfg.getSimilarity());
        cfg.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

        try (IndexWriter writer = new IndexWriter(dir, cfg)) {
          msg = new Debouncer();

          for (String sdoc : sdocs) {
            try {
              Parameters jsdoc = Parameters.parseString(sdoc);
              count++;

              String pubStr = jsdoc.getString("created_at");
              long epochSecond = datePattern.parse(pubStr).toInstant().getEpochSecond();

              String text = jsdoc.getString("text");
              Parameters userDoc = jsdoc.getMap("user");
              Document doc = new Document();
              doc.add(new StringField("id", jsdoc.getString("id_str"), Field.Store.YES));
              doc.add(new StringField("lang", jsdoc.getString("lang"), Field.Store.YES));
              doc.add(new StringField("user", userDoc.getString("screen_name"), Field.Store.YES));
              doc.add(new TextField("body", text, Field.Store.YES));
              //doc.add(new StringField("mediaType", jsdoc.getString("media-type").toUpperCase(), Field.Store.YES));
              //doc.add(new StringField("source", jsdoc.getString("source"), Field.Store.YES));
              doc.add(new LongField("storedTimeStamp", epochSecond, Field.Store.YES));
              doc.add(new NumericDocValuesField("timeStamp", epochSecond));

              writer.addDocument(doc);

              if (msg.ready()) {
                System.err.println("L.Indexing: " + text + " " + count + " so far... " + msg.estimate(count, total));
              }
            } catch (Exception e) {
              logger.log(Level.WARNING, "Document-Exception", e);
            }
          }
        }

      }
    }

    System.err.println("L.Indexing.Done " + msg.estimate(count, count));
  }
}
