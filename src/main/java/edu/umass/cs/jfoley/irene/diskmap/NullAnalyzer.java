package edu.umass.cs.jfoley.irene.diskmap;

import org.apache.lucene.analysis.Analyzer;

/**
 * @author jfoley.
 */
public class NullAnalyzer extends Analyzer {
  @Override
  protected TokenStreamComponents createComponents(String fieldName) {
    throw new UnsupportedOperationException("Field that requires analysis: " + fieldName);
  }
}
