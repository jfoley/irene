package edu.umass.cs.jfoley.irene.expr;

import edu.umass.cs.jfoley.irene.IreneIndex;

/**
 * Any sort of flags or other configuration to be passed to a query evaluation run.
 * @author jfoley
 */
public class IreneQuery {
  public final QExpr originalRoot;
  private final QExpr transformed;

  public IreneQuery(IreneIndex index, QExpr originalRoot) {
    this.originalRoot = originalRoot;
    this.transformed = index.transform(originalRoot);
  }
}
