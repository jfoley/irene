package edu.umass.cs.jfoley.irene.movement;

import ciir.jfoley.chai.collections.util.ListFns;
import edu.umass.cs.jfoley.irene.eval.iters.AndDocIdSetIterator;
import edu.umass.cs.jfoley.irene.eval.iters.OrDocIdSetIterator;
import org.apache.lucene.index.PostingsEnum;
import org.apache.lucene.search.*;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


/**
 * @author jfoley.
 */
public class MoveInfo {
  public static MoveInfo all(int maxDoc) {
    MoveInfo mi = new MoveInfo();
    mi.estimatedDF = maxDoc;
    mi.all = true;
    mi.mover = DocIdSetIterator.all(maxDoc);
    return mi;
  }
  public static MoveInfo empty() {
    MoveInfo mi = new MoveInfo();
    mi.estimatedDF = 0;
    mi.mover = DocIdSetIterator.empty();
    return mi;
  }
  public static MoveInfo and(List<? extends MoveInfo> children) {
    MoveInfo andOp = new MoveInfo();
    andOp.children = new ArrayList<>(children);

    andOp.bestChild = Objects.requireNonNull(children.get(0));

    for (int i = 1; i < children.size(); i++) {
      assert andOp.bestChild != null;
      if(children.get(i).estimatedDF < andOp.bestChild.estimatedDF) {
        andOp.bestChild = children.get(i);
      }
    }

    // estimatedDF of smallest child:
    andOp.estimatedDF = andOp.bestChild != null ? andOp.bestChild.estimatedDF : 0L;
    // exact mover:
    andOp.mover = new AndDocIdSetIterator(ListFns.map(children, x -> x.mover));
    return andOp;
  }
  public static MoveInfo or(List<? extends MoveInfo> children) {
    MoveInfo orOp = new MoveInfo();

    orOp.children = new ArrayList<>(children);
    orOp.kind = Operation.OR;
    orOp.bestChild = Objects.requireNonNull(children.get(0));

    for (int i = 1; i < children.size(); i++) {
      assert orOp.bestChild != null;
      if(children.get(i).estimatedDF > orOp.bestChild.estimatedDF) {
        orOp.bestChild = children.get(i);
      }
    }

    // estimatedDF of biggest child:
    orOp.estimatedDF = orOp.bestChild != null ? orOp.bestChild.estimatedDF : 0L;

    // exact mover:
    orOp.mover = new OrDocIdSetIterator(ListFns.map(children, x -> x.mover));
    return orOp;
  }
  public static MoveInfo wrap(Scorer scorer) {
    MoveInfo forScorer = new MoveInfo();
    forScorer.mover = scorer;
    forScorer.estimatedDF = scorer.cost();
    return forScorer;
  }
  public static MoveInfo wrapTerm(PostingsEnum postings, long estimatedDF) {
    MoveInfo forTerm = new MoveInfo();
    forTerm.mover = postings;
    forTerm.estimatedDF = estimatedDF;
    return forTerm;
  }

  public static class SimplificationParams {
    final int maxDoc;
    double orApproxFraction = 0.7;
    double andApproxFraction = 0.9;
    int andApprox = 10;

    public SimplificationParams(int maxDoc) {
      this.maxDoc = maxDoc;
    }

    public SimplificationParams newWithMaxDoc(int maxDoc) {
      SimplificationParams copy = new SimplificationParams(maxDoc);
      copy.andApprox = andApprox;
      copy.andApproxFraction = andApproxFraction;
      copy.orApproxFraction = orApproxFraction;
      return copy;
    }
  }

  static SimplificationParams losslessParams = new SimplificationParams(-1);
  static {
    losslessParams.orApproxFraction = 2.0;
    losslessParams.andApproxFraction = 2.0;
    losslessParams.andApprox = Integer.MAX_VALUE;
  }

  public MoveInfo simplify(SimplificationParams params) {
    assert(params.maxDoc > 0);
    if(children.size() == 0) {
      return this;
    }
    List<MoveInfo> schildren = new ArrayList<>();

    for (MoveInfo child : children) {
      schildren.add(child.simplify(params));
      // TODO, drop children.
    }

    if (kind == Operation.OR) {
      return simplifyToOR(params, schildren);
    }
    if (kind == Operation.AND) {
      return simplifyToAND(params, schildren);
    }
    throw new UnsupportedOperationException("Can't yet simplify with kind="+kind);
  }

  private MoveInfo simplifyToOR(SimplificationParams params, List<MoveInfo> children) {
    List<MoveInfo> useful = new ArrayList<>();

    // re-estimate DF:
    this.estimatedDF = 0;
    double aritySum = 0;
    for (MoveInfo child : children) {
      if(child.all) {
        //System.out.println("Choosing Node from OR because percentage=100%");
        return child;
      } else if (!child.isEmpty()) {
        aritySum += child.estimatedDF;
        useful.add(child);
        if(this.estimatedDF < child.estimatedDF) {
          this.estimatedDF = child.estimatedDF;
        }
      }
    }

    if(useful.size() == 0) { return MoveInfo.empty(); }
    if(useful.size() == 1) { return useful.get(0); }

    int numChildren = useful.size();
    double arityMean = aritySum / (double) numChildren;
    double visitPercentage = arityMean / (double) params.maxDoc;

    // if we're going to hit >70% of documents on average, just visit them all and don't waste CPU cycles finding that out.
    if(visitPercentage > params.orApproxFraction) {
      return MoveInfo.all(params.maxDoc);
    }

    this.mover = new OrDocIdSetIterator(ListFns.map(useful, u -> u.mover));
    return this;
  }

  private MoveInfo simplifyToAND(SimplificationParams params, List<MoveInfo> children) {
    List<MoveInfo> useful = new ArrayList<>();

    // re-estimate DF:
    this.estimatedDF = params.maxDoc;
    MoveInfo minChild = children.size() > 0 ? children.get(0) : null;
    for (MoveInfo child : children) {
      double percentage = child.estimatedDF / (double) params.maxDoc;
      if (child.all || percentage > params.andApproxFraction) {
        //System.out.println("Dropping Node from AND because percentage="+percentage);
        // drop any children that are essentially "all".
        // note that this combined with the OrMover simplification to all will prune trees that aren't worth evaluating in first-pass.
      } else if (!child.isEmpty()) {
        useful.add(child);
        if(this.estimatedDF > child.estimatedDF) {
          this.estimatedDF = child.estimatedDF;
          minChild = child;
        }
      }
    }

    // break early for base cases:
    if(useful.size() == 0) { return MoveInfo.empty(); }
    if(useful.size() == 1) { return useful.get(0); }

    int numChildren = useful.size();

    // if we have enough children, it's not worth doing a real and, just grab the smallest one:
    if (numChildren > params.andApprox) {
      return minChild;
    }

    // fallback to exact mover:
    this.mover = new AndDocIdSetIterator(ListFns.map(useful, x -> x.mover));
    return this;
  }

  private boolean isEmpty() {
    return mover != null && mover.docID() == DocIdSetIterator.NO_MORE_DOCS;
  }

  public static enum Operation {
    AND,
    OR
  }

  public Parameters toJSON() {
    Parameters out = Parameters.create();
    out.put("all", all);
    out.put("estimatedDF", estimatedDF);
    out.put("children", ListFns.map(children, MoveInfo::toJSON));
    out.put("mover", mover.getClass().toString());
    out.put("kind", kind.toString());
    return out;
  }

  public boolean all = false;
  public long estimatedDF;
  public List<MoveInfo> children = Collections.emptyList();
  public DocIdSetIterator mover;
  public @Nullable MoveInfo bestChild = null;
  public Operation kind = Operation.AND;

  public boolean isLeaf() { return children.size() == 0; }
}
