package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import org.apache.lucene.search.Explanation;

import java.io.IOException;

/**
 * P(d|q) = count(q,d) / count(q,*) or alpha=0.5 if q not in document
 * @author jfoley
 */
@QueryOperator("mle")
@TermDataNeeded(DataNeeded.COUNTS)
public class DocumentLikelihoodModel extends SingleChildQueryEvalNode<CountEvalNode> {
  public static final double DefaultAlpha = 0.5;
  private final double collectionFrequency;
  private final float bg;
  private final double alpha;

  public DocumentLikelihoodModel(CountEvalNode child, double alpha) {
    super(child);
    IreneCountStatistics stats = child.getCountStatistics();
    this.alpha = alpha;
    this.collectionFrequency = stats.collectionFrequency;
    this.bg = (float) Math.log(alpha / collectionFrequency);
  }

  @Override
  public float score(int doc) throws IOException {
    int c = child.count(doc);
    if (c == 0) return bg;
    return (float) Math.log(c / collectionFrequency);
  }

  @Override
  public int count(int doc) throws IOException {
    return child.count(doc);
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    int count = child.count(doc);
    if (count > 0) {
      return Explanation.match(score(doc), "log of P(d|q)="+count+"/"+collectionFrequency, child.explain(doc));
    } else {
      return Explanation.match(score(doc), "log of P(d|miss)="+alpha+"/"+collectionFrequency, child.explain(doc));
    }
  }
}
