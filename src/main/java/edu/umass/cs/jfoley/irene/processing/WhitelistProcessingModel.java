package edu.umass.cs.jfoley.irene.processing;

import ciir.jfoley.chai.collections.TopKHeap;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Scorer;
import org.roaringbitmap.RoaringBitmap;

import java.io.IOException;

/**
 * @author jfoley
 */
public class WhitelistProcessingModel extends IreneProcessingModel {
  private final RoaringBitmap whitelist;

  public WhitelistProcessingModel(int requested, RoaringBitmap whitelist) {
    super(requested);
    this.whitelist = whitelist;
  }
  public WhitelistProcessingModel(RoaringBitmap whitelist) {
    super(whitelist.getCardinality());
    this.whitelist = whitelist;
  }

  @Override
  public void score(Scorer scorer, int doc, TopKHeap<ScoreDoc> output) throws IOException {
    if(whitelist.contains(doc)) {
      output.offer(new ScoreDoc(doc, scorer.score()));
    }
  }
}
