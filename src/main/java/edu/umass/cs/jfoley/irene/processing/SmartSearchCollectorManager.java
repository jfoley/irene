package edu.umass.cs.jfoley.irene.processing;

import ciir.jfoley.chai.collections.TopKHeap;
import edu.umass.cs.jfoley.irene.expr.ExplainScorer;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * @author jfoley
 */
public class SmartSearchCollectorManager implements CollectorManager<SmartSearchCollectorManager.SmartSearchCollector, TopDocs> {
  public static final Comparator<ScoreDoc> cmp = (lhs, rhs) -> {
    int cmp = Float.compare(lhs.score, rhs.score);
    if(cmp != 0) return cmp;
    return Integer.compare(lhs.doc, rhs.doc);
  };
  public final int requested;

  static class SmartSearchCollector implements Collector {
    public final TopKHeap<ScoreDoc> heap;

    public SmartSearchCollector(int requested) { heap = new TopKHeap<>(requested, cmp); }

    @Override
    public LeafCollector getLeafCollector(LeafReaderContext context) throws IOException {
      final int docBase = context.docBase;
      return new LeafCollector() {
        public ExplainScorer scorer;

        @Override
        public void setScorer(Scorer scorer) throws IOException {
          this.scorer = (ExplainScorer) scorer;
        }

        @Override
        public void collect(int doc) throws IOException {
          int fdoc = doc + docBase;
          float score = scorer.score();
          ScoreDoc sdoc = new ScoreDoc(fdoc, score);
          if(heap.offerAndCheckFull(sdoc)) {
            //noinspection ConstantConditions -- since block only enters when heap is full:
            scorer.setMinimumRequiredScore(heap.peek().score);
          }
        }
      };
    }

    @Override public boolean needsScores() { return true; }
  }

  public SmartSearchCollectorManager(int requested) {
    this.requested = requested;
  }

  @Override
  public SmartSearchCollector newCollector() throws IOException {
    return new SmartSearchCollector(requested);
  }

  @Override
  public TopDocs reduce(Collection<SmartSearchCollector> collectors) throws IOException {
    TopKHeap<ScoreDoc> finalHeap = new TopKHeap<>(requested, cmp);
    for (SmartSearchCollector collector : collectors) {
      for (ScoreDoc scoreDoc : collector.heap.getUnsortedList()) {
        finalHeap.offer(scoreDoc);
      }
    }

    // final merge:
    ScoreDoc[] arr = new ScoreDoc[finalHeap.size()];
    List<ScoreDoc> sorted = finalHeap.getSorted();
    for (int i = 0; i < sorted.size(); i++) {
      ScoreDoc scoreDoc = sorted.get(i);
      arr[i] = scoreDoc;
    }
    float maxScore = Float.MAX_VALUE;
    if(!sorted.isEmpty()) maxScore = sorted.get(0).score;

    return new TopDocs(Math.toIntExact(finalHeap.getTotalSeen()), arr, maxScore);
  }
}
