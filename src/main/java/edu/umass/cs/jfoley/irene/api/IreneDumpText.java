package edu.umass.cs.jfoley.irene.api;

import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.string.StrUtil;
import ciir.jfoley.chai.time.Debouncer;
import edu.umass.cs.jfoley.irene.IreneIndex;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author jfoley
 */
public class IreneDumpText {
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    IreneIndex index = new IreneIndex(argp.get("index", "/mnt/scratch3/jfoley/robust.irene/"));

    try (PrintWriter output = IO.openPrintWriter(argp.get("output","robust.txt.zst"))) {
      Debouncer msg = new Debouncer();
      AtomicLong done = new AtomicLong(0);
      long total = index.getTotalDocuments();

      index.allDocs().sequential().mapToObj(id -> {
        try {
          return StrUtil.join(index.getFieldTerms(id, "body"));
        } catch (IOException e) {
          e.printStackTrace(System.err);
        }
        return null;
      }).filter(Objects::nonNull).forEachOrdered((String x) -> {
        long count = done.incrementAndGet();
        if(msg.ready()) {
          System.err.println("#"+msg.estimateStr(count, total));
        }
        output.println(x);
      });
    }

  }
}
