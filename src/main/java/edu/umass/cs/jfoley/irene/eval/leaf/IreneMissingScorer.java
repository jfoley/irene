package edu.umass.cs.jfoley.irene.eval.leaf;

import ciir.jfoley.chai.collections.list.IntList;
import edu.umass.cs.jfoley.irene.impl.IreneTermInfo;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.Explanation;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * @see IreneWrappedScorer
 * @author jfoley
 */
public final class IreneMissingScorer implements LeafEvalNode {
  private final float missingScore;

  public IreneMissingScorer(float missingScore) {
    this.missingScore = missingScore;
  }

  @Override
  public int frequency() throws IOException { return 0; }

  @Override
  public IntList positions(int doc) throws IOException { throw new UnsupportedOperationException(); }

  @Override
  public int currentDocument() throws IOException {
    return DocIdSetIterator.NO_MORE_DOCS;
  }

  @Override
  public void syncTo(int doc) throws IOException {

  }

  @Override
  public int length(int doc) {
    throw new UnsupportedOperationException();
  }

  @Override
  public float score(int doc) {
    return missingScore;
  }

  @Override
  public int count(int doc) throws IOException {
    return 0;
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    return Explanation.noMatch("IreneMissingScorer, bg="+missingScore);
  }

  @Override
  public long estimateDF() {
    return 0;
  }

  @Override
  public IreneTermInfo getTermInfo() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int advance(int target) {
    return DocIdSetIterator.NO_MORE_DOCS;
  }

  @Nonnull
  @Override
  public MoveInfo getMovementInfo(){
    return MoveInfo.empty();
  }

  @Override
  public boolean hasNoMatches() {
    return true;
  }
}
