package edu.umass.cs.jfoley.irene.expr.macro;

import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nullable;

/**
 * @author jfoley
 */
public class MakeCanonicalOperators extends Macro {
  public MakeCanonicalOperators(Parameters cfg) {
    super(cfg);
  }

  @Nullable
  @Override
  public QExpr transform(QExpr input) {
    final String operator = this.factory.canonicalOperators.getOrDefault(input.operator, input.operator);
    QExpr newQ = input.copy(transform(input.children));
    newQ.operator = operator;
    return newQ;
  }
}
