package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import org.apache.lucene.search.Explanation;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;

/**
 * @author jfoley
 */
@QueryOperator("bm25")
@TermDataNeeded(DataNeeded.COUNTS)
public class GalagoBM25EvalNode extends SingleChildQueryEvalNode<LeafEvalNode> {
  public static double DefaultB = 0.75;
  public static double DefaultK = 1.2;
  final double b;
  final double k;
  private final double averageDocumentLength;
  private final double idf;

  public GalagoBM25EvalNode(double b, double k, LeafEvalNode leafNode) {
    super(leafNode);
    this.b = b;
    this.k = k;

    IreneCountStatistics stats = child.getCountStatistics();

    long documentCount = stats.totalDocumentCount;
    averageDocumentLength = stats.averageDocumentLength();
    long df = stats.documentFrequency;

    idf = Math.log(documentCount / (df + 0.5));
  }

  @Override
  public float score(int doc) throws IOException {
    double count = child.count(doc);
    double length = child.length(doc);

    double numerator = count * (k+1);
    double denominator = count + (k* (1- b + (b * length / averageDocumentLength)));
    return (float) (idf * (numerator / denominator));
  }

  @Override
  public int count(int doc) throws IOException {
    return child.count(doc);
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    float score = score(doc);
    String desc = Parameters.parseArray("count", child.count(doc), "length", child.length(doc), "k", k, "b", b, "averageDocumentLength", averageDocumentLength, "idf", idf).toString();
    return Explanation.match(score, desc, child.explain(doc));
  }
}
