package edu.umass.cs.jfoley.irene.expr.ast;

import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import org.apache.lucene.index.Term;

/**
 * @author jfoley
 */
public class TermRequest {
  public final Term term;
  public final DataNeeded needed;
  public TermRequest(Term term, DataNeeded needed) {
    this.term = term;
    this.needed = needed;
  }
}
