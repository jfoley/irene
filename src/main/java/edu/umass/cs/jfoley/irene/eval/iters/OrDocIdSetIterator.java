package edu.umass.cs.jfoley.irene.eval.iters;

import org.apache.lucene.search.DocIdSetIterator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley
 */
public class OrDocIdSetIterator extends DocIdSetIterator {
  final ArrayList<DocIdSetIterator> children;
  int current;
  final long cost;

  public OrDocIdSetIterator(List<DocIdSetIterator> children) {
    this.children = new ArrayList<>(children);
    // highest-cost first:
    children.sort((lhs, rhs) -> -Long.compare(lhs.cost(), rhs.cost()));

    // figure out current position and cost:
    int id = NO_MORE_DOCS;
    long maxCost = 0L;
    for (DocIdSetIterator child : children) {
      id = Math.min(child.docID(), id);
      maxCost = Math.max(child.cost(), maxCost);
    }

    this.cost = maxCost;
    this.current = id;
  }


  @Override
  public int docID() {
    return current;
  }

  @Override
  public int nextDoc() throws IOException {
    return advance(current+1);
  }

  @Override
  public int advance(int target) throws IOException {
    int nextMin = NO_MORE_DOCS;
    for (DocIdSetIterator child : children) {
      int where = child.docID();
      if(where < target) {
        where = child.advance(target);
      }
      nextMin = Math.min(where, nextMin);
      if(nextMin == target) break;
    }

    current = nextMin;
    return nextMin;
  }

  @Override
  public long cost() {
    return cost;
  }
}
