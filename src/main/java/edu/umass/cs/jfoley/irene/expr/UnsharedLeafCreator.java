package edu.umass.cs.jfoley.irene.expr;

import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.impl.IreneTermInfo;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Scorer;

import java.io.IOException;
import java.util.*;

/**
 * @author jfoley.
 */
public class UnsharedLeafCreator implements LeafCreator {
  private final QueryContext qctx;
  IdentityHashMap<LuceneScorerRequest, Scorer> generatedScorers;
  HashMap<IreneQueryLanguage.TermRequest, LeafBuilder> terms;

  // for data storage local to this class only.
  private static class LeafBuilder {
    IreneQueryLanguage.TermRequest request;
    IreneTermInfo info;
    LeafEvalNode node;
  }

  public UnsharedLeafCreator(QueryContext context) {
    this.qctx = context;
    terms = new HashMap<>();
    generatedScorers = new IdentityHashMap<>();
  }

  @Override
  public void withTermRequests(List<IreneQueryLanguage.TermRequest> termsRequested, IndexSearcher searcher, LeafReaderContext context) {

    for (IreneQueryLanguage.TermRequest req: termsRequested) {
      LeafBuilder lb = new LeafBuilder();
      lb.request = req;
      lb.info = this.qctx.getTermInfo(req.term, req.forWhat);
      this.terms.put(req, lb);
    }
  }

  @Override
  public LeafEvalNode requestTerm(IreneQueryLanguage.TermRequest input) {
    LeafBuilder lb = Objects.requireNonNull(this.terms.get(input), "Missing: "+input);
    if(lb.node == null) {
      lb.node = this.qctx.create(lb.info);
    }
    return lb.node;
  }

  @Override
  public Scorer getScorer(LuceneScorerRequest queryInfo) {
    try {
      Scorer scorer = generatedScorers.get(queryInfo);
      if(scorer == null) {
        scorer = queryInfo.getWeight().scorer(qctx.context);
        generatedScorers.put(queryInfo, scorer);
      }
      return scorer;
    } catch (IOException e) {
      throw new RuntimeException("Couldn't create a scorer from a weight for LSR: "+queryInfo, e);
    }
  }

  @Override
  public void withScorerRequests(List<LuceneScorerRequest> requests) {
  }

  @Override
  public List<DocIdSetIterator> getAllIterators() {
    List<DocIdSetIterator> movers = new ArrayList<>();
    for (LeafBuilder leafEvalNode : this.terms.values()) {
      LeafEvalNode node = leafEvalNode.node;
      if(node != null) {
        movers.add(node.getMovementInfo().mover);
      }
    }
    movers.addAll(generatedScorers.values());
    return movers;
  }
}
