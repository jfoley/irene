package edu.umass.cs.jfoley.irene.impl;

import org.apache.lucene.index.PostingsEnum;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author jfoley
 */
public enum DataNeeded {
  DOCS(0),
  COUNTS(1),
  POSITIONS(2);

  private final int level;

  DataNeeded(int i) {
    this.level = i;
  }

  public int flags() {
    switch (this) {
      case DOCS:
        return PostingsEnum.NONE;
      case COUNTS:
        return PostingsEnum.FREQS;
      case POSITIONS:
        return PostingsEnum.ALL;
      default:
        throw new IllegalStateException();
    }
  }

  @Nonnull
  public static DataNeeded max(@Nullable DataNeeded lhs, @Nullable DataNeeded rhs) {
    if(lhs == null && rhs == null) return DOCS;
    if(lhs == null) return rhs;
    if(rhs == null) return lhs;
    int id = Math.max(lhs.level, rhs.level);
    switch (id) {
      case 0: return DOCS;
      case 1: return COUNTS;
      case 2: return POSITIONS;
      default: throw new IllegalStateException();
    }
  }
}
