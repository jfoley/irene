package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import org.apache.lucene.search.Explanation;

import java.io.IOException;

/**
 * @author jfoley
 */
@QueryOperator("to-score")
@TermDataNeeded(DataNeeded.COUNTS)
public final class CountToScore extends SingleChildQueryEvalNode<CountEvalNode> {
  public CountToScore(CountEvalNode child) {
    super(child);
  }

  @Override
  public float score(int doc) throws IOException {
    return child.count(doc);
  }

  @Override
  public int count(int doc) throws IOException {
    return child.count(doc);
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    return Explanation.match(score(doc), "CountToScore", child.explain(doc));
  }
}
