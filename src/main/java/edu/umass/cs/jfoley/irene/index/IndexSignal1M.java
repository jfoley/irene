package edu.umass.cs.jfoley.irene.index;

import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.time.Debouncer;
import edu.umass.cs.jfoley.irene.index.fields.BooleanField;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.*;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author jfoley
 */
public class IndexSignal1M {
  public static final Logger logger = Logger.getLogger(IndexSignal1M.class.getName());

  static final DateTimeFormatter datePattern = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    logger.setUseParentHandlers(false);
    FileHandler lfh = new FileHandler("indexing-errors.log");
    SimpleFormatter formatter = new SimpleFormatter();
    lfh.setFormatter(formatter);
    logger.addHandler(lfh);

    //argp.put("duplicates", "signal.dups.gz");

    HashMap<String, String> duplicateField = new HashMap<>();
    HashSet<String> primaryDuplicate = new HashSet<>();
    if(argp.isString("duplicates")) {
      try(LinesIterable lines = LinesIterable.fromFile(argp.getString("duplicates"))) {
        for (String line : lines) {
          String data = line.trim();
          final String[] ids = line.split("\\s+");
          for (String id : ids) {
            duplicateField.put(id, data);
          }
          primaryDuplicate.add(ids[0]); // highlander principle
        }
      }
      System.out.println("Loaded "+duplicateField.size()+" duplicates!");
    }

    long total = argp.get("total", 1_000_000L);

    final IndexingParams params = IndexingParams.start()
        .append()
        .withPath(argp.get("output", "/mnt/scratch3/jfoley/output.irene"))
        .withAnalyzer("duplicates", new WhitespaceAnalyzer());

    Debouncer msg = null;
    try (final IreneIndexer writer = params.build()) {
      try (LinesIterable sdocs = LinesIterable.fromFile(argp.get("input", "/mnt/scratch3/jfoley/signal-news/signalmedia1m.jsonl.gz"))) {
        msg = writer.getMessage();
        for (String sdoc : sdocs) {
          try {
            Parameters jsdoc = Parameters.parseString(sdoc);

            final String id = jsdoc.getString("id");

            boolean canonical = true;
            final String duplicates = duplicateField.get(id);
            if(duplicates != null) {
              canonical = primaryDuplicate.contains(id);
            }

            String pubStr = jsdoc.getString("published");
            long epochSecond = 0;
            if(pubStr.contains("Z")) {
              Instant published = Instant.parse(pubStr);
              epochSecond = published.getEpochSecond();
            } else {
              epochSecond = LocalDate.parse(pubStr, datePattern).atStartOfDay().toInstant(ZoneOffset.UTC).getEpochSecond();
            }

            Document doc = new Document();
            doc.add(new StringField("id", id, Field.Store.YES));
            // canonical=yes if this is the one we return w/o duplicates
            doc.add(new BooleanField("canonical", canonical, Field.Store.YES));
            // each document may have a pointer to its other duplicates:
            if(duplicates != null) {
              doc.add(new TextField("duplicates", duplicates, Field.Store.YES));
            }
            doc.add(new TextField("title", jsdoc.getString("title"), Field.Store.YES));
            doc.add(new TextField("body", jsdoc.getString("content"), Field.Store.YES));
            doc.add(new StringField("mediaType", jsdoc.getString("media-type").toUpperCase(), Field.Store.YES));
            doc.add(new StringField("source", jsdoc.getString("source"), Field.Store.YES));
            doc.add(new LongField("storedTimeStamp", epochSecond, Field.Store.YES));
            doc.add(new NumericDocValuesField("timeStamp", epochSecond));
            writer.pushDocument(doc);

            if (msg.ready()) {
              long count = writer.numProcessed();
              System.err.println("L.Indexing: " + jsdoc.getString("title") + " " + count + " so far... " + msg.estimate(count, total));
            }
          } catch (Exception e) {
            logger.log(Level.WARNING, "Document-Exception", e);
          }
        }
      }
    }
  }
}
