package edu.umass.cs.jfoley.irene.eval.opt;

import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static edu.umass.cs.jfoley.irene.eval.opt.WANDDQL.min;

/**
 * Faster than #combine(), if you're doing dirichlet
 * @author jfoley.
 */
@QueryOperator("optbm25")
@TermDataNeeded(DataNeeded.COUNTS)
public class WBoolBM25 extends OptEvalNode {
  public final List<LeafEvalNode> children;
  private final float[] bestAfter;
  private final float[] bestScore;
  private final float[] bestUntil;
  private final float[] minUntil;
  private final double b;
  private final double k;
  private long numHits;
  private final int N;
  protected int wandSplit;
  protected int maxscoreSplit;
  protected int maxscoreUseful;
  public final List<OptBM25Child> cnodes;

  public final LeafEvalNode[] leafs;
  public final OptBM25Child[] cnodeArr;
  public final float[] weights;
  private int split;

  public WBoolBM25(IreneIndex index, double b, double k, List<LeafEvalNode> children, float[] weights) {
    try {
      this.b = b;
      this.k = k;
      this.N = children.size();
      this.maxscoreUseful = N;
      this.maxscoreSplit = 0;
      this.children = new ArrayList<>(children);
      // TODO sort children a la BinImp
      this.numHits = 0L;
      // figure out current position and cost:
      int id = NO_MORE_DOCS;
      long maxCost = 0L;
      for (LeafEvalNode child : children) {
        id = Math.min(child.currentDocument(), id);
        maxCost = Math.max(child.estimateDF(), maxCost);
      }
      this.numHits = maxCost;
      this.current = id;

      cnodes = new ArrayList<>(N);
      for (int i = 0; i < N; i++) {
        LeafEvalNode child = this.children.get(i);
        OptBM25Child node = new OptBM25Child(index, b, k, weights[i], child);
        cnodes.add(node);
      }

      // WAND requires this sorting:
      cnodes.sort((lhs, rhs) -> -Double.compare(lhs.max, rhs.max));

      this.weights = new float[N];
      leafs = new LeafEvalNode[N];
      cnodeArr = new OptBM25Child[N];
      for (int i = 0; i < N; i++) {
        OptBM25Child c = cnodes.get(i);
        cnodeArr[i] = c;
        leafs[i] = c.node;
        this.weights[i] = c.weight;
      }

      bestScore = new float[N];
      bestUntil = new float[N];
      minUntil = new float[N];
      bestAfter = new float[N];
      float fwdSum = 0;
      float minSum = 0;

      for (int i = 0; i < N; i++) {
        OptBM25Child child = cnodes.get(i);
        bestScore[i] = (float) child.max;

        fwdSum += child.max;
        bestUntil[i] = fwdSum;

        minSum += child.min;
        minUntil[i] = minSum;
      }

      float rvrSum = 0;
      for (int n = N-1; n >= 0; n--) {
        bestAfter[n] = rvrSum;
        rvrSum += bestScore[n];
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  // doesn't make any sense...
  public boolean hasNoMatches() { return children.size() == 0; }

  public static final float NegativeInfinity = -Float.MAX_VALUE;
  public static final boolean easyScore = false;

  @Override
  public float score(int doc) throws IOException {
    if(current == doc) {
      if(easyScore) {
        float score = 0;
        for (OptBM25Child c : cnodeArr) {
          score += c.score(doc);
        }
        return score;
      } else {
        // IF we reach here; Score OR unconditionally:
        float score = 0;
        for (int i = 0; i < split; i++) {
          score += cnodeArr[i].score(doc);
        }
        if (split > 0 && score + bestAfter[split - 1] < heapMinimumScore) {
          return NegativeInfinity;
        }
        // Score rest conditionally only:
        for (int i = split; i < N; i++) {
          score += cnodeArr[i].score(doc);
          if(score + bestAfter[i] < heapMinimumScore) {
            return NegativeInfinity;
          }
        }
        return score;
      }
    }
    return NegativeInfinity;
  }


  @Override
  public int count(int doc) throws IOException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    heapMinimumScore = NegativeInfinity;
    List<Explanation> children = new ArrayList<>(N);
    List<String> descs = new ArrayList<>(N);
    float myscore = 0;
    for (int i = 0; i < N; i++) {
      OptBM25Child optChild = cnodes.get(i);
      IreneCountStatistics stats = optChild.stats;
      int where = optChild.node.currentDocument();
      int length = optChild.node.length(doc);
      int count = optChild.node.count(doc);
      float weight = optChild.weight;
      Parameters descP = Parameters.parseArray("_doc", where, "weight", weight, "count", count, "length", length, "b", b, "k", k, "clen", stats.collectionLength, "cf", stats.collectionFrequency, "avgDL", stats.averageDocumentLength(), "idf", optChild.idf);
      double numerator = count * (k+1);
      double denominator = count + (k* (1-b + (b*length / optChild.avgDL)));
      double score = numerator / denominator;
      descP.put("wscore", weight * score);
      descP.put("score", score);
      myscore += weight * score;

      String desc = descP.toString()+"\n";
      descs.add(desc);
      children.add(optChild.node.explain(doc));
    }
    return Explanation.match(myscore, myscore+"\t"+descs.toString(), children);
  }

  private float heapMinimumScore = -Float.MAX_VALUE;
  @Override
  public void setMinimumRequiredScore(float theta) {
    heapMinimumScore = theta;

    this.wandSplit = 0;
    this.split = 0;
    this.maxscoreSplit = 0;
    this.maxscoreUseful = N;

    // Okay: Removing Order from WAND:
    // The unobserved set must be less than theta
    //   -> some of the observed must occur
    // AND the sum of all but the weakest in the observed set cannot be more than theta
    //   -> all of them must be required to get as close as possible to theta
    // AND the weakest of the observed set must be greater than what's remaining
    //   -> the sum of the unobserved can't replace any item in the observed
    //   -> all of the observed must occur

    for (int i = 0; i < N; i++) {
      double remaining = bestAfter[i];
      //System.out.printf("WAND[%d].Remaining: %1.3f\n", i, remaining);
      if (remaining < theta) {
        double weakest = min(bestScore, 0, i + 1);
        double observed = bestUntil[i];
        boolean necessary = ((observed - weakest + bestAfter[i]) < theta);
        if ((remaining < weakest) && necessary) {
          split = wandSplit = i + 1;
        }
      }
    }

    if(wandSplit == 0) {
      // fallback to maxscore
      for (int i = 0; i < N; i++) {
        double est = minUntil[i] + bestAfter[i];
        if(est < theta) {
          split = maxscoreUseful = maxscoreSplit = i+1;
          break;
        }
      }
    }

    // which to use now based on maxScoreSplit, wandSplit
  }

  @Override
  public boolean matches(int id) throws IOException {
    if(current < id) {
      current = advance(id);
    }
    return current == id;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createOrMover(children);
  }

  @Override
  public long estimateDF() {
    return numHits;
  }

  /** as an iterator! */
  int current = 0;

  @Override
  public int docID() { return current; }

  @Override
  public int nextDoc() throws IOException {
    return advance(current+1);
  }

  @Override
  public int advance(int target) throws IOException {
    if(wandSplit > 0) {
      // AND movement from WAND:
      current = target;

      while(true) {
        boolean match = true;
        for (int i = 0; i < wandSplit; i++) {
          int where = leafs[i].currentDocument();
          if(where < current) {
            where = leafs[i].advance(current);
            if(where == NO_MORE_DOCS) return NO_MORE_DOCS;
          }
          if(where > current) {
            current = where;
            match = false;
            break; // to continue while
          }
        }

        if(match) {
          return current;
        }
      }
    } else {
      // OR movement from maxscore:
      int nextMin = NO_MORE_DOCS;
      for (int i = 0; i < maxscoreUseful; i++) {
        LeafEvalNode child = leafs[i];
        int where = child.currentDocument();
        if (where < target) {
          where = child.advance(target);
        }
        nextMin = Math.min(where, nextMin);
        if (nextMin == target) break;
      }

      current = nextMin;
      return nextMin;
    }
  }

  @Override
  public long cost() {
    return numHits;
  }
}
