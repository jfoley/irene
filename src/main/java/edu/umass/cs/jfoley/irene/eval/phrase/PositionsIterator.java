package edu.umass.cs.jfoley.irene.eval.phrase;

import ciir.jfoley.chai.collections.list.IntList;

import javax.annotation.Nonnull;
import java.util.Arrays;

/**
 * @author jfoley
 */
public class PositionsIterator {
  private final int size;
  private int[] data;
  private int index;

  public PositionsIterator(@Nonnull IntList data) {
    this.data = data.unsafeArray();
    this.size = data.size();
    this.index = 0;
  }

  public boolean isDone() {
    return index >= size;
  }

  public void reset() {
    index = 0;
  }

  public boolean next() {
    index++;
    return !isDone();
  }

  public String toString() {
    return Arrays.toString(Arrays.copyOf(data, size));
  }

  public int currentPosition() {
    return data[index];
  }

  public int count() {
    return size;
  }
}
