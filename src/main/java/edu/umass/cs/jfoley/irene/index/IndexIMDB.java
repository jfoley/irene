package edu.umass.cs.jfoley.irene.index;

import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.io.Directory;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.archive.ZipArchive;
import ciir.jfoley.chai.string.StrUtil;
import ciir.jfoley.chai.time.Debouncer;
import edu.umass.cs.jfoley.irene.IreneIndex;
import gnu.trove.map.hash.TIntFloatHashMap;
import gnu.trove.set.hash.TIntHashSet;
import org.apache.lucene.document.*;
import org.jsoup.Jsoup;
import org.lemurproject.galago.core.util.WordLists;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author jfoley.
 */
public class IndexIMDB {
  public static void main(String[] args) throws IOException {
    Directory input = Directory.Read("/home/jfoley/code/data/aclImdb");
    Directory train = input.childDir("train");
    Directory test = input.childDir("test");

    List<ZipArchive> trainingZips = Arrays.asList(
        ZipArchive.open(train.child("pos.zip")),
        ZipArchive.open(train.child("neg.zip")));

    List<ZipArchive> bgZips = Collections.singletonList(
        ZipArchive.open(train.child("unsup.zip"))
    );

    List<ZipArchive> testingZips = Arrays.asList(
        ZipArchive.open(test.child("pos.zip")),
        ZipArchive.open(test.child("neg.zip")));

    try (IreneIndexer index = IndexingParams.start().withPath("imdb-fix.lucene").create().build()) {
      indexDocsFromZips(trainingZips, index, "train");
      indexDocsFromZips(bgZips, index, "unsup");
      indexDocsFromZips(testingZips, index, "test");
    }
  }

  private static void indexDocsFromZips(List<ZipArchive> trainingZips, IreneIndexer index, String fold) {
    for (ZipArchive trainingZip : trainingZips) {
      System.out.println(fold+"\t"+trainingZip.getName()+"\t"+index.numProcessed());
      trainingZip.forEach(is -> {
        String text;
        try {
          String html = IO.readAll(is.getReader());
          text = Jsoup.parse(html).text();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
        String name = StrUtil.takeAfterLast(is.getName(), '/');
        String id = StrUtil.takeBefore(name, '_');
        int label = Integer.parseInt(StrUtil.takeAfter(StrUtil.takeBeforeLast(name, ".txt"), "_"));

        String uniq = String.format("%s_%s_%d", fold, id, label);

        if(index.printMessageIfReady(System.out, 100_000)) {
          System.out.println("\t"+uniq+"\t"+text);
        }
        String classLabel = "NEUTRAL";
        if(label >= 7) {
          classLabel = "POSITIVE";
        } else if(label <= 4) {
          classLabel = "NEGATIVE";
        }

        index.pushDocument(
            new StringField("id", uniq, Field.Store.YES),
            new StringField("class", classLabel, Field.Store.YES),
            new StringField("index", id, Field.Store.YES),
            new IntField("label", label, Field.Store.YES),
            new StringField("fold", fold, Field.Store.YES),
            new TextField("body", text, Field.Store.YES)
        );
      });
    }
  }

  public static class CAL4CharShingling {
    public static void main(String[] args) throws IOException {
      Directory base = Directory.Read("/home/jfoley/code/ictir-models/");
      IreneIndex index = new IreneIndex(base.childPath("imdb.lucene"));

      Directory out = base.childDir("data-cal");
      processToFile(index, "train", out.childPath("imdb.train"));
      processToFile(index, "test",  out.childPath("imdb.test"));
      processToFile(index, "unsup", out.childPath("imdb.unsup"));

    }
    public static void processToFile(IreneIndex index, String fold, String output) {
      Debouncer msg = new Debouncer();
      final int count = fold.equals("unsup") ? 50000 : 25000;
      AtomicLong processed = new AtomicLong(0);

      List<String> names = new ArrayList<>(count);
      try (PrintWriter out = IO.openPrintWriter(output)) {
        index.allDocs().forEachOrdered(docIndex -> {
          Document doc = index.doc(docIndex);
          if(doc == null) return;
          if(!fold.equals(doc.get("fold"))) {
            return;
          }
          String id = doc.get("id");
          names.add(id);
          char[] text = doc.get("body").toCharArray();

          TIntHashSet brepr = new TIntHashSet(text.length);
          // Cormack, Grossman "Evaluation of Machine-Learning Protocols for Technology-Assisted Review in Electronic Discovery"
          for (int i = 0; i < text.length-3; i++) {
            String slice = new String(text, i, 4).toLowerCase();
            int hash = Math.abs(slice.hashCode()) % 1_000_081; // prime near 1 million
            brepr.add(hash);
          }

          int[] features = brepr.toArray();
          brepr.clear();
          brepr = null;
          Arrays.sort(features);

          int label = doc.getField("label").numericValue().intValue();

          StringBuilder sb = new StringBuilder();
          sb.append(label);
          for (int fid : features) {
            sb.append(' ').append(fid+1).append(':').append(1);
          }
          out.println(sb);

          long ct = processed.incrementAndGet();
          if(msg.ready()) {
            System.out.println(fold+"\t"+msg.estimateStr(ct, count));
          }
        });

        try (PrintWriter nout = IO.openPrintWriter(output+".names")) {
          for (String name : names) {
            nout.println(name);
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

    }
  }

  public static class DumpFeatFiles {
    public static void main(String[] args) throws IOException {
      Directory base = Directory.Read("/home/jfoley/code/ictir-models/");
      IreneIndex index = new IreneIndex(base.childPath("imdb.lucene"));

      Directory out = base.childDir("data-n" + NgramSize);
      Map<String, Integer> vocab = new HashMap<>();
      vocab.put("OOV", 0); // to play nice with liblinear
      processToFile(index, vocab, "train", out.childPath("imdb.train"));
      processToFile(index, vocab, "test",  out.childPath("imdb.test"));
      processToFile(index, vocab, "unsup", out.childPath("imdb.unsup"));

      List<Pair<Integer, String>> vocabTerms = new ArrayList<>();
      vocab.forEach((key, idx) -> {
        vocabTerms.add(Pair.of(idx, key));
      });
      vocabTerms.sort(Pair.cmpLeft());
      try (PrintWriter vout = IO.openPrintWriter(out.child("vocab.txt"))) {
        for (int i = 0; i < vocabTerms.size(); i++) {
          Pair<Integer, String> vocabTerm = vocabTerms.get(i);
          assert(i == vocabTerm.left) : "i="+i+", vt="+vocabTerm;
          vout.println(vocabTerm.right);
        }
      }
    }
    public final static int NgramSize = 1;
    private static final Set<String> stopwords = WordLists.getWordListOrDie("inquery");
    private static final org.lemurproject.galago.krovetz.KStem sKStem = new org.lemurproject.galago.krovetz.KStem();
    public static void processToFile(IreneIndex index, Map<String, Integer> vocab, String fold, String output) {
      Debouncer msg = new Debouncer();
      final int count = fold.equals("unsup") ? 50000 : 25000;
      AtomicLong processed = new AtomicLong(0);


      List<String> names = new ArrayList<>(count);
      try (PrintWriter out = IO.openPrintWriter(output)) {
        index.allDocs().forEachOrdered(docIndex -> {
          Document doc = index.doc(docIndex);
          if(doc == null) return;
          if(!fold.equals(doc.get("fold"))) {
            return;
          }
          String id = doc.get("id");
          names.add(id);
          String text = doc.get("body");
          List<String> terms = index.tokenize(text);
          int label = doc.getField("label").numericValue().intValue();

          TIntFloatHashMap features = new TIntFloatHashMap(terms.size()/2);
          for(String term : terms) {
            if(stopwords.contains(term)) {
              continue;
            }
            int termId = vocab.computeIfAbsent(term, missing -> vocab.size());
            features.adjustOrPutValue(termId, 1, 1);
          }

          // ngrams
          for (int i = 2; i <= NgramSize; i++) {
            for (List<String> kv : ListFns.sliding(terms, NgramSize)) {
              String ngram = StrUtil.join(kv, "_");
              int termId = vocab.computeIfAbsent(ngram, missing -> vocab.size());
              features.adjustOrPutValue(termId, 1, 1);
            }
          }

          List<Pair<Integer, Float>> fpairs = new ArrayList<>(features.size());
          features.forEachEntry((fid,score) -> {
            fpairs.add(Pair.of(fid, score));
            return true;
          });
          fpairs.sort(Pair.cmpLeft());

          StringBuilder sb = new StringBuilder();
          sb.append(label);
          for (Pair<Integer, Float> fpair : fpairs) {
            sb.append(' ').append(fpair.left).append(':').append(fpair.right);
          }
          out.println(sb);

          long ct = processed.incrementAndGet();
          if(msg.ready()) {
            System.out.println(fold+"\t"+msg.estimateStr(ct, count));
          }
        });

        try (PrintWriter nout = IO.openPrintWriter(output+".names")) {
          for (String name : names) {
            nout.println(name);
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

    }
  }
}
