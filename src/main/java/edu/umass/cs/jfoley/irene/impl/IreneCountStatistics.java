package edu.umass.cs.jfoley.irene.impl;

import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.TermStatistics;

/**
 * @author jfoley
 */
public class IreneCountStatistics {

  public long collectionFrequency;
  public long documentFrequency;

  /** Length of the collection, in terms or tokens. */
  public long collectionLength;
  /** Length of the collection, in number of documents. */
  public long totalDocumentCount;

  /** Number of unique terms in the collection (usually a specific field */
  public long totalUniqueTerms;

  public IreneCountStatistics() {
    this(0,0,0,0,0);
  }
  public IreneCountStatistics(long collectionFrequency, long documentFrequency, long collectionLength, long totalDocumentCount, long totalUniqueTerms) {
    this.collectionFrequency = collectionFrequency;
    this.documentFrequency = documentFrequency;
    this.collectionLength = collectionLength;
    this.totalDocumentCount = totalDocumentCount;
    this.totalUniqueTerms = totalUniqueTerms;
  }

  public IreneCountStatistics(CollectionStatistics cstats, TermStatistics tstats, long totalUniqueTerms) {
    this(tstats.totalTermFreq(), tstats.docFreq(), cstats.sumTotalTermFreq(), cstats.maxDoc(), totalUniqueTerms);
  }

  public void add(IreneCountStatistics rhs) {
    this.collectionFrequency += rhs.collectionFrequency;
    this.documentFrequency += rhs.documentFrequency;

    this.collectionLength += rhs.collectionLength;
    this.totalDocumentCount += rhs.totalDocumentCount;
  }

  public double averageDocumentLength() {
    return collectionLength / (double) totalDocumentCount;
  }
}
