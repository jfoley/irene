package edu.umass.cs.jfoley.irene.expr;

import org.apache.lucene.search.Query;
import org.apache.lucene.search.Weight;

/**
 * @author jfoley.
 */
public class LuceneScorerRequest {
  private final String rawQuery;
  private final Query parsed;
  private final Query rewritten;
  public boolean needsScores = true;
  private Weight weight;
  /** Set to true if it's actually being used! */
  public boolean requested = false;

  public LuceneScorerRequest(String rawQuery, Query parsed, Query rewritten) { this.rawQuery = rawQuery;
    this.parsed = parsed;
    this.rewritten = rewritten;
  }

  @Override
  public String toString() {
    return "LuceneScorerRequest("+rawQuery+", "+parsed+", "+rewritten+", needsScores="+needsScores+")";
  }

  public Query getQuery() {
    return rewritten;
  }

  public void setWeight(Weight weight) {
    this.weight = weight;
  }

  public Weight getWeight() {
    return weight;
  }
}
