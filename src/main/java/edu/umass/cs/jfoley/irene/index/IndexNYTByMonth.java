package edu.umass.cs.jfoley.irene.index;

import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.io.inputs.InputContainer;
import ciir.jfoley.chai.io.inputs.InputFinder;
import ciir.jfoley.chai.io.inputs.InputStreamable;
import ciir.jfoley.chai.string.StrUtil;
import ciir.jfoley.chai.time.Debouncer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.json.JSONUtil;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * @author jfoley
 */
public class IndexNYTByMonth {
  static final DateTimeFormatter datePattern = DateTimeFormatter.ofPattern("yyyyMMdd");
  public static LocalDate parseNYTDate(String input) {
    return LocalDate.parse(input, datePattern);
  }

  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);

    String inputPath = argp.get("input", "/media/jfoley/flash/nyt-bymonth.zip");

    InputFinder fdr = InputFinder.Default();
    StandardAnalyzer analyzer = new StandardAnalyzer(CharArraySet.EMPTY_SET);
    final IndexWriterConfig cfg = new IndexWriterConfig(analyzer);

    Debouncer msg = new Debouncer();
    int count = 0;
    try (final Directory dir = FSDirectory.open(Paths.get(argp.get("output", "/media/jfoley/flash/indexes/nyt.index")));
         final IndexWriter writer = new IndexWriter(dir, cfg)) {
      for (InputContainer container : fdr.findAllInputs(inputPath)) {
        for (InputStreamable streamable : container.getInputs()) {
          try (LinesIterable lines = streamable.lines()) {
            for (String line : lines) {
              String dat[] = line.split("\t");
              String docName = dat[0];
              // remove quotes and unescape:
              String title = JSONUtil.unescape(StrUtil.removeSurrounding(dat[1], "\"", "\""));
              String body = JSONUtil.unescape(StrUtil.removeSurrounding(dat[2], "\"", "\""));

              String dateString = StrUtil.takeBefore(docName, "_");
              String uniqueId = StrUtil.takeAfter(docName, "_");

              LocalDate date = parseNYTDate(dateString);
              long unixTime = date.atStartOfDay().toInstant(ZoneOffset.UTC).getEpochSecond();


              count++;
              if(msg.ready()) {
                System.err.println(msg.estimate(count, 1_831_109));
                System.err.println(docName);
                System.err.println("\t" + date + " " + unixTime);
                System.err.println("\t" + title);
              }

              Document ldoc = new Document();
              // when
              ldoc.add(new NumericDocValuesField("timeStamp", unixTime));
              ldoc.add(new LongField("storedTimeStamp", unixTime, Field.Store.YES));

              // what:
              ldoc.add(new StringField("id", uniqueId, Field.Store.YES));
              ldoc.add(new TextField("title", title, Field.Store.YES));
              ldoc.add(new TextField("body", body, Field.Store.YES));
              ldoc.add(new IntField("year", date.getYear(), Field.Store.NO));
              ldoc.add(new IntField("month", date.getMonthValue(), Field.Store.NO));
              ldoc.add(new IntField("day", date.getDayOfMonth(), Field.Store.NO));

              writer.addDocument(ldoc);
            }
          }
        }
      }
    }
    System.err.println("Done!");
  }
}
