package edu.umass.cs.jfoley.irene.index.fields;

import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;

/**
 * @author jfoley
 */
public final class BooleanField extends Field {
  public final static String TRUE = "TRUE";
  public final static String FALSE = "FALSE";

  public BooleanField(String name, boolean value, Store stored) {
    super(name, value ? TRUE : FALSE, stored == Store.YES ? StringField.TYPE_STORED : StringField.TYPE_NOT_STORED);
  }
}
