package edu.umass.cs.jfoley.irene.index;

import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.time.Debouncer;
import edu.umass.cs.jfoley.irene.IreneEnglishAnalyzer;
import edu.umass.cs.jfoley.irene.impl.IreneSimilarity;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author jfoley.
 */
public class IndexReutersJSONL {
  public static final Logger logger = Logger.getLogger(IndexSignal1M.class.getName());

  static final DateTimeFormatter datePattern = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    logger.setUseParentHandlers(false);
    FileHandler lfh = new FileHandler("indexing-errors.log");
    SimpleFormatter formatter = new SimpleFormatter();
    lfh.setFormatter(formatter);
    logger.addHandler(lfh);

    long total = argp.get("total", 806791L);
    long count = 0;
    Debouncer msg = null;

    try (final Directory dir = FSDirectory.open(Paths.get(argp.get("output", "reuters.irene")))) {
      final IndexWriterConfig cfg = new IndexWriterConfig(new IreneEnglishAnalyzer());
      try (LinesIterable sdocs = LinesIterable.fromFile(argp.get("input", "/home/jfoley/code/controversyNews/scripts/reuters/reuters.jsonl.gz"))) {
        cfg.setSimilarity(new IreneSimilarity());
        System.out.println("Similarity: "+cfg.getSimilarity());
        cfg.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

        try (IndexWriter writer = new IndexWriter(dir, cfg)) {
          msg = new Debouncer();

          for (String sdoc : sdocs) {
            try {
              Parameters jsdoc = Parameters.parseString(sdoc);
              count++;

              String pubStr = jsdoc.getString("published");
              long epochSecond = 0;
              epochSecond = LocalDate.parse(pubStr, datePattern).atStartOfDay().toInstant(ZoneOffset.UTC).getEpochSecond();

              Document doc = new Document();
              doc.add(new StringField("id", jsdoc.getString("itemid"), Field.Store.YES));
              doc.add(new TextField("title", jsdoc.getString("title"), Field.Store.YES));
              doc.add(new TextField("body", jsdoc.getString("content"), Field.Store.YES));
              doc.add(new LongField("storedTimeStamp", epochSecond, Field.Store.YES));
              doc.add(new NumericDocValuesField("timeStamp", epochSecond));

              writer.addDocument(doc);

              if (msg.ready()) {
                System.err.println("L.Indexing: " + jsdoc.getString("title") + " " + count + " so far... " + msg.estimate(count, total));
              }
            } catch (Exception e) {
              logger.log(Level.WARNING, "Document-Exception", e);
            }
          }
        }

      }
    }

    System.err.println("L.Indexing.Done " + msg.estimate(count, count));
  }
}
