package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import org.apache.lucene.search.Explanation;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;

/**
 * @author jfoley
 */
@QueryOperator(value={"linear","jm"})
@TermDataNeeded(DataNeeded.COUNTS)
public class LinearSmoothingEvalNode extends SingleChildQueryEvalNode<CountEvalNode> {
  public static final double DefaultLambda = 0.8;
  final double lambda;
  private final long collectionFreq;
  private final long collectionLength;
  private final double background;

  public LinearSmoothingEvalNode(CountEvalNode child, double lambda) {
    super(child);
    IreneCountStatistics stats = child.getCountStatistics();
    this.lambda = lambda;
    this.collectionFreq = stats.collectionFrequency;
    this.collectionLength = stats.collectionLength;
    background = (1-lambda) * Math.max(collectionFreq, 0.5) / collectionLength;
  }

  @Override
  public float score(int doc) throws IOException {
    double count = child.count(doc);
    double length = child.length(doc);
    return (float) Math.log(lambda * (count/length) + background);
  }

  @Override
  public int count(int doc) throws IOException {
    return child.count(doc);
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    float score = score(doc);
    String desc = Parameters.parseArray("count", child.count(doc), "length", child.length(doc), "lambda", lambda, "clen", collectionLength, "cf", collectionFreq).toString();
    return Explanation.match(score, desc, child.explain(doc));
  }
}
