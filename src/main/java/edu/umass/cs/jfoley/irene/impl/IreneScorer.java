package edu.umass.cs.jfoley.irene.impl;

import edu.umass.cs.jfoley.irene.eval.QueryEvalNode;
import edu.umass.cs.jfoley.irene.expr.ExplainScorer;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.Explanation;

import java.io.IOException;

/**
 * @author jfoley
 */
public final class IreneScorer extends ExplainScorer {
  final DocIdSetIterator iter;
  private final QueryEvalNode eval;

  public IreneScorer(QueryEvalNode eval, DocIdSetIterator mover) {
    this.eval = eval;
    this.iter = (eval instanceof DocIdSetIterator) ? (DocIdSetIterator) eval : mover;
  }

  @Override
  public void setMinimumRequiredScore(float score) {
    eval.setMinimumRequiredScore(score);
  }

  @Override
  public float score() throws IOException {
    int doc = iter.docID();
    if(doc == NO_MORE_DOCS) {
      throw new RuntimeException();
    }
    float score = eval.score(doc);
    if(Float.isNaN(score)) throw new RuntimeException("NaN found trying to score doc="+doc);
    if(Float.isInfinite(score)) throw new RuntimeException("Infinity found trying to score doc="+doc);
    return score;
  }

  @Override
  public int freq() throws IOException {
    return eval.count(iter.docID());
  }

  @Override
  public int docID() {
    return iter.docID();
  }

  @Override
  public int nextDoc() throws IOException {
    int r = iter.nextDoc();
    return nextMatching(r);
  }

  public int nextMatching(int docID) throws IOException {
    for (int id = docID; id < NO_MORE_DOCS; id = iter.nextDoc()) {
      if(eval.matches(id)) {
        return id;
      }
    }
    return NO_MORE_DOCS;
  }

  @Override
  public int advance(int target) throws IOException {
    int at = iter.advance(target);
    if(at == NO_MORE_DOCS) return at;
    return nextMatching(at);
  }

  @Override
  public String toString() {
    return "IreneScorer@"+docID()+": "+eval+" "+iter;
  }

  @Override
  public long cost() {
    return iter.cost();
  }

  public Explanation explain(int doc) throws IOException {
    if(docID() != doc) {
      return Explanation.noMatch("IreneScorer::noMatch("+doc+", still at docID="+docID()+"), q="+this.eval.explain(doc));
      //return Explanation.noMatch("IreneScorer::noMatch("+doc+", still at docID="+docID()+"), q="+this.eval.explain(doc));
    } else {
      float score = eval.score(doc);
      return Explanation.match(score, "IreneScorer::match("+doc+")", eval.explain(doc));
    }
  }
}
