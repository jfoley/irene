package edu.umass.cs.jfoley.irene.expr;

import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Scorer;

import java.util.List;

/**
 * @author jfoley.
 */
public interface LeafCreator {
  void withTermRequests(List<IreneQueryLanguage.TermRequest> termsRequested, IndexSearcher searcher, LeafReaderContext context);

  LeafEvalNode requestTerm(IreneQueryLanguage.TermRequest input);

  Scorer getScorer(LuceneScorerRequest queryInfo);

  void withScorerRequests(List<LuceneScorerRequest> requests);

  /** For extremely safe but slow, OR movement. */
  List<DocIdSetIterator> getAllIterators();
}
