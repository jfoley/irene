package edu.umass.cs.jfoley.irene.eval.opt;

import ciir.jfoley.chai.math.StreamingStats;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import org.apache.lucene.index.Term;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

/**
 * @author jfoley
 */
public class OptBM25Child {
  public static final float NegativeInfinity = -Float.MAX_VALUE;
  /**
   * Todo, this is a hack.
   */
  public static Cache<Term, Future<StreamingStats>> scoreStatsCache = Caffeine.newBuilder().maximumSize(100_000).build();
  public final float weight;
  public final LeafEvalNode node;
  public final StreamingStats scores;
  public final IreneCountStatistics stats;
  public final double b;
  public final double k;
  public final double max;
  public final double min;
  public final float idf;
  public double diff;
  public final double avgDL;

  public OptBM25Child(IreneIndex index, double b, double k, float weight, LeafEvalNode node) {
    this.b = b;
    this.k = k;
    this.node = node;
    this.stats = node.getCountStatistics();
    try {
      this.scores = scoreStatsCache.get(node.getTerm(), missing ->
          ForkJoinPool.commonPool().submit(() -> index.getStats(new QExpr("bm25", new QExpr(missing)).setConfig("b", b, "k", k))

          )).get();
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException(e);
    }

    avgDL = stats.averageDocumentLength();
    long df = stats.documentFrequency;
    long dc = stats.totalDocumentCount;
    this.idf = (float) Math.log(dc / (df + 0.5));

    // idf already counted in this one:
    this.max = scores.getMax() * weight;

    // BM25 can move idf into weight parameter:
    this.weight = weight*idf;

    // 0-scoring document of length 1...; from Galago
    // use the weight with idf here to skip a multiplication
    this.min = this.weight * ((k+1) / (k* (1-b + (b / avgDL))));
    diff = max - min;
  }

  public final double score(int doc) throws IOException {
    int count = node.count(doc);
    int length = node.length(doc);

    double numerator = count * (k+1);
    double denominator = count + (k* (1-b + (b*length / avgDL)));

    return weight * (numerator / denominator); // note idf flattened into weight
  }
}
