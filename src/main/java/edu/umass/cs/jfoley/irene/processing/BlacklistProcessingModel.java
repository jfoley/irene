package edu.umass.cs.jfoley.irene.processing;

import ciir.jfoley.chai.collections.TopKHeap;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Scorer;
import org.roaringbitmap.RoaringBitmap;

import java.io.IOException;

/**
 * @author jfoley
 */
public class BlacklistProcessingModel extends IreneProcessingModel {
  private final RoaringBitmap blacklist;

  public BlacklistProcessingModel(int requested, RoaringBitmap blacklist) {
    super(requested);
    this.blacklist = blacklist;
  }

  @Override
  public void score(Scorer scorer, int doc, TopKHeap<ScoreDoc> output) throws IOException {
    if(blacklist.contains(doc)) return;
    output.offer(new ScoreDoc(doc, scorer.score()));
  }
}
