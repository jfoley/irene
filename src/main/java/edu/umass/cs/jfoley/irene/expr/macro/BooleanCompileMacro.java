package edu.umass.cs.jfoley.irene.expr.macro;

import edu.umass.cs.jfoley.irene.expr.LuceneScorerRequest;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nullable;

/**
 * Lucene's Boolean subsystem is so much faster than anything we could hack together, so we compile any  totally boolean subtrees in the query to a "lucene-match" scorer.
 * @author jfoley
 */
public class BooleanCompileMacro extends Macro {
  static class Mark {
    final boolean matches;
    public Mark(boolean matches) {
      this.matches = matches;
    }
  }
  public BooleanCompileMacro(Parameters cfg) {
    super(cfg);
  }

  @Nullable
  @Override
  public QExpr transform(QExpr input) {
    // skip any nodes that are marked to be skipped:
    if(!input.config.get("shouldCompileBooleans", true)) return input;
    if(input.isLeaf()) return input; // don't compile single elements.

    boolean compilable = markAll(input);

    if(compilable) {
      Query lquery = compile(input);
      QExpr compiled = new QExpr("lucene-match", lquery.toString());
      LuceneScorerRequest instance = factory.luceneQuery(lquery);
      instance.needsScores = false;
      compiled.setUserData(LuceneScorerRequest.class, instance);
      return compiled;
    }
    return input.copy(transform(input.children));
  }

  private Query compile(QExpr input) {
    assert(input.getUserData(Mark.class).matches) : "Subtree is not compilable: "+input;
    //System.err.println("BooleanCompileMacro::compile("+input+")");

    if(input.isLeaf()) {
      return factory.getLuceneQuery(input).getQuery();
    } else {
      BooleanClause.Occur kind;
      BooleanQuery.Builder builder = new BooleanQuery.Builder();
      switch (input.operator) {
        case "and":
          kind = BooleanClause.Occur.MUST;
          //builder.setMinimumNumberShouldMatch(input.children.size());
          break;
        case "or":
          kind = BooleanClause.Occur.SHOULD;
          break;
        case "not":
          kind = BooleanClause.Occur.MUST_NOT;
          break;
        default:
          throw new UnsupportedOperationException(input.toString());
      }
      for (QExpr child : input.children) {
        builder.add(compile(child), kind);
      }
      return builder.build();
    }
  }

  public boolean isSupportedNode(QExpr input) {
    switch (input.operator) {
      case "or":
      case "not":
      case "and":
      case "lucene-match":
      case "category":
      case "int-range":
      case "text":
      case "extents":
      case "counts":
      case "lucene-count":
        return true;
    }
    return false;
  }

  private boolean markAll(QExpr input) {
    if(input.hasUserData(Mark.class)) {
      return input.getUserData(Mark.class).matches;
    }

    // leaf node:
    boolean current = isSupportedNode(input);
    boolean allChildren = true;
    for (QExpr child : input.children) {
      allChildren &= markAll(child);
    }
    if(current && allChildren) {
      input.setUserData(Mark.class, new Mark(true));
      return true;
    } else {
      input.setUserData(Mark.class, new Mark(false));
      return false;
    }
  }
}
