package edu.umass.cs.jfoley.irene.api;

import ciir.jfoley.chai.string.StrUtil;
import ciir.jfoley.chai.web.WebServer;
import ciir.jfoley.chai.web.json.JSONAPI;
import ciir.jfoley.chai.web.json.JSONMethod;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.json.JSONUtil;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author jfoley
 */
public class IreneAPIServer implements Closeable, Runnable {
  private final Parameters argp;
  private final IreneIndex index;
  private final Map<String, JSONMethod> methods;
  private final HashSet<String> fields;

  public IreneAPIServer(Parameters argp) throws IOException {
    this.argp = argp;
    this.fields = new HashSet<>(argp.getAsList("fields", String.class));
    this.index = new IreneIndex(argp.getString("index"));
    this.methods = new HashMap<>();
    methods.put("/arguments", this::getArguments);
    methods.put("/docs", this::getDocuments);
    methods.put("/search", this::search);
    methods.put("/stats", this::termStats);
    methods.put("/cacheSizes", (p) -> {
      Parameters output = Parameters.create();
      output.put("docCache", docCache.estimatedSize());
      output.put("searchCache", searchCache.estimatedSize());
      output.put("statsCache", statsCache.estimatedSize());
      return output;
    });
    // tokenization as a service:
    methods.put("/tokenize", p -> Parameters.parseArray("terms", index.tokenize(p.getString("input"))));
    methods.put("/count", p -> Parameters.parseArray("hits", index.count(getQuery(p))));
  }


  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    IreneAPIServer server = new IreneAPIServer(argp);
    server.run();
  }

  Parameters getArguments(Parameters input) {
    return argp;
  }

  private Cache<String, Document> docCache = Caffeine.newBuilder().maximumSize(250_000).build();

  Parameters getDocuments(Parameters input) {
    boolean countsOnly = input.get("countsOnly", false);
    boolean tokenized = input.get("tokenized", false);
    String termDelimiter = input.get("delim", "|");
    List<String> names = input.getAsList("input", Object.class).stream().map(Object::toString).collect(Collectors.toList());

    Parameters resp = Parameters.create();

    Parameters found = Parameters.create();
    resp.put("found", found);

    for (String name : names) {
      Parameters docR = Parameters.create();
      Document fields = docCache.get(name, missing -> index.doc(index.documentByName(name)));
      if(fields == null) {
        resp.extendList("missing", name);
        continue;
      }

      String body = fields.get("body");
      if(countsOnly) {
        List<String> terms = index.tokenize(body);
        Map<String, Integer> counts = new HashMap<>(terms.size()/3);
        for (String term : terms) {
          counts.compute(term, (key, prevCount) -> prevCount == null ? 1 : prevCount+1);
        }
        docR.put("counts", Parameters.wrap(counts));
      } else {
        if (tokenized) {
          List<String> terms = index.tokenize(body);
          docR.put("terms", StrUtil.join(terms, termDelimiter));
        } else {
          docR.put("body", body);
        }
      }

      for (String fname : this.fields) {
        docR.put(fname, JSONUtil.parseString(fields.getField(fname).stringValue()));
      }
      found.put(name, docR);
    }

    return resp;
  }

  private Cache<Parameters, Parameters> searchCache = Caffeine.newBuilder().maximumSize(10_000).build();

  public QExpr getQuery(Parameters input) throws IOException {
    if (input.isMap("q")) {
      Parameters qjson = input.getMap("q");
      return QExpr.parseJSON(qjson); // parse query JSON, probably POSTed here.
      // -- full control now available from other languages, including python, JS, etc.
    } else {
      String query = input.getAsString("q");
      return index.simpleQuery(input.get("model", "combine"), query);
    }
  }

  Parameters search(Parameters input) throws IOException {
    return searchCache.get(input, missing -> {
      try {
        Parameters output = Parameters.create();

        final int requested = input.get("n", 1000);
        QExpr qexpr = getQuery(input);

        output.put("qexpr", qexpr.toString());
        output.put("qjson", qexpr.toJSON());
        TopDocs results = index.search(qexpr, requested);

        output.put("totalHits", results.totalHits);
        ArrayList<Parameters> docInfos = new ArrayList<>(results.scoreDocs.length);
        output.put("results", docInfos);

        for (ScoreDoc sdoc : results.scoreDocs) {
          Parameters docR = Parameters.create();
          String name = index.getField(sdoc.doc, "id");
          docR.put("doc", name);
          docR.put("score", sdoc.score);
          docInfos.add(docR);
        }

        return output;
      } catch (IOException ioe) {
        throw new RuntimeException(ioe);
      }
    });
  }

  private Cache<String, IreneCountStatistics> statsCache = Caffeine.newBuilder().maximumSize(100_000).build();

  Parameters termStats(Parameters input) {
    List<String> terms = input.getAsList("input", Object.class).stream().map(Object::toString).collect(Collectors.toList());
    Parameters stats = Parameters.create();
    Parameters words = Parameters.create();

    for (String term : terms) {
      IreneCountStatistics ts = statsCache.get(term, index::getTermStatistics);
      if(ts != null) {
        stats.put("collectionLength", ts.collectionLength);
        stats.put("numDocs", ts.totalDocumentCount);
        stats.put("averageDocumentLength", ts.averageDocumentLength());

        Parameters wordInfo = Parameters.create();
        wordInfo.put("df", ts.documentFrequency);
        wordInfo.put("cf", ts.collectionFrequency);
        words.put(term, wordInfo);
      }
    }

    Parameters output = Parameters.create();
    output.put("stats", stats);
    output.put("words", words);
    return output;
  }

  @Override
  public void close() throws IOException {
    index.close();
  }

  @Override
  public void run() {
    WebServer server = JSONAPI.start(argp, methods);
    server.join();
  }
}
