package edu.umass.cs.jfoley.irene.galago;

import ciir.jfoley.chai.IntMath;
import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import org.apache.lucene.index.LeafReaderContext;
import org.lemurproject.galago.core.index.stats.FieldStatistics;
import org.lemurproject.galago.core.index.stats.IndexPartStatistics;
import org.lemurproject.galago.core.index.stats.NodeStatistics;
import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.retrieval.Results;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.NodeType;
import org.lemurproject.galago.core.retrieval.query.QueryType;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.core.tokenize.Tokenizer;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jfoley on 4/17/16.
 */
public class IreneRetrieval implements Retrieval {
    private IreneIndex index;
    private Parameters globalP;
    private IreneTokenizer tokenizer;

    public IreneRetrieval(String path) throws IOException {
        this(path, Parameters.create());
    }
    public IreneRetrieval(String path, Parameters params) throws IOException {
        index = new IreneIndex(path);
        globalP = params;
        tokenizer = new IreneTokenizer();
    }

    @Override
    public void close() throws IOException {
        index.close();
    }

    @Override
    public Parameters getGlobalParameters() {
        return globalP;
    }

    @Override
    public Parameters getAvailableParts() throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Document getDocument(String s, Document.DocumentComponents documentComponents) throws IOException {
        int id = index.documentByName(s);
        if(id < 0) return null;

        Document gdoc = new Document();
        org.apache.lucene.document.Document doc = index.doc(id);
        if(doc == null) return null;
        gdoc.name = s;
        gdoc.identifier = id;
        gdoc.text = doc.get("body");

        if(documentComponents.tokenize) {
            tokenizer.tokenize(gdoc);
        }

        return gdoc;
    }

    @Override
    public Long getDocumentId(String s) throws IOException {
        return (long) index.documentByName(s);
    }

    @Override
    public Map<String, Document> getDocuments(List<String> list, Document.DocumentComponents documentComponents) throws IOException {
        HashMap<String, Document> docs = new HashMap<>();
        for (String name : list) {
            Document document = getDocument(name, documentComponents);
            if(document == null) continue;
            docs.put(name, document);
        }
        return docs;
    }

    @Override
    public NodeType getNodeType(Node node) throws Exception {
        return null;
    }

    @Override
    public QueryType getQueryType(Node node) throws Exception {
        return null;
    }

    @Override
    public Node transformQuery(Node node, Parameters parameters) throws Exception {
        return null;
    }

    @Override
    public Results executeQuery(Node node) throws Exception {
        return null;
    }

    @Override
    public Results executeQuery(Node node, Parameters parameters) throws Exception {
        return null;
    }

    @Override
    public IndexPartStatistics getIndexPartStatistics(String s) throws IOException {
        return null;
    }

    @Override
    public FieldStatistics getCollectionStatistics(String s) throws Exception {
        return getCollectionStatistics(StructuredQuery.parse(s));
    }

    @Override
    public FieldStatistics getCollectionStatistics(Node node) throws Exception {
        return null;
    }

    @Override
    public NodeStatistics getNodeStatistics(String s) throws Exception {
        return getNodeStatistics(StructuredQuery.parse(s));
    }

    @Override
    public NodeStatistics getNodeStatistics(Node node) throws Exception {
        QExpr expr = GalagoToIreneQuery.transpose(node);
        IreneCountStatistics ics = index.computeCountStatistics(expr);
        NodeStatistics stats = new NodeStatistics();
        stats.node = node.toPrettyString();
        stats.nodeDocumentCount = ics.documentFrequency;
        stats.nodeFrequency = ics.collectionFrequency;
        stats.maximumCount = -1; // TODO: estimate?
        return stats;
    }

    @Override
    public Integer getDocumentLength(Long id) throws IOException {
        if(id == null || id < 0) return null;
        for (LeafReaderContext leafReaderContext : index.reader.leaves()) {
            try {
                long length = leafReaderContext.reader().getNormValues("body").get(id.intValue());
                return IntMath.fromLong(length);
            } catch (Exception e) {
                continue;
            }
        }
        return null;
    }

    @Override
    public Integer getDocumentLength(String s) throws IOException {
        return getDocumentLength((long) index.documentByName(s));
    }

    @Override
    public String getDocumentName(Long docId) throws IOException {
        return index.getField(docId.intValue(), "id");
    }

    @Override
    public void addNodeToCache(Node node) throws Exception { }

    @Override
    public void addAllNodesToCache(Node node) throws Exception { }

    @Override
    public Tokenizer getTokenizer() {
        return tokenizer;
    }
}
