package edu.umass.cs.jfoley.irene.expr;

import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.lang.UserData;
import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.expr.macro.WeightMacroChain;
import edu.umass.cs.jfoley.irene.expr.types.InputTypeChecker;
import edu.umass.cs.jfoley.irene.index.fields.BooleanField;
import org.apache.lucene.index.Term;
import org.lemurproject.galago.utility.Parameters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * @author jfoley
 */
public class QExpr extends UserData {
  public final static String DEFAULT_KEY = "default";
  public String operator;
  public List<QExpr> children = new ArrayList<>();
  public Parameters config = Parameters.create();

  public QExpr(String operator) {
    this.operator = operator;
  }

  public QExpr(String operator, String defaultValue) {
    this.operator = operator;
    this.config.put(DEFAULT_KEY, defaultValue);
  }

  public QExpr(String operator, QExpr firstChild) {
    this.operator = operator;
    this.children.add(firstChild);
  }

  public QExpr(String operator, List<QExpr> seq) {
    this.operator = operator;
    this.children = ListFns.map(seq, QExpr::copy);
  }

  public QExpr(String operator, Parameters config, List<QExpr> children) {
    this.operator = operator;
    this.config = config;
    this.children = children;
  }

  public QExpr(Term term) {
    this.operator = "text";
    this.config.put(DEFAULT_KEY, term.text());
    this.config.put("field", term.field());
  }

  public String getDefault() {
    return config.getAsString(DEFAULT_KEY);
  }

  private boolean hasDefault() {
    return config.containsKey(DEFAULT_KEY);
  }

  public static QExpr create(String operator) {
    return new QExpr(operator);
  }

  public static QExpr text(String term) {
    return new QExpr("text", term);
  }

  /** Chainable addChild. */
  public QExpr addChild(QExpr child) {
    if(child == this) {
      throw new IllegalArgumentException("Tried to build a query loop! Must be a tree, not a cyclic graph.");
    }
    this.children.add(child);
    return this;
  }
  public QExpr addChildren(Collection<? extends QExpr> children) {
    for (QExpr child : children) {
      addChild(child);
    }
    return this;
  }

  public Parameters toJSON() {
    return Parameters.parseArray(
        "operator", operator,
        "config", config,
        "children", ListFns.map(children, QExpr::toJSON)
    );
  }

  public static QExpr parseJSON(Parameters qjson) {
    String operator = qjson.getString("operator");
    Parameters config = qjson.get("config", Parameters.create());

    List<Parameters> childJSON = qjson.getAsList("children", Parameters.class);
    List<QExpr> children = new ArrayList<>(childJSON.size());
    for (Parameters child : childJSON) {
      children.add(parseJSON(child));
    }
    return new QExpr(operator, config, children);
  }

  public String toString() {
    // deep to-string
    //return toJSON().toPrettyString();
    StringBuilder out = new StringBuilder();
    toSimpleString(out);
    return out.toString();
  }

  private void toSimpleString(StringBuilder out) {
    out.append('#').append(operator);

    if(config.containsKey("field")) {
      out.append('.').append(config.getAsString("field"));
    }

    if(this.hasDefault()) {
      out.append(':').append(this.getDefault());
    }

    out.append('(');
    for (int i = 0; i < children.size(); i++) {
      QExpr child = children.get(i);
      if(i > 0) {
        out.append(' ');
      }
      child.toSimpleString(out);
    }
    out.append(')');
  }

  @Override
  public int hashCode() {
    // ouch, recursive:
    return operator.hashCode() ^ children.hashCode();
  }

  @Override
  public boolean equals(Object other) {
    if(other instanceof QExpr) {
      QExpr rhs = (QExpr) other;
      if(!rhs.operator.equals(operator)) return false;
      if(rhs.children.size() != children.size()) return false;
      if(!rhs.config.equals(config)) return false;
      for (int i = 0; i < children.size(); i++) {
        // recurse, depth first:
        if(!children.get(i).equals(rhs.children.get(i))) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  @Deprecated
  public IreneQueryModel toModel(IreneIndex index) {
    return new IreneQueryModel(index, this);
  }

  public QExpr copy() {
    ArrayList<QExpr> newChildren = new ArrayList<>(this.children.size());
    newChildren.addAll(this.children);
    return copy(newChildren);
  }

  public QExpr deepCopy() {
    ArrayList<QExpr> newChildren = new ArrayList<>(this.children.size());
    for (QExpr child : this.children) {
      newChildren.add(child.deepCopy());
    }
    return copy(newChildren);
  }

  public QExpr copy(List<QExpr> newChildren) {
    QExpr clone = new QExpr(this.operator);
    clone.userData = new HashMap<>(this.userData);
    clone.config.copyFrom(this.config);
    clone.children = newChildren;
    return clone;
  }

  public QExpr setConfig(Object... kv) {
    if(kv.length % 2 != 0) {
      throw new IllegalArgumentException("Need even kv pairs for setConfig(...) call...");
    }
    config.copyFrom(Parameters.parseArray(kv));
    return this;
  }

  public static QExpr fieldMatch(String field, String value) {
    QExpr q = new QExpr("text", value);
    q.config.set("field", field);
    return q;
  }

  public static QExpr intMatch(String field, int value) {
    QExpr q = new QExpr("int-range");
    q.config.set("field", field);
    q.config.set("min", value);
    q.config.set("max", value);
    return q;
  }

  public static QExpr mustBeTrue(String field) {
    return fieldMatch(field, BooleanField.TRUE);
  }

  public static QExpr require(QExpr cond, QExpr score) {
    return new QExpr("require")
        .addChild(cond)
        .addChild(score);
  }

  public boolean isLeaf() {
    return children.size() == 0;
  }

  public InputTypeChecker.RequiredType getTypeParameter(InputTypeChecker.RequiredType ifNotSpecified) {
    final String type = this.config.get("type", (String) null);
    if(type == null) return ifNotSpecified;
    switch (type) {
      case "scores":
        return InputTypeChecker.scores;
      case "counts":
        return InputTypeChecker.counts;
      case "bools":
        return InputTypeChecker.bools;
      case "positions":
      case "extents":
        return InputTypeChecker.positions;
      default:
        throw new UnsupportedOperationException("Unknown type="+type);
    }
  }

  public int size() {
    return children.size();
  }

  /**
   * Here, we assume that weights have been processed, or you're getting raw weights.
   * @return weights for child nodes:
   */
  public float[] getTransformedWeightVector() {
    if(!this.hasUserData(WeightMacroChain.Completed.class)) {
      throw new IllegalStateException("Attempt to access TransformedWeightVector without first transforming the query.");
    }
    int N = this.children.size();
    float[] weights = new float[N];
    for (int i = 0; i < N; i++) {
      weights[i] = (float) this.config.getDouble(Integer.toString(i));
    }
    return weights;
  }

  public QExpr setWeight(double v) {
    config.set("weight", v);
    return this;
  }

  public QExpr setOperator(String operator) {
    this.operator = operator;
    return this;
  }

  public static void main(String[] args) {
    QExpr j =
        new QExpr("require")
            .addChild(QExpr.fieldMatch("prefixes", "_"))
            .addChild(new QExpr("combine")
                .addChild(
                    new QExpr("dirichlet")
                        .setConfig("mu", 50)
                        .addChild(QExpr.fieldMatch("field", "term"))
                ));
    System.out.println(j.toJSON().toPrettyString());
  }
}
