package edu.umass.cs.jfoley.irene.expr.types;

import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.string.StrUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley
 */
@FunctionalInterface
public interface InputTypeChecker {

  interface TypeCheckResult {
    boolean success();
  }
  final class TypeCheckSuccess implements TypeCheckResult {
    @Override
    public final boolean success() { return true; }
  }
  abstract class TypeCheckError implements TypeCheckResult {
    public final boolean success() { return false; }
    public abstract String toString();
  }
  class TypeMismatch {
    public final int position;
    public final OperatorType found;
    public final OperatorType required;

    public TypeMismatch(int position, OperatorType found, OperatorType required) {
      this.position = position;
      this.found = found;
      this.required = required;
    }

    @Override
    public String toString() {
      return "["+position+"]: Expected="+required+" Actual: "+found;
    }
  }
  class TypeCheckMismatches extends TypeCheckError {
    public final List<TypeMismatch> errors;

    public TypeCheckMismatches(List<TypeMismatch> errors) {
      this.errors = errors;
    }

    @Override
    public String toString() {
      return StrUtil.join(ListFns.map(errors, TypeMismatch::toString));
    }
  }
  class IncorrectNumberOfArguments extends TypeCheckError {
    final int expectedNumArgs;
    final int actualNumArgs;

    public IncorrectNumberOfArguments(int expectedNumArgs, int actualNumArgs) {
      this.expectedNumArgs = expectedNumArgs;
      this.actualNumArgs = actualNumArgs;
    }

    @Override
    public String toString() {
      return "Expected "+expectedNumArgs+" arguments, found: "+actualNumArgs;
    }
  }
  class NeedsAtLeastArguments extends TypeCheckError {
    final int number;

    public NeedsAtLeastArguments(int number) {
      this.number = number;
    }

    @Override
    public String toString() {
      return "Expected "+number+" or more arguments.";
    }
  }

  TypeCheckResult satisfies(List<OperatorType> inputTypes);

  interface RequiredType {
    boolean satisfies(OperatorType type);
    OperatorType wouldSatify();
  }

  RequiredType bools = new RequiredType() {
    @Override
    public boolean satisfies(OperatorType type) {
      return true;
    }

    @Override
    public OperatorType wouldSatify() {
      return OperatorType.BOOLS;
    }
  };
  RequiredType counts = new RequiredType() {
    @Override
    public boolean satisfies(OperatorType type) {
      return (type == OperatorType.COUNTS) || (type == OperatorType.POSITIONS);
    }

    @Override
    public OperatorType wouldSatify() {
      return OperatorType.COUNTS;
    }
  };
  RequiredType positions = new RequiredType() {
    @Override
    public boolean satisfies(OperatorType type) {
      return type == OperatorType.POSITIONS;
    }

    @Override
    public OperatorType wouldSatify() {
      return OperatorType.POSITIONS;
    }
  };
  RequiredType scores = new RequiredType() {
    @Override
    public boolean satisfies(OperatorType type) {
      return type == OperatorType.SCORES;
    }

    @Override
    public OperatorType wouldSatify() {
      return OperatorType.SCORES;
    }
  };

  // For operators that don't take any input, like #text(), or #pagerank()
  InputTypeChecker NoInput = inputTypes ->
      inputTypes.isEmpty() ?
          new TypeCheckSuccess() :
          new IncorrectNumberOfArguments(0, inputTypes.size());

  // For operators that require exactly N inputs of a given type, like #dirichlet(1)
  class ExactlyN implements InputTypeChecker {
    final RequiredType typeCheck;
    final int n;

    public ExactlyN(RequiredType typeCheck, int n) {
      this.typeCheck = typeCheck;
      this.n = n;
    }

    @Override
    public TypeCheckResult satisfies(List<OperatorType> inputTypes) {
      if(inputTypes.size() != n) {
        return new IncorrectNumberOfArguments(n, inputTypes.size());
      }

      List<TypeMismatch> errors = new ArrayList<>();
      for (int i = 0; i < inputTypes.size(); i++) {
        OperatorType inputType = inputTypes.get(i);
        if (!typeCheck.satisfies(inputType)) {
          errors.add(new TypeMismatch(i, inputType, typeCheck.wouldSatify()));
        }
      }
      if(errors.isEmpty()) return new TypeCheckSuccess();
      return new TypeCheckMismatches(errors);
    }
  }

  class OneOrMore implements InputTypeChecker {
    final RequiredType typeCheck;

    public OneOrMore(RequiredType typeCheck) {
      this.typeCheck = typeCheck;
    }

    @Override
    public TypeCheckResult satisfies(List<OperatorType> inputTypes) {
      // check size:
      if(inputTypes.size() < 1) {
        return new NeedsAtLeastArguments(1);
      }

      // check types:
      List<TypeMismatch> errors = new ArrayList<>();
      for (int i = 0; i < inputTypes.size(); i++) {
        OperatorType inputType = inputTypes.get(i);
        if (!typeCheck.satisfies(inputType)) {
          errors.add(new TypeMismatch(i, inputType, typeCheck.wouldSatify()));
        }
      }

      if(errors.isEmpty()) return new TypeCheckSuccess();
      return new TypeCheckMismatches(errors);
    }
  }

  class XThenY implements InputTypeChecker {
    final RequiredType xType;
    final RequiredType yType;

    public XThenY(RequiredType xType, RequiredType yType) {
      this.xType = xType;
      this.yType = yType;
    }

    @Override
    public TypeCheckResult satisfies(List<OperatorType> inputTypes) {
      if(inputTypes.size() != 2) {
        return new IncorrectNumberOfArguments(2, inputTypes.size());
      }

      List<TypeMismatch> mismatches = new ArrayList<>();
      if(!xType.satisfies(inputTypes.get(0))) mismatches.add(new TypeMismatch(0, inputTypes.get(0), xType.wouldSatify()));
      if(!yType.satisfies(inputTypes.get(1))) mismatches.add(new TypeMismatch(1, inputTypes.get(1), yType.wouldSatify()));
      if(mismatches.isEmpty()) return new TypeCheckSuccess();
      return new TypeCheckMismatches(mismatches);
    }
  }

  public class KOrMore implements InputTypeChecker {
    final RequiredType typeCheck;
    private final int k;

    public KOrMore(RequiredType typeCheck, int k) {
      this.typeCheck = typeCheck;
      this.k = k;
    }

    @Override
    public TypeCheckResult satisfies(List<OperatorType> inputTypes) {
      // check size:
      if(inputTypes.size() < k) {
        return new NeedsAtLeastArguments(k);
      }

      // check types:
      List<TypeMismatch> errors = new ArrayList<>();
      for (int i = 0; i < inputTypes.size(); i++) {
        OperatorType inputType = inputTypes.get(i);
        if (!typeCheck.satisfies(inputType)) {
          errors.add(new TypeMismatch(i, inputType, typeCheck.wouldSatify()));
        }
      }

      if(errors.isEmpty()) return new TypeCheckSuccess();
      return new TypeCheckMismatches(errors);
    }
  }
}
