package edu.umass.cs.jfoley.irene.eval.iters;

import org.apache.lucene.search.DocIdSetIterator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley.
 */
public class AndDocIdSetIterator extends DocIdSetIterator {
  final ArrayList<DocIdSetIterator> children;
  int current;
  final long cost;

  public AndDocIdSetIterator(List<DocIdSetIterator> children) {
    this.children = new ArrayList<>(children);
    // lowest-cost first:
    children.sort((lhs, rhs) -> Long.compare(lhs.cost(), rhs.cost()));

    this.current = 0;
    // figure out current position and cost:
    int id = NO_MORE_DOCS;
    long minCost = 0L;
    for (DocIdSetIterator child : children) {
      id = Math.min(child.docID(), id);
      minCost = Math.min(child.cost(), minCost);
    }

    this.cost = minCost;
    this.current = id;
  }

  private int advanceToMatch() throws IOException {
    while(true) {
      boolean match = true;
      for (DocIdSetIterator child : children) {
        int cpos = child.docID();
        if(cpos < current) {
          cpos = child.advance(current);
          if(cpos == NO_MORE_DOCS) return NO_MORE_DOCS;
        }
        // reset target:
        if(cpos > current) {
          current = cpos;
          match = false;
          break;
        }
      }

      if(match) {
        return current;
      }
    }
  }

  @Override
  public int docID() {
    return current;
  }

  @Override
  public int nextDoc() throws IOException {
    return advance(current+1);
  }

  @Override
  public int advance(int target) throws IOException {
    current = target;
    return advanceToMatch();
  }

  @Override
  public long cost() {
    return cost;
  }
}
