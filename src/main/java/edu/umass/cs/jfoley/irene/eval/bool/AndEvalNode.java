package edu.umass.cs.jfoley.irene.eval.bool;

import edu.umass.cs.jfoley.irene.eval.QueryEvalNode;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;

import java.io.IOException;
import java.util.List;

/**
 * @author jfoley.
 */
@QueryOperator(value={"and","all"})
public class AndEvalNode extends BoolEval {
  public AndEvalNode(List<QueryEvalNode> children) {
    super(children);
    this.children.sort(leastToMostFrequent);
  }

  private int __matching = -1;

  @Override
  public boolean matches(int id) throws IOException {
    if(__matching == id) return true;
    if(rawMatches(id)) {
      __matching = id;
      return true;
    }
    return false;
  }

  private boolean rawMatches(int id) throws IOException {
    for (QueryEvalNode child : children) {
      if(!child.matches(id)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createAndMover(children);
  }


}
