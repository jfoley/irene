package edu.umass.cs.jfoley.irene.galago;

import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.NodeParameters;
import org.lemurproject.galago.core.retrieval.traversal.Traversal;
import org.lemurproject.galago.utility.Parameters;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jfoley on 4/17/16.
 */
public class GalagoToIreneQuery {
    public static QExpr transpose(Node galago) {
        String op = galago.getOperator();
        QExpr irene = null;
        switch (op) {
            case "lengths":
                return null;
            default:
                irene = new QExpr(op);
        }

        Parameters dest = irene.config;
        NodeParameters np = galago.getNodeParameters();
        np.getKeySet().forEach(key -> {
            if(np.isBoolean(key)) {
                dest.put(key, np.getBoolean(key));
            } else if(np.isDouble(key)) {
                dest.put(key, np.getDouble(key));
            } else if(np.isLong(key)) {
                dest.put(key, np.getLong(key));
            } else if(np.isString(key)) {
                dest.put(key, np.getString(key));
            } else {
                throw new IllegalArgumentException();
            }
        });

        if(galago.numChildren() > 0) {
            for (QExpr qExpr : transposeChildren(galago.getInternalNodes())) {
                if(qExpr == null) continue;
                irene.addChild(qExpr);
            }
        }
        return irene;
    }

    private static List<QExpr> transposeChildren(List<Node> children) {
        ArrayList<QExpr> newChildren = new ArrayList<>();
        for (int i = 0; i < children.size(); i++) {
            newChildren.add(transpose(children.get(i)));
        }
        return newChildren;
    }
}
