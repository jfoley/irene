package edu.umass.cs.jfoley.irene;

import ciir.jfoley.chai.web.json.JSONAPI;
import ciir.jfoley.chai.web.json.JSONMethod;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.*;

/**
 * @author jfoley
 */
public class JSONSearch {

  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);

    Map<String, JSONMethod> actions = new HashMap<>();
    IreneIndex index = new IreneIndex(argp.getString("index"));

    Parameters pFields = argp.get("fields", Parameters.create());
    Set<String> fieldsToLoad = new HashSet<>();
    Map<String, String> fieldKinds = new HashMap<>();

    fieldKinds.put("id", "string");
    fieldKinds.put("body", "body");
    fieldsToLoad.add("id");

    for (String key : pFields.keySet()) {
      Parameters defn = pFields.getMap(key);
      if(defn.get("alwaysPull", false)) {
        fieldsToLoad.add(key);
      }
      String kind = defn.getString("kind");
      fieldKinds.put(key, kind);
    }

    actions.put("/search", (p) -> {
      Parameters output = Parameters.create();
      String qtext = p.getString("q");
      int count = p.get("n", 10);

      QExpr query = index.simpleQuery(p.get("model", "combine"), qtext);
      long start = System.currentTimeMillis();
      TopDocs topDocs = index.search(query, count);
      long end = System.currentTimeMillis();

      output.put("searchTime", (end-start));
      output.put("count", count);
      output.put("query", query.toString());
      output.put("totalHits", topDocs.totalHits);

      Set<String> fieldsRequested = new HashSet<>(fieldsToLoad);
      fieldsRequested.addAll(p.getAsList("fields", String.class));
      System.out.println(fieldsRequested);

      output.put("_fields", new ArrayList<>(fieldsRequested));

      List<Parameters> docs = new ArrayList<>();
      start = System.currentTimeMillis();
      ScoreDoc[] scoreDocs = topDocs.scoreDocs;
      for (int i = 0; i < scoreDocs.length; i++) {
        ScoreDoc scoreDoc = scoreDocs[i];
        Document doc = index.searcher.doc(scoreDoc.doc, fieldsRequested);
        System.out.println(doc.getFields());
        Parameters docP = Parameters.parseArray("rank", i+1, "score", scoreDoc.score, "num", scoreDoc.doc);

        docP.copyFrom(luceneDocToJSON(doc, fieldKinds));

        docs.add(docP);
      }
      end = System.currentTimeMillis();
      output.put("nameTime", (end - start));

      output.put("docs", docs);
      return output;
    });

    actions.put("/doc", (p) -> {
      Parameters output = Parameters.create();

      int id = index.documentByName(p.getString("id"));
      Document doc = index.doc(id);
      if(doc == null) return output;
      return luceneDocToJSON(doc, fieldKinds);
    });

    actions.put("/docByNum", (p) -> {
      Parameters output = Parameters.create();
      Document doc = index.doc(p.getInt("num"));
      if(doc != null) {
        return luceneDocToJSON(doc, fieldKinds);
      }
      return output;
    });

    JSONAPI.start(argp.get("port", 1235), actions);
  }

  public static Parameters luceneDocToJSON(@Nonnull Document doc, Map<String, String> fieldKinds) {
    Parameters output = Parameters.create();

    for (IndexableField indexableField : doc.getFields()) {
      String key = indexableField.name();
      String kind = fieldKinds.get(key);
      switch (kind) {
        default:
        case "text":
        case "string":
          output.putIfNotNull(key, indexableField.stringValue());
          break;
        case "long":
        case "int":
        case "float":
        case "double":
        case "number":
          output.putIfNotNull(key, indexableField.numericValue());
          break;
      }
    }

    return output;
  }
}
