package edu.umass.cs.jfoley.irene.util;

import ciir.jfoley.chai.time.Debouncer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.jsoup.Jsoup;
import org.lemurproject.galago.core.index.corpus.CorpusReader;
import org.lemurproject.galago.core.index.disk.DiskIndex;
import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author jfoley
 */
public class IndexFromGalago {
  public static final Logger logger = Logger.getLogger(IndexFromGalago.class.getName());
  public static void main(String[] args) throws Exception {
    Parameters argp = Parameters.parseArgs(args);

    logger.setUseParentHandlers(false);
    FileHandler lfh = new FileHandler("indexing-errors.log");
    SimpleFormatter formatter = new SimpleFormatter();
    lfh.setFormatter(formatter);
    logger.addHandler(lfh);

    final DiskIndex index = new DiskIndex(argp.get("index", "/mnt/scratch3/jfoley/w3c.galago"));
    final CorpusReader corpus = (CorpusReader) index.getIndexPart("corpus");
    long total = corpus.getManifest().getLong("keyCount");
    final CorpusReader.KeyIterator iterator = corpus.getIterator();

    final boolean shouldJSoup = argp.get("jsoup", true);

    final Document.DocumentComponents dcp = Document.DocumentComponents.JustText;

    AtomicLong count = new AtomicLong(0);

    Debouncer msg = new Debouncer();
    try (final FSDirectory dir = FSDirectory.open(Paths.get(argp.get("output", "/mnt/scratch3/jfoley/w3c.irene")))) {
      final IndexWriterConfig cfg = new IndexWriterConfig(new EnglishAnalyzer());
      System.out.println("Similarity: "+cfg.getSimilarity());
      cfg.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        try (IndexWriter writer = new IndexWriter(dir, cfg)) {
          iterator.forAllKeyStrings(docId -> {
            try {
              Document document = iterator.getDocument(dcp);

              String text = document.text;
              if (shouldJSoup) {
                org.jsoup.nodes.Document jsoup = Jsoup.parse(text);
                text = jsoup.text();
              }

              String id = document.name;

              org.apache.lucene.document.Document doc = new org.apache.lucene.document.Document();
              doc.add(new StringField("id", id, Field.Store.YES));
              doc.add(new TextField("body", text, Field.Store.YES));

              try {
                writer.addDocument(doc);
              } catch (IOException e) {
                logger.log(Level.WARNING, "Pull-Document-Exception", e);
              }

              long curCount = count.incrementAndGet();
              if (msg.ready()) {
                System.err.println("L.Indexing: " + msg.estimate(curCount, total));
              }
            } catch (Exception e) {
              logger.log(Level.WARNING, "Pull-Document-Exception", e);
            }
          });

        }
      }

    long curCount = count.get();
    System.err.println("L.Indexing.Done " + msg.estimate(curCount, total));
  }
}
