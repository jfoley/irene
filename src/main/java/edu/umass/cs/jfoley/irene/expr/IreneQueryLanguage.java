package edu.umass.cs.jfoley.irene.expr;

import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.diskmap.NullAnalyzer;
import edu.umass.cs.jfoley.irene.eval.*;
import edu.umass.cs.jfoley.irene.eval.bool.AndEvalNode;
import edu.umass.cs.jfoley.irene.eval.bool.MajorityEvalNode;
import edu.umass.cs.jfoley.irene.eval.bool.OrEvalNode;
import edu.umass.cs.jfoley.irene.eval.leaf.*;
import edu.umass.cs.jfoley.irene.eval.opt.*;
import edu.umass.cs.jfoley.irene.expr.macro.*;
import edu.umass.cs.jfoley.irene.expr.types.InputTypeChecker;
import edu.umass.cs.jfoley.irene.expr.types.OperatorType;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.util.*;

/**
 * @author jfoley
 */ // If you want to add more operators, you'll have to extend this class.
public class IreneQueryLanguage {
  public String defaultField = "body";

  public final Map<String, String> canonicalOperators = new HashMap<>();
  Map<String, DataNeeded> dataNeededMap = new HashMap<>();
  Map<String, Class<? extends QueryEvalNode>> operatorMapping = new HashMap<>();
  private IreneIndex index;
  public QueryParser luceneQueryParser;
  private MacroChain macro;

  public IreneQueryLanguage() {

  }
  public IreneQueryLanguage(IreneIndex index, Parameters argp) {
    this.index = index;
    for (Class<? extends QueryEvalNode> defaultOperator : defaultOperators) {
      register(defaultOperator);
    }
    luceneQueryParser = new QueryParser(defaultField, index != null ? index.analyzer : new NullAnalyzer());
    macro = new MacroChain(argp, Arrays.asList(
        new MakeCanonicalOperators(argp),
        WeightMacroChain.init(argp),
        new SDMMacro(argp),
        new SingleChildCollapser(argp),
        new BooleanCompileMacro(argp),
        new InsertScorersMacro(argp)));
    macro.setFactory(this);
  }

  public static final String getOperator(QExpr node) {
    return node.operator;
    //return canonicalOperators.getOrDefault(node.operator, node.operator);
  }

  public static InputTypeChecker getInputRequirements(QExpr node) {
    switch (getOperator(node)) {
      case "require":
        return new InputTypeChecker.XThenY(InputTypeChecker.bools, node.getTypeParameter(InputTypeChecker.scores));
      case "category":
      case "text":
      case "extents":
      case "counts":
      case "lucene-match":
      case "lucene-count":
      case "lucene-score":
      case "int-range":
        return InputTypeChecker.NoInput;
      case "importance":
      case "and":
      case "or":
        return new InputTypeChecker.KOrMore(InputTypeChecker.bools, 2);
      case "bdql":
        return new InputTypeChecker.OneOrMore(InputTypeChecker.bools);
      case "majority": // majority(2) is all...
        return new InputTypeChecker.KOrMore(InputTypeChecker.bools, 3);
      case "od":
      case "uw":
        return new InputTypeChecker.OneOrMore(InputTypeChecker.positions);
      case "bm25":
      case "dirichlet":
      case "additive":
      case "mle":
      case "linear":
      case "to-score":
        return new InputTypeChecker.ExactlyN(InputTypeChecker.counts, 1);
      case "combine":
        return new InputTypeChecker.OneOrMore(InputTypeChecker.scores);
      case "syn":
        return new InputTypeChecker.KOrMore(InputTypeChecker.positions, 2);
      case "dql":
      case "optbm25":
      case "countsum":
        return new InputTypeChecker.OneOrMore(InputTypeChecker.counts);
      default:
        // TODO, attempt reflection instantiation...
        throw new UnsupportedOperationException("Unknown operator=" + getOperator(node) + " for query: " + node);
    }
  }

  public OperatorType getOutputType(QExpr node) {
    switch (getOperator(node)) {
      case "and":
      case "or":
      case "majority":
      case "lucene-match":
      case "category":
      case "int-range":
        return OperatorType.BOOLS;
      case "text":
      case "extents":
      case "syn":
        return OperatorType.POSITIONS;
      case "od":
      case "uw":
      case "countsum":
      case "counts":
      case "lucene-count":
        return OperatorType.COUNTS;
      case "bm25":
      case "dirichlet":
      case "additive":
      case "mle":
      case "linear":
      case "to-score":
      case "combine":
      case "dql":
      case "optbm25":
      case "bdql":
      case "lucene-score":
      case "importance":
        return OperatorType.SCORES;
      default:
        // TODO, attempt reflection instantiation...
        throw new UnsupportedOperationException("Unknown operator=" + getOperator(node) + " for query: " + node);
    }
  }

  public List<LuceneScorerRequest> collectQueryUsage(QExpr query) {
    ArrayList<LuceneScorerRequest> usage = new ArrayList<>();
    collectQueryUsage(query, usage);
    return usage;
  }

  public LuceneScorerRequest luceneQuery(Query builtQuery) {
    String queryText = builtQuery.toString();
    final Query rewrite;
    try {
      rewrite = index.searcher.rewrite(builtQuery);
    } catch (IOException e) {
      throw new RuntimeException("Can't rewrite built query="+builtQuery, e);
    }
    return new LuceneScorerRequest(queryText, builtQuery, rewrite);
  }
  public LuceneScorerRequest luceneQuery(String query) {
    final Query parsed;
    try {
      parsed = luceneQueryParser.parse(query);
    } catch (ParseException e) {
      throw new RuntimeException("Can't parse query="+query, e);
    }
    final Query rewrite;
    try {
      rewrite = index.searcher.rewrite(parsed);
    } catch (IOException e) {
      throw new RuntimeException("Can't rewrite query="+query+" parsed="+parsed, e);
    }
    return new LuceneScorerRequest(query, parsed, rewrite);
  }

  public LuceneScorerRequest getLuceneQuery(QExpr input) {
    if(input.hasUserData(LuceneScorerRequest.class)) {
      return input.getUserData(LuceneScorerRequest.class);
    }
    assert(input.isLeaf()) : "only works for leaf queries";
    Parameters cfg = input.config;
    switch (input.operator) {
      case "lucene-match":
      case "lucene-count": {
        LuceneScorerRequest lqr = luceneQuery(input.getDefault());
        lqr.needsScores = false;
        input.setUserData(LuceneScorerRequest.class, lqr);
        return lqr;
      }
      case "lucene-score": {
        LuceneScorerRequest lqr = luceneQuery(input.getDefault());
        input.setUserData(LuceneScorerRequest.class, lqr);
        return lqr;
      }
      case "int-range": {
        String field = cfg.getString("field");
        Integer min = null;
        Integer max = null;
        if(cfg.containsKey("min")) { min = cfg.getInt("min"); }
        if(cfg.containsKey("max")) { max = cfg.getInt("max"); }
        if(min == null && max == null) {
          throw new IllegalArgumentException("Must specify either min or max.");
        }
        NumericRangeQuery nrq = NumericRangeQuery.newIntRange(field, min, max, true, true);
        LuceneScorerRequest lqr = luceneQuery(nrq);
        input.setUserData(LuceneScorerRequest.class, lqr);
        return lqr;
      }
      case "text":
      case "extents":
      case "counts": {
        String token = Objects.requireNonNull(cfg.get("term", cfg.get("token", cfg.get(QExpr.DEFAULT_KEY, (String) null))));
        String field = cfg.get("field", defaultField);
        TermQuery tq = new TermQuery(new Term(field, token));
        return luceneQuery(tq);
      }
      case "category": {
        String val = Objects.requireNonNull(cfg.get("value", cfg.get(QExpr.DEFAULT_KEY, (String) null)));
        String field = cfg.get("field", defaultField);
        TermQuery tq = new TermQuery(new Term(field, val));
        return luceneQuery(tq);
      }
      default:
        throw new IllegalArgumentException("No lucene query for input.");
    }
  }

  private void collectQueryUsage(QExpr input, List<LuceneScorerRequest> output) {
    switch (getOperator(input)) {
      case "lucene-match":
      case "lucene-count":
      case "int-range":
      case "lucene-score":
      {
        final LuceneScorerRequest query = getLuceneQuery(input);
        output.add(query);
      } break;
    }
    for (QExpr child : input.children) {
      collectQueryUsage(child, output);
    }
  }

  public QExpr applyMacros(QExpr expr) {
    return macro.transformNonEmpty(expr);
  }

  public static class QExprTypeCheckError {
    InputTypeChecker.TypeCheckResult typeCheckResult;
    QExpr relevantQuery;

    public QExprTypeCheckError(QExpr node, InputTypeChecker.TypeCheckResult result) {
      this.relevantQuery = node;
      this.typeCheckResult = result;
    }
  }

  void typeCheck(QExpr root) {
    List<QExprTypeCheckError> errors = new ArrayList<>();
    typeCheck(root, errors);

    if(!errors.isEmpty()) {
      System.err.println("TypeChecking Errors Found!");
      for (QExprTypeCheckError error : errors) {
        System.err.println("Node:"+error.relevantQuery+"\tError:"+error.typeCheckResult);
      }
      throw new RuntimeException("TypeChecking Error, Check stderr.");
    }
  }
  private void typeCheck(QExpr node,  List<QExprTypeCheckError> errors) {
    InputTypeChecker checker = getInputRequirements(node);
    List<OperatorType> childOutputs = new ArrayList<>();
    for (QExpr child : node.children) {
      childOutputs.add(getOutputType(child));
    }

    // check types before recursion:
    InputTypeChecker.TypeCheckResult result = checker.satisfies(childOutputs);
    if(!result.success()) {
      errors.add(new QExprTypeCheckError(node, result));
      return;
    }

    // check types recursively:
    for (QExpr child : node.children) {
      typeCheck(child, errors); // recurse.
    }
  }


  public static List<Class<? extends QueryEvalNode>> defaultOperators = Arrays.asList(
       // complete query models:
      BinaryDirichletQLEvalNode.class, // bdql
      DirichletQLEvalNode.class, //dql
      SortedMaxscoreBM25.class, // optbm25
      BinaryImportanceEval.class, //importance

      // scoring operators:
      GalagoBM25EvalNode.class, // bm25
      GalagoCombine.class, // combine
      DirichletSmoothingEvalNode.class, //dirichlet
      AdditiveSmoothingNode.class, // additive, laplace
      LinearSmoothingEvalNode.class, //linear, jm
      CountToScore.class,
      DocumentLikelihoodModel.class, // mle

      // Low-level:
      IreneOrderedWindow.class, // od
      IreneUnorderedWindow.class, //uw
      SynonymEval.class,

      // counts
      CountSum.class, //countsum

      // boolean
      AndEvalNode.class, //and
      OrEvalNode.class, //or
      MajorityEvalNode.class,

      //generic
      RequireEval.class
  );

  public void register(Class<? extends QueryEvalNode> qEvalClass) {
    QueryOperator opNameAnn = qEvalClass.getDeclaredAnnotation(QueryOperator.class);
    if (opNameAnn == null) throw new IllegalArgumentException("Expected @QueryOperator(\"foo\") annotation for class: "+qEvalClass+"!");
    String[] opName = opNameAnn.value();

    TermDataNeeded termDataNeeded = qEvalClass.getDeclaredAnnotation(TermDataNeeded.class);
    if (termDataNeeded != null) {
      for (String op : opName) {
        dataNeededMap.put(op, termDataNeeded.value());
      }
    }

    if(opName.length > 1) {
      String canonical = opName[0];
      for (int i = 1; i < opName.length; i++) {
        String alt = opName[i];
        canonicalOperators.put(alt, canonical);
      }
    }

    for (String op : opName) {
      Class<?> prev = operatorMapping.put(op, qEvalClass);
      assert (prev == null) : "Registered multiple classes for the same operator: #"+op+"! " + prev + " " + qEvalClass;
    }
  }

  public String getTextOrNull(QExpr expr) {
    Parameters cfg = expr.config;
    switch (getOperator(expr)) {
      case "text":
        return cfg.get("term", cfg.get("token", cfg.get(QExpr.DEFAULT_KEY, (String) null)));
      default:
        return null;
    }
  }

  public static class TermRequest {
    final Term term;
    final DataNeeded forWhat;

    @Override
    public String toString() {
     return "TermRequest("+term+", "+forWhat+")";
    }

    @Override
    public int hashCode() {
      return term.hashCode() ^ forWhat.hashCode();
    }

    @Override
    public boolean equals(Object other) {
      if(other instanceof TermRequest) {
        TermRequest rhs = (TermRequest) other;
        return forWhat.equals(rhs.forWhat) && term.equals(rhs.term);
      }
      return false;
    }

    public TermRequest(Term term, DataNeeded forWhat) {
      this.term = term;
      this.forWhat = forWhat;
    }
  }

  public List<TermRequest> collectTermRequests(QExpr input) {
    List<TermRequest> data = new ArrayList<>();
    collectTermRequestsRecursive(input, data, DataNeeded.DOCS);
    return data;
  }
  private void collectTermRequestsRecursive(QExpr input, List<TermRequest> output, DataNeeded whatFor) {
    Parameters cfg = input.config;

    String op = getOperator(input);
    String token = null;
    String field = null;
    if("text".equals(op)) {
      token = Objects.requireNonNull(cfg.get("term", cfg.get("token", cfg.get(QExpr.DEFAULT_KEY, (String) null))));
      field = cfg.get("field", defaultField);
    } else if("category".equals(op)) {
      token = Objects.requireNonNull(cfg.get("value", cfg.get(QExpr.DEFAULT_KEY, (String) null)));
      field = cfg.get("field", defaultField);
    }

    if(token != null) {
      TermRequest termR = new TermRequest(new Term(field, token), whatFor);
      output.add(termR);
      input.setUserData(TermRequest.class, termR);
      assert (input.children.size() == 0) : "Expect that 'term' nodes have no children.";
    }

    // calculate what's needed for this operator's children, if any:
    DataNeeded childUsage = DataNeeded.max(whatFor, dataNeededMap.getOrDefault(input.operator, DataNeeded.DOCS));

    // recurse if this has children.
    for (QExpr child : input.children) {
      collectTermRequestsRecursive(child, output, childUsage);
    }
  }

  public void collectTerms(QExpr input, Map<Term, DataNeeded> output, DataNeeded whatFor) {
    Parameters cfg = input.config;
    switch (getOperator(input)) {
      case "text": {
        String token = Objects.requireNonNull(cfg.get("term", cfg.get("token", cfg.get(QExpr.DEFAULT_KEY, (String) null))));
        String field = cfg.get("field", defaultField);
        Term t = new Term(field, token);

        // find term and save for later on the query-node.
        output.compute(t, (oldTerm, oldUsage) -> DataNeeded.max(whatFor, oldUsage));
        input.setUserData(Term.class, t);

        assert (input.children.size() == 0) : "Expect that 'term' nodes have no children.";
      } return;
      case "category": {
        String val = Objects.requireNonNull(cfg.get("value", cfg.get(QExpr.DEFAULT_KEY, (String) null)));
        String field = cfg.get("field", defaultField);

        Term t = new Term(field, val);
        output.compute(t, (oldTerm, oldUsage) -> DataNeeded.max(DataNeeded.DOCS, oldUsage));
        input.setUserData(Term.class, t);

        assert (input.children.size() == 0) : "Expect that 'category' nodes have no children.";
      } return;

      default:
        break;
    }

    // calculate what's needed for this operator's children, if any:
    DataNeeded childUsage = DataNeeded.max(whatFor, dataNeededMap.getOrDefault(input.operator, DataNeeded.DOCS));

    // recurse if this has children.
    for (QExpr child : input.children) {
      collectTerms(child, output, childUsage);
    }
  }

  public QueryEvalNode reify(QExpr input, QueryContext ctx) {
    assert(index != null);
    Parameters cfg = input.config;

    if (input.children.isEmpty()) {
      return leaf(input, ctx);
    }

    switch (getOperator(input)) {
      case "require":
        if(input.children.size() != 2) {
          throw new RuntimeException("Require expects 2 children, found: "+input.children);
        }
        return new RequireEval(
            reify(input.children.get(0), ctx),
            reify(input.children.get(1), ctx)
        );
      case "majority":
        return new MajorityEvalNode(reifyChildren(input, ctx));
      case "or":
        return new OrEvalNode(reifyChildren(input, ctx));
      case "and":
        return new AndEvalNode(reifyChildren(input, ctx));
      case "od":
        return new IreneOrderedWindow(ctx.exprModel, cfg.get("width", 1), leafChildren(input, ctx));
      case "uw":
        return new IreneUnorderedWindow(ctx.exprModel, cfg.get("width", 8), leafChildren(input, ctx));
      case "bdql":
        return new BinaryDirichletQLEvalNode(index, cfg.get("mu", DirichletSmoothingEvalNode.DefaultMu), leafChildren(input, ctx), input.getTransformedWeightVector());
      case "optbm25": {
        List<LeafEvalNode> children = leafChildren(input, ctx);
        float[] weights = input.getTransformedWeightVector();
        double b = cfg.get("b", GalagoBM25EvalNode.DefaultB);
        double k = cfg.get("k", GalagoBM25EvalNode.DefaultK);
        String opt = cfg.get("opt", "maxscore");
        String sort = cfg.get("sort", "df");
        switch (opt) {
          case "maxscore":
          case "df-maxscore":
            return new SortedMaxscoreBM25(index, b, k, children, weights, sort);
          case "wand":
            return new SortedWANDBM25(index, b, k, children, weights);
          case "wbool":
            return new WBoolBM25(index, b, k, children, weights);
          default: throw new IllegalArgumentException("opt="+opt);
        }
      }
      case "dql": {
        List<LeafEvalNode> children = leafChildren(input, ctx);
        float[] weights = input.getTransformedWeightVector();
        double mu = cfg.get("mu", DirichletSmoothingEvalNode.DefaultMu);
        String opt = cfg.get("opt", "maxscore");
        String sort = cfg.get("sort", "df");
        switch (opt) {
          case "maxscore":
            return new DirichletQLEvalNode(index, mu, children, weights);
          case "wand":
            return new WANDDQL(index, mu, children, weights);
          case "df-maxscore":
            return new SortedMaxscoreDQL(index, mu, children, weights, sort);
          case "wbool":
            return new WBoolDQL(index, mu, children, weights);
          default: throw new IllegalArgumentException("opt="+opt);
        }
      }
      case "combine":
        return new GalagoCombine(reifyChildren(input, ctx), input.getTransformedWeightVector());
      case "syn":
        return new SynonymEval(ctx.exprModel, leafChildren(input, ctx));
      case "countsum":
        return new CountSum(reifyChildren(input, ctx));
      case "importance":
        return new BinaryImportanceEval(reifyChildren(input, ctx), cfg);
      case "dirichlet":
        return new DirichletSmoothingEvalNode(reifyChild(input, ctx, CountEvalNode.class), cfg.get("mu", DirichletSmoothingEvalNode.DefaultMu));
      case "linear":
        return new LinearSmoothingEvalNode(reifyChild(input, ctx, CountEvalNode.class), cfg.get("lambda", LinearSmoothingEvalNode.DefaultLambda));
      case "additive":
        return new AdditiveSmoothingNode(reifyChild(input, ctx, CountEvalNode.class), cfg.get("epsilon", AdditiveSmoothingNode.DefaultEpsilon));
      case "mle":
        return new DocumentLikelihoodModel(reifyChild(input, ctx, CountEvalNode.class), cfg.get("alpha", DocumentLikelihoodModel.DefaultAlpha));
      case "to-score":
        return new CountToScore(reifyChild(input, ctx, CountEvalNode.class));
      case "bm25":
        return new GalagoBM25EvalNode(
            cfg.get("b", GalagoBM25EvalNode.DefaultB),
            cfg.get("k", GalagoBM25EvalNode.DefaultK),
            leafChild(input, ctx));
      default:
        // TODO, attempt reflection instantiation...
        throw new UnsupportedOperationException("Unknown operator=" + input.operator + " for query: " + input);
    }
  }


  @SuppressWarnings("unchecked")
  private <T extends QueryEvalNode> T reifyChild(QExpr input, QueryContext ctx, Class<T> klazz) {
    List<QueryEvalNode> children = reifyChildren(input, ctx);
    if (children.size() != 1) throw new RuntimeException("Query node " + input.operator + " expects a single child!");

    QueryEvalNode q = children.get(0);
    if(!klazz.isAssignableFrom(q.getClass())) {
      throw new RuntimeException("Expected child to be of class: "+klazz+" but was of class: "+q.getClass());
    }
    return (T) q;
  }

  private LeafEvalNode leafChild(QExpr input, QueryContext ctx) {
    List<LeafEvalNode> children = leafChildren(input, ctx);
    if (children.size() != 1) throw new RuntimeException("Query node " + input.operator + " expects a single child!");
    return children.get(0);
  }

  public LeafEvalNode leaf(QExpr input, QueryContext ctx) {
    switch (getOperator(input)) {
      case "category":
      case "text":
        return ctx.leafCreator.requestTerm(input.getUserData(TermRequest.class));
      case "lucene-match":
      case "lucene-count":
      case "int-range":
      case "lucene-score": {
        LuceneScorerRequest queryInfo = input.getUserData(LuceneScorerRequest.class);
        float defaultScore = (float) input.config.get("defaultScore", 0.0);
        return IreneWrappedScorer.create(ctx.getScorer(queryInfo), defaultScore);
      }
      case "syn":
        return new SynonymEval(ctx.exprModel, leafChildren(input, ctx));
      default:
        throw new UnsupportedOperationException("Unknown operator=" + input.operator + " wanted as **leaf** for query: " + input);
    }
  }

  public List<QueryEvalNode> reifyChildren(QExpr input, QueryContext ctx) {
    List<QueryEvalNode> output = new ArrayList<>(input.children.size());
    for (QExpr child : input.children) {
      output.add(reify(child, ctx));
    }
    if (output.isEmpty()) throw new RuntimeException("Query node " + input.operator + " expects children!");
    return output;
  }


  public List<LeafEvalNode> leafChildren(QExpr input, QueryContext ctx) {
    List<LeafEvalNode> output = new ArrayList<>(input.children.size());
    for (QExpr child : input.children) {
      output.add(leaf(child, ctx));
    }
    if (output.isEmpty()) throw new RuntimeException("Query node " + input.operator + " expects leaf children!");
    return output;
  }
}
