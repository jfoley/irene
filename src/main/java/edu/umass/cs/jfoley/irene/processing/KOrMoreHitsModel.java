package edu.umass.cs.jfoley.irene.processing;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author jfoley
 */
public class KOrMoreHitsModel implements CollectorManager<KOrMoreHitsModel.CountingCollector, Boolean> {
  AtomicLong foundCount = new AtomicLong(0);
  final long threshold;

  public KOrMoreHitsModel(long threshold) {
    this.threshold = threshold;
  }

  @Override
  public CountingCollector newCollector() throws IOException {
    return new CountingCollector(this, threshold);
  }

  @Override
  public Boolean reduce(Collection<CountingCollector> collectors) throws IOException {
    return foundCount.get() > threshold;
  }

  static class CountingCollector implements Collector {
    final KOrMoreHitsModel parent;
    final long threshold;

    CountingCollector(KOrMoreHitsModel parent, long threshold) {
      this.parent = parent;
      this.threshold = threshold;
    }

    @Override
    public LeafCollector getLeafCollector(LeafReaderContext context) throws IOException {
      return new LeafCollector() {
        @Override public void setScorer(Scorer scorer) throws IOException { }

        @Override
        public void collect(int doc) throws IOException {
          if(parent.foundCount.incrementAndGet() > threshold) {
            throw new CollectionTerminatedException();
          }
        }
      };
    }

    @Override
    public boolean needsScores() {
      return false;
    }
  }
}
