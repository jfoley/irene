package edu.umass.cs.jfoley.irene.expr.macro;

import edu.umass.cs.jfoley.irene.expr.IreneQueryLanguage;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nullable;

/**
 * @author jfoley.
 */
public class SingleChildCollapser extends Macro {
  public SingleChildCollapser(Parameters cfg) {
    super(cfg);
  }

  @Nullable
  @Override
  public QExpr transform(QExpr input) {
    // TODO, include #combine, #importance in here as long as we push the weights around appropriately.
    // This means we need to normalize (only once) as a macro pass before running this macro.

    switch (IreneQueryLanguage.getOperator(input)) {
      case "od":
      case "uw":
      case "syn":
      case "or":
      case "and": {
        if(input.children.size() == 0) {
          return null;
        } else if(input.children.size() == 1) {
          return transform(input.children.get(0));
        }
      }
      default:
        return input.copy(this.transform(input.children));
    }
  }
}
