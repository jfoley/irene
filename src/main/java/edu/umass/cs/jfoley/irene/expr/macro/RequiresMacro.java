package edu.umass.cs.jfoley.irene.expr.macro;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author jfoley
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiresMacro {
  Class<?>[] value();
}
