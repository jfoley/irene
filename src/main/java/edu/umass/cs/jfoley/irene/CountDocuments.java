package edu.umass.cs.jfoley.irene;

import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;

/**
 * Command-line tool to count documents in an irene index.
 */
public class CountDocuments {
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        IreneIndex index = new IreneIndex(argp.getString("index"));

        System.out.println("Total Number of Documents: "+index.getTotalDocuments());
        final String defaultField = argp.get("field", "body");
        System.out.println("Total Number of Terms: "+index.reader.getSumTotalTermFreq(defaultField));
        long numTokens = index.getVocabularySize(defaultField);
        System.out.println("Total Number of Unique Terms: "+numTokens);
    }
}
