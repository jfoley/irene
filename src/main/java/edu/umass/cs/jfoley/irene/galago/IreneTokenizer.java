package edu.umass.cs.jfoley.irene.galago;

import ciir.jfoley.chai.collections.list.IntList;
import edu.umass.cs.jfoley.irene.IreneEnglishAnalyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.tokenize.Tokenizer;
import org.lemurproject.galago.tupleflow.FakeParameters;
import org.lemurproject.galago.tupleflow.TupleFlowParameters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by jfoley on 4/17/16.
 */
public class IreneTokenizer extends Tokenizer {
    private final IreneEnglishAnalyzer analyzer;

    public IreneTokenizer() {
        this(new FakeParameters());
    }
    public IreneTokenizer(TupleFlowParameters parameters) {
        super(parameters);
        this.analyzer = new IreneEnglishAnalyzer();
    }

    @Override
    public void tokenize(Document input) {
        if(input == null) throw new NullPointerException();
        if(input.text == null) throw new NullPointerException();

        List<String> tokens = new ArrayList<>();
        IntList termCharBegins = new IntList();
        IntList termCharEnds = new IntList();

        try {
            TokenStream body = analyzer.tokenStream("body", input.text);

            final CharTermAttribute charTermAttr = body.addAttribute(CharTermAttribute.class);
            final OffsetAttribute charOffsetAttr = body.addAttribute(OffsetAttribute.class);

            body.reset();
            while(body.incrementToken()) {
                String term = charTermAttr.toString();
                termCharBegins.push(charOffsetAttr.startOffset());
                termCharEnds.push(charOffsetAttr.endOffset());
                tokens.add(term);
            }
            body.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        input.termCharBegin = termCharBegins;
        input.termCharEnd = termCharEnds;
        input.tags = Collections.emptyList();
        input.terms = tokens;
    }
}
