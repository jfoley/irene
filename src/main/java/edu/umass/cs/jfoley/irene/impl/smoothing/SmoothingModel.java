package edu.umass.cs.jfoley.irene.impl.smoothing;

import edu.umass.cs.jfoley.irene.eval.QueryEvalNode;
import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;

/**
 * @author jfoley
 */
public interface SmoothingModel {
  QueryEvalNode makeScorer(LeafEvalNode leafNode);
}
