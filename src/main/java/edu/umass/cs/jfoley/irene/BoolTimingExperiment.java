package edu.umass.cs.jfoley.irene;

import ciir.jfoley.chai.collections.list.IntList;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.math.StreamingStats;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;

/**
 * @author jfoley
 */
public class BoolTimingExperiment {
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    String inputTSV = argp.get("queries", "rob04.titles.tsv");
    try (IreneIndex index = new IreneIndex(argp.get("index", "robust.irene"))) {
      for (String line : LinesIterable.fromFile(inputTSV).slurp()) {
        String[] parts = line.split("\t");
        if (parts.length != 2) {
          System.err.println("Unknown query line: ``" + line + "``");
          continue;
        }
        String qid = parts[0];
        String input = parts[1];

        IntList results;
        long start = System.currentTimeMillis();
        QExpr qExpr = index.simpleQuery(argp.get("model", "combine"), input);

        results = index.matching(qExpr);
        long end = System.currentTimeMillis();
        System.err.println(qid+" "+qExpr.toString());
        System.err.println("  found " + results.size()+" in "+(end - start)+"ms.");

        final int N = Math.min(500, results.size());
        BooleanQuery.Builder builder = new BooleanQuery.Builder();
        QExpr best100 = new QExpr("or");
        IntList nums = new IntList();
        for (int i = 0; i < N; i++) {
          final int num = results.getQuick(i);
          nums.push(num);
          final String name = index.getField(num, "id");
          best100.addChild(QExpr.fieldMatch("id", name));
          builder.add(new TermQuery(new Term("id", name)), BooleanClause.Occur.SHOULD);
        }

        StreamingStats info = new StreamingStats();
        for (int i = 0; i < 100; i++) {
          start = System.currentTimeMillis();
          final IntList again = index.matching(best100);
          end = System.currentTimeMillis();
          assert(again.equals(nums));
          info.push((end - start) / 1e3);
        }
        System.err.println(info);

        /*
        final String bquery = builder.build().toString();
        System.err.println(bquery);
        StreamingStats linfo = new StreamingStats();
        for (int i = 0; i < 100; i++) {
          start = System.currentTimeMillis();
          final TopDocs again = index.searcher.search(builder.build(), N);
          end = System.currentTimeMillis();
          IntList hits = new IntList(N);
          for (ScoreDoc scoreDoc : again.scoreDocs) {
            hits.push(scoreDoc.doc);
          }
          assert(hits.equals(nums));
          linfo.push((end - start) / 1e3);
        }
        System.err.println(linfo);*/

      }
    }

  }
}
