package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Includes fast MaxScore based optimizations.
 * @author jfoley
 */
@QueryOperator("importance")
public class BinaryImportanceEval implements QueryEvalNode {
  private final int N;
  private final QueryEvalNode[] predicates;
  private final float[] weights;
  private final float[] bestCaseRemaining;
  private long numHits;
  private final List<QueryEvalNode> matchChildren;
  private float heapMinimumScore = -Float.MAX_VALUE;

  public void setMinimumRequiredScore(float score) {
    /*if(score != heapMinimumScore) {
      System.out.println(score);
    }*/
    heapMinimumScore = score;
  }

  // Heuristic: we want to score nodes first if they impact the score most, but also if they're cheaper, hoping to skip the most expensive nodes we can.
  private static class WeightedChild implements Comparable<WeightedChild> {
    private final double importance;
    QueryEvalNode node;
    float weight;
    long df;

    private WeightedChild(QueryEvalNode node, float weight) {
      this.node = node;
      this.weight = weight;
      df = node.estimateDF();
      this.importance = Math.abs(weight) / df;
    }

    @Override
    public int compareTo(@Nonnull WeightedChild o) {
      return Double.compare(this.importance, o.importance);
    }
  }

  public BinaryImportanceEval(List<QueryEvalNode> nodes, Parameters cfg) {
    List<WeightedChild> wnodes = new ArrayList<>(nodes.size());
    matchChildren = new ArrayList<>();
    numHits = 0;

    for (int i = 0; i < nodes.size(); i++) {
      final QueryEvalNode node = nodes.get(i);
      if(!node.hasNoMatches()) {
        matchChildren.add(node);
        numHits = Math.max(node.estimateDF(), numHits);
        wnodes.add(new WeightedChild(node, (float) cfg.get(Integer.toString(i), 1.0)));
      }
    }

    // most hits first:
    matchChildren.sort(mostToLeastFrequent);
    // highest score first.
    wnodes.sort(Comparator.reverseOrder());

    this.N = matchChildren.size();
    predicates = new QueryEvalNode[N];
    weights = new float[N];
    bestCaseRemaining = new float[N];

    for (int i = 0; i < N; i++) {
      final WeightedChild wnode = wnodes.get(i);
      predicates[i] = wnode.node;
      weights[i] = (float) wnode.weight;
    }

    for (int i = 0; i < N; i++) {
      float sum = 0;
      for (int j = i+1; j < N; j++) {
        sum += Math.max(weights[j], 0);
      }
      bestCaseRemaining[i] = sum;
    }
  }

  @Override
  public boolean hasNoMatches() {
    return N==0;
  }

  @Override
  public float score(int doc) throws IOException {
    if(matches(doc)) {
      return score;
    }
    return -Float.MAX_VALUE;
    /*float sum = 0;
    for (int i = 0; i < N; i++) {
      float val = predicates[i].count(doc) * weights[i];
      sum += val;

      // short-circuit if the best case scenario can't add to the heap.
      if(sum + bestCaseRemaining[i] < heapMinimumScore) {
        break;
      }
    }
    return sum;*/
  }

  @Override
  public int count(int doc) throws IOException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    throw new UnsupportedOperationException();
  }

  // cache the score as you compute whether it matches:
  private int doc = -1;
  private float score = 0;
  private boolean match = false;

  @Override
  public boolean matches(int id) throws IOException {
    if(id == doc) {
      return match;
    }

    // set up current document in cache:
    doc = id;
    score = 0;
    match = false;

    // look at weights in decreasing order:
    for (int i = 0; i < N; i++) {
      if(predicates[i].matches(id)) {
        score += weights[i];
      }
      if(score + bestCaseRemaining[i] < heapMinimumScore) {
        match = false;
        break;
      }
    }

    match = score > heapMinimumScore;
    return match;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createOrMover(matchChildren);
  }

  @Override
  public long estimateDF() {
    return numHits;
  }
}
