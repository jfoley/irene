package edu.umass.cs.jfoley.irene.impl;

import edu.umass.cs.jfoley.irene.eval.leaf.IreneIndexedTerm;
import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.eval.leaf.MissingTerm;
import org.apache.lucene.index.*;
import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.TermStatistics;

import java.io.IOException;
import java.util.Map;

/**
 * @author jfoley
 */
public class IreneTermInfo {
  final DataNeeded dataNeeded;
  final Term term;
  final TermContext termContext;
  final CollectionStatistics collectionStatistics;
  final TermStatistics termStatistics;
  public final long vocabularySize;

  public IreneTermInfo(DataNeeded dataNeeded, Term term, TermContext termContext, CollectionStatistics collectionStatistics, TermStatistics termStatistics, long vocabularySize) {
    this.dataNeeded = dataNeeded;
    this.term = term;
    this.termContext = termContext;
    this.collectionStatistics = collectionStatistics;
    this.termStatistics = termStatistics;
    this.vocabularySize = vocabularySize;
  }

  public CollectionStatistics getCollectionStatistics() { return collectionStatistics; }
  public TermStatistics getTermStatistics() { return termStatistics; }

  public LeafEvalNode create(LeafReaderContext context, NumericDocValues lengths) throws IOException {
    final TermState state = termContext.get(context.ord);
    if (state == null) {
      return new MissingTerm(this, lengths);
    }

    TermsEnum termIter = context.reader().terms(term.field()).iterator();
    termIter.seekExact(term.bytes(), state);
    PostingsEnum docs = termIter.postings(null, dataNeeded.flags());
    if (docs == null) return new MissingTerm(this, lengths); // error?

    return new IreneIndexedTerm(this, lengths, docs);
  }

  public LeafEvalNode create(LeafReaderContext context, Map<String, NumericDocValues> lengthsCache) throws IOException {

    NumericDocValues lengths = lengthsCache.computeIfAbsent(term.field(), (missing) -> {
      try {
        return context.reader().getNormValues(term.field());
      } catch (IOException e) {
        throw new RuntimeException("Couldn't find norm-lengths for field: "+missing, e);
      }
    });
    return create(context, lengths);
  }

  public Term getTerm() {
    return term;
  }
}
