package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import org.apache.lucene.search.Explanation;

import java.io.IOException;

/**
 * @author jfoley.
 */
@QueryOperator(value={"additive", "laplace"})
@TermDataNeeded(DataNeeded.COUNTS)
public class AdditiveSmoothingNode extends SingleChildQueryEvalNode<CountEvalNode> {
  public static final double DefaultEpsilon = 0.1;
  final double epsilon;
  private final double denom;

  public AdditiveSmoothingNode(CountEvalNode child, double epsilon) {
    super(child);
    long vsize = child.getCountStatistics().totalUniqueTerms;
    this.epsilon = epsilon;
    this.denom = vsize * epsilon; // how much we have virtually added to denominator of every document
  }

  @Override
  public float score(int doc) throws IOException {
    double count = child.count(doc);
    double length = child.length(doc);
    return (float) Math.log((count+epsilon) / (length+denom));
  }

  @Override
  public int count(int doc) throws IOException {
    return child.count(doc);
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    float score = score(doc);
    return Explanation.match(score, String.format("(%d+%f)/%d=%f", child.count(doc), epsilon, child.length(doc), score), child.explain(doc));
  }
}
