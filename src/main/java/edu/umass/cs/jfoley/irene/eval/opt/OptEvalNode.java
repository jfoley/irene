package edu.umass.cs.jfoley.irene.eval.opt;

import edu.umass.cs.jfoley.irene.eval.QueryEvalNode;
import org.apache.lucene.search.DocIdSetIterator;

/**
 * @author jfoley
 */
public abstract class OptEvalNode extends DocIdSetIterator implements QueryEvalNode {
}
