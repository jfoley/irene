package edu.umass.cs.jfoley.irene.expr.macro;

import edu.umass.cs.jfoley.irene.expr.IreneQueryLanguage;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nullable;
import java.util.List;

/**
 * @author jfoley
 */
public class MacroChain extends Macro {
  private final List<Macro> macros;

  public MacroChain(Parameters cfg, List<Macro> macros) {
    super(cfg);
    this.macros = macros;
  }

  @Override
  public void setFactory(IreneQueryLanguage factory) {
    for (Macro macro : this.macros) {
      macro.setFactory(factory);
    }
  }

  @Nullable
  @Override
  public QExpr transform(QExpr input) {
    QExpr result = input;
    for (Macro macro : macros) {
      result = macro.transform(result);
      if(result == null) return null;
    }
    return result;
  }
}
