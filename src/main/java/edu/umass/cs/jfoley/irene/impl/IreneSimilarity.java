package edu.umass.cs.jfoley.irene.impl;

import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.NumericDocValues;
import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.TermStatistics;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;

/**
 * This is a hacked-similarity that's not actually used during scoring but is only used to ensure that the length gets stored properly, rather than in Lucene's default, pre-divided normed state.
 * @author jfoley
 */
public class IreneSimilarity extends Similarity {
  @Override
  public long computeNorm(FieldInvertState state) {
    return state.getLength();
  }

  @Override
  public IreneStats computeWeight(float queryBoost, CollectionStatistics collectionStats, TermStatistics... termStats) {
    for (TermStatistics termStat : termStats) {
      System.err.println(termStat.term()+" DF: "+termStat.docFreq()+" TTF:"+termStat.totalTermFreq());
    }
    return new IreneStats(collectionStats, termStats);
    //return new IreneStats(collectionStats, termStats);
    //throw new UnsupportedOperationException();
  }

  @Override
  public IreneSimScorer simScorer(SimWeight weight, LeafReaderContext context) throws IOException {
    throw new UnsupportedOperationException();
    //IreneStats stats = (IreneStats) weight;
    //NumericDocValues norms = context.reader().getNormValues(stats.getField());
    //return new IreneSimScorer(norms);
  }

  public static class IreneSimScorer extends SimScorer {
    private final NumericDocValues norms;

    public IreneSimScorer(NumericDocValues norms) {
      this.norms = norms;
    }

    @Override
    public float score(int doc, float freq) {
      if (norms == null) {
        return freq;
      }
      double length = norms.get(doc);
      return (float) (Math.log(freq) - Math.log(length));
    }

    @Override
    public float computeSlopFactor(int distance) {
      return 1.0f / (distance + 1);
    }

    @Override
    public float computePayloadFactor(int doc, int start, int end, BytesRef payload) {
      return 1;
    }
  }
}
