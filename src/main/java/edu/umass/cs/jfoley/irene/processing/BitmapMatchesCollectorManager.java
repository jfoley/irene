package edu.umass.cs.jfoley.irene.processing;

import edu.umass.cs.jfoley.irene.expr.ExplainScorer;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.CollectorManager;
import org.apache.lucene.search.LeafCollector;
import org.apache.lucene.search.Scorer;
import org.roaringbitmap.RoaringBitmap;

import java.io.IOException;
import java.util.Collection;

/**
 * @author jfoley
 */
public class BitmapMatchesCollectorManager implements CollectorManager<BitmapMatchesCollectorManager.DocIdSetMatchesCollector, RoaringBitmap> {
  final Double minimumScore;

  public BitmapMatchesCollectorManager() {
    this(null);
  }
  public BitmapMatchesCollectorManager(Double minimumScore) {
    this.minimumScore = minimumScore;
  }

  @Override
  public DocIdSetMatchesCollector newCollector() throws IOException {
    return new DocIdSetMatchesCollector(minimumScore);
  }

  @Override
  public RoaringBitmap reduce(Collection<DocIdSetMatchesCollector> collectors) throws IOException {
    RoaringBitmap hits = new RoaringBitmap();
    for (DocIdSetMatchesCollector collector : collectors) {
      hits.or(collector.hits);
    }
    return hits;
  }

  static class DocIdSetMatchesCollector implements Collector {
    RoaringBitmap hits = new RoaringBitmap();
    private final Double minimumScore;

    DocIdSetMatchesCollector(Double minimumScore) {
      this.minimumScore = minimumScore;
    }

    @Override
    public LeafCollector getLeafCollector(LeafReaderContext context) throws IOException {
      final int docBase = context.docBase;
      return new LeafCollector() {
        Scorer scorer = null;

        @Override public void setScorer(Scorer scorer) throws IOException {
          if(minimumScore != null && scorer instanceof ExplainScorer) {
            // smart score if possible:
            ((ExplainScorer) scorer).setMinimumRequiredScore(minimumScore.floatValue());
            this.scorer = scorer;
          }
        }

        @Override
        public void collect(int doc) throws IOException {
          if(minimumScore != null) {
            if(scorer.score() > minimumScore) {
              hits.add(docBase+doc);
            }
          } else {
            hits.add(docBase + doc);
          }
        }
      };
    }

    @Override
    public boolean needsScores() {
      return minimumScore != null;
    }
  }
}
