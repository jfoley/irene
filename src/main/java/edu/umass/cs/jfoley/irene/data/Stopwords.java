package edu.umass.cs.jfoley.irene.data;

import ciir.jfoley.chai.errors.FatalError;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.lang.ThreadsafeLazyPtr;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author jfoley
 */
public class Stopwords {
  private static ThreadsafeLazyPtr<Set<String>> _inquery = new ThreadsafeLazyPtr<>(() -> {
    try {
      return new HashSet<>(LinesIterable.of(IO.resource("/inquery.txt")).slurp());
    } catch (IOException e) {
      throw new FatalError(e);
    }
  });

  public static Set<String> getInqueryList() {
    return _inquery.get();
  }
}
