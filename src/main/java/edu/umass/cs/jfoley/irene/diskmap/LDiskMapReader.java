package edu.umass.cs.jfoley.irene.diskmap;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

/**
 * @author jfoley
 */
public class LDiskMapReader implements Closeable {
  public final FSDirectory dir;
  public final IndexReader reader;
  public final IndexSearcher searcher;

  public LDiskMapReader(File dir) throws IOException {
    this.dir = FSDirectory.open(dir.toPath());
    reader = DirectoryReader.open(this.dir);
    searcher = new IndexSearcher(reader);
  }

  public byte[] get(byte[] key) throws IOException {
    BooleanQuery q = new BooleanQuery.Builder().add(new TermQuery(new Term("id", new BytesRef(key))), BooleanClause.Occur.MUST).build();
    ScoreDoc[] scoreDocs = searcher.search(q, 1000).scoreDocs;
    if(scoreDocs.length == 0) {
      return null;
    }
    BytesRef data = searcher.doc(scoreDocs[0].doc, Collections.singleton("value")).getField("value").binaryValue();
    if(data == null) return null;
    return Arrays.copyOfRange(data.bytes, data.offset, data.length);
  }

  @Override
  public void close() throws IOException {
    reader.close();
    dir.close();
  }
}
