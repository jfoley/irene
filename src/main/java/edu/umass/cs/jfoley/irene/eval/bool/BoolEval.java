package edu.umass.cs.jfoley.irene.eval.bool;

import edu.umass.cs.jfoley.irene.eval.QueryEvalNode;
import edu.umass.cs.jfoley.irene.eval.RecursiveEval;

import java.io.IOException;
import java.util.List;

/**
 * @author jfoley.
 */
public abstract class BoolEval extends RecursiveEval {
  private long numHits;

  protected BoolEval(List<QueryEvalNode> children) {
    super(children);
    this.numHits = 0;
    for (QueryEvalNode child : children) {
      numHits = Math.max(numHits, child.estimateDF());
    }
  }

  @Override
  public float score(int doc) throws IOException {
    return matches(doc) ? 1 : 0;
  }

  @Override
  public int count(int doc) throws IOException {
    return matches(doc) ? 1 : 0;
  }

  public long estimateDF() {
    return numHits;
  }
}
