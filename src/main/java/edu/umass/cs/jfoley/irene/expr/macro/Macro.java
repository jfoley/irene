package edu.umass.cs.jfoley.irene.expr.macro;

import edu.umass.cs.jfoley.irene.expr.IreneQueryLanguage;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley
 */
public abstract class Macro {
  final protected Parameters cfg;
  protected IreneQueryLanguage factory;

  public void setFactory(IreneQueryLanguage factory) {
    this.factory = factory;
  }

  Macro(Parameters cfg) {
    this.cfg = cfg;
    factory = null;
  }

  @Nullable
  public abstract QExpr transform(QExpr input);

  @Nonnull
  public final QExpr transformNonEmpty(QExpr input) {
    QExpr output = transform(input);
    if(output == null) throw new IllegalStateException("Macro transformed to nothing. Query="+input+" in macro:"+this.getClass());
    return output;
  }

  /**
   * Helper function, transform all children and keep non-null children.
   * @param children list of children, from a QExpr, usually.
   * @return the children with this transform applied, or empty list if none of them passed the filter.
   */
  final List<QExpr> transform(List<QExpr> children) {
    ArrayList<QExpr> output = new ArrayList<>(children.size());

    for (QExpr child : children) {
      QExpr newChild = transform(child);
      if(newChild != null)
        output.add(newChild);
    }

    return output;
  }
}
