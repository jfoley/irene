package edu.umass.cs.jfoley.irene.eval.leaf;

import ciir.jfoley.chai.collections.util.ListFns;
import edu.umass.cs.jfoley.irene.eval.CountEvalNode;
import edu.umass.cs.jfoley.irene.eval.phrase.PositionsIterator;
import edu.umass.cs.jfoley.irene.expr.IreneQueryModel;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley
 */
@QueryOperator(value={"uw", "unordered", "prox"})
@TermDataNeeded(DataNeeded.POSITIONS)
public class IreneUnorderedWindow implements CountEvalNode {
  private final List<LeafEvalNode> children;
  private final int width;
  private final IreneQueryModel index;
  private IreneCountStatistics stats = null;

  private int calculatedDocument = -1;
  private int currentFrequency = 0;

  public IreneUnorderedWindow(IreneQueryModel index, int width, List<LeafEvalNode> children) {
    this.index = index;
    this.width = width;
    this.children = children;
  }

  @Override
  public int length(int doc) {
    return children.get(0).length(doc);
  }

  @Override
  public IreneCountStatistics getCountStatistics() {
    if(stats == null) {

      if(IreneOrderedWindow.estimateStats) {
        this.stats = IreneOrderedWindow.estimateMin(children);
        return stats;
      } else {
        stats = index.collectCountStatistics(
            new StatsKey.UnorderedWindowStatsKey(
                ListFns.map(children, LeafEvalNode::getTerm),
                width));
      }
    }
    return stats;
  }

  public static int countIter(List<PositionsIterator> iters, int width) {
    int hits = 0;

    int max = iters.get(0).currentPosition()+1;
    int min = iters.get(0).currentPosition();
    for (int i = 1; i < iters.size(); i++) {
      int pos = iters.get(i).currentPosition();
      max = Math.max(max, pos+1);
      min = Math.min(min, pos);
    }

    while(true) {
      boolean match = (max - min <= width) || (width == -1);
      if (match) {
        hits++;
        if(IreneOrderedWindow.maxFrequency > 0 && hits >= IreneOrderedWindow.maxFrequency) {
          return hits;
        }
      }

      int oldMin = min;
      // now, reset bounds
      max = Integer.MIN_VALUE;
      min = Integer.MAX_VALUE;
      for (PositionsIterator iter : iters) {
        int pos = iter.currentPosition();
        if (pos == oldMin) {
          boolean notDone = iter.next();
          if (!notDone) {
            return hits;
          }
          assert(iter.currentPosition() > oldMin);
        }
        pos = iter.currentPosition();
        max = Math.max(max, pos+1);
        min = Math.min(min, pos);
      }
    }
  }

  private int countIntersection(int doc) throws IOException {
    List<PositionsIterator> posIters = new ArrayList<>();
    for (LeafEvalNode child : children) {
      if(!child.matches(doc)) {
        return 0;
      }
      int count = child.count(doc);
      if(count > 0) {
        posIters.add(child.positionsIter(doc));
      } else {
        return 0;
      }
    }

    return countIter(posIters, width);
  }

  @Override
  public float score(int doc) throws IOException {
    return count(doc);
  }

  @Override
  public int count(int doc) throws IOException {
    if(calculatedDocument == doc) {
      return currentFrequency;
    }
    currentFrequency = countIntersection(doc);
    calculatedDocument = doc;
    return currentFrequency;
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    int count = count(doc);
    List<Explanation> childExplains = new ArrayList<>();
    for (LeafEvalNode child : children) {
      childExplains.add(child.explain(doc));
    }
    if(count == 0) {
      return Explanation.noMatch("UnorderedWindow no match: "+childExplains);
    } else {
      return Explanation.match(count, "UnorderedWindow match: ", childExplains);
    }
  }

  @Override
  public boolean matches(int id) throws IOException {
    for (LeafEvalNode child : this.children) {
      if(!child.matches(id)) return false;
    }
    return count(id) > 0;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createAndMover(this.children);
  }

  /**
   * TODO, this causes an deadlock in calculate statistics.
   * We want to grab statistics if they're available, not just return zero... but that causes a deadlock if we've created this query to literally calculate the statistics.
   * @see IreneOrderedWindow#estimateDF()
   * @return an estimate of how many times we expect this to hit.
   */
  @Override
  public long estimateDF() {
    if(stats == null) {
      // Cheap estimate: take the min if we have any children:
      return IreneOrderedWindow.estimateMin(children).documentFrequency;
    }
    return stats.documentFrequency;
  }
}
