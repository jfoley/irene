package edu.umass.cs.jfoley.irene.index;

/**
 * @author jfoley
 */
public class IndexingException extends RuntimeException {
  public IndexingException(Exception inner) {
    super(inner);
  }
}
