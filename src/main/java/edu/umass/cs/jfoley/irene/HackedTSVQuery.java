package edu.umass.cs.jfoley.irene;

import ciir.jfoley.chai.io.LinesIterable;
import edu.umass.cs.jfoley.irene.eval.DirichletSmoothingEvalNode;
import edu.umass.cs.jfoley.irene.eval.leaf.IreneOrderedWindow;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;

/**
 * @author jfoley
 */
public class HackedTSVQuery {
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    String inputTSV = argp.get("queries", "rob04.titles.tsv");
    int depth = argp.get("requested", 1000);

    if(argp.get("normify", false)) {
      DirichletSmoothingEvalNode.normify = true;
    }
    if(argp.get("estimateWindowStats", false)) {
      IreneOrderedWindow.estimateStats = true;
    }
    int maxFrequency = argp.get("maxWindowHits", -1);
    if(maxFrequency > 0) {
      IreneOrderedWindow.maxFrequency = maxFrequency;
    }

    try (IreneIndex index = new IreneIndex(argp.get("index", "robust.irene"))) {
      for (String line : LinesIterable.fromFile(inputTSV).slurp()) {
        String[] parts = line.split("\t");
        if(parts.length != 2) {
          System.err.println("Unknown query line: ``"+line+"``");
          continue;
        }
        String qid = parts[0];
        String input = parts[1];

        TopDocs results;
        long start = System.currentTimeMillis();

        QExpr qExpr = index.simpleQuery(argp.get("model", "sdm"), input);

        results = index.search(qExpr, depth);
        long end = System.currentTimeMillis();

        System.err.println(qid+" "+qExpr.toString());
        System.err.println("  found " + results.totalHits+","+results.scoreDocs.length +" in "+(end - start)+"ms.");

        ScoreDoc[] scoreDocs = results.scoreDocs;

        for (int i = 0; i < scoreDocs.length; i++) {
          ScoreDoc scoreDoc = scoreDocs[i];
          int docId = scoreDoc.doc;
          if(docId == DocIdSetIterator.NO_MORE_DOCS) {
            System.err.println("# error: scored NO_MORE_DOCS somewhere :( for "+scoreDoc);
          }
          //Document doc = index.searcher.doc(docId);
          String name = index.getField(docId, "id");
          System.out.printf("%s Q0 %s %d %10.8f irene\n", qid, name, i+1, scoreDoc.score);
        }
      }
    }

  }
}
