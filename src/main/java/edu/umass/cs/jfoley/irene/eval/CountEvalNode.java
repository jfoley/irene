package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import org.apache.lucene.search.Explanation;

import java.io.IOException;

/**
 * @author jfoley
 */
public interface CountEvalNode extends QueryEvalNode {
  int length(int doc);
  IreneCountStatistics getCountStatistics();
  int count(int doc) throws IOException;
  Explanation explain(int doc) throws IOException;
}
