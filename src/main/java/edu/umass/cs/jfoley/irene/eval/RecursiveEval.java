package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley.
 */
public abstract class RecursiveEval implements QueryEvalNode {
  protected final String className;
  public final List<QueryEvalNode> children;
  public final int N;

  protected RecursiveEval(List<QueryEvalNode> children) {
    this.children = children;
    this.className = this.getClass().getSimpleName();
    N = children.size();
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    List<Explanation> cExplain = new ArrayList<>();
    for (QueryEvalNode child : children) {
      cExplain.add(child.explain(doc));
    }
    if(matches(doc)) {
      return Explanation.match(score(doc), className+".Match", cExplain);
    } else {
      return Explanation.noMatch(className+".Miss", cExplain);
    }
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createOrMover(children);
  }
}
