package edu.umass.cs.jfoley.irene.eval.leaf;

import ciir.jfoley.chai.collections.list.IntList;
import edu.umass.cs.jfoley.irene.impl.IreneTermInfo;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.Scorer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;

/**
 * #lucene-count
 * #lucene-match
 * #lucene-score
 * @see IreneMissingScorer
 * @author jfoley
 */
public final class IreneWrappedScorer implements LeafEvalNode {
  private final Scorer scorer;
  private final float missingScore;
  private int lastLoc = -1;

  public IreneWrappedScorer(@Nonnull Scorer scorer, float missingScore) {
    this.missingScore = missingScore;
    this.scorer = scorer;
  }

  @Override
  public int frequency() throws IOException {
    if (scorer.docID() == DocIdSetIterator.NO_MORE_DOCS) {
      return 0;
    }
    return scorer.freq();
  }

  @Override
  public IntList positions(int doc) throws IOException {
    throw new UnsupportedOperationException();
  }

  @Override
  public int currentDocument() throws IOException {
    return scorer.docID();
  }

  public void syncTo(int doc) throws IOException {
    if(lastLoc > doc) return;
    if(scorer.docID() < doc) {
      lastLoc = scorer.advance(doc);
    }
  }

  @Override
  public float score(int doc) throws IOException {
    syncTo(doc);
    if(scorer.docID() != doc) {
      return this.missingScore;
    }
    return scorer.score();
  }

  @Override
  public int length(int doc) {
    throw new UnsupportedOperationException();
  }

  @Override
  public int count(int doc) throws IOException {
    syncTo(doc);
    if(scorer.docID() != doc) {
      return 0;
    }
    return scorer.freq();
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    syncTo(doc);
    if(scorer.docID() == doc) {
      return Explanation.match(score(doc), "Match!");
    }
    return Explanation.noMatch("No Match, Background="+missingScore);
  }

  @Override
  public IreneTermInfo getTermInfo() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Term getTerm() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int advance(int target) throws IOException {
    return scorer.advance(target);
  }

  @Nonnull
  @Override
  public MoveInfo getMovementInfo() {
    return MoveInfo.wrap(scorer);
  }

  @Override
  public long estimateDF() {
    return scorer.cost();
  }

  @Nonnull
  public static LeafEvalNode create(@Nullable Scorer scorer, float v) {
    if(scorer == null) {
      return new IreneMissingScorer(v);
    } else {
      return new IreneWrappedScorer(scorer, v);
    }
  }
}
