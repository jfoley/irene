package edu.umass.cs.jfoley.irene.util;
/**
 * <p>Title: Kstemmer</p>
 * <p>Description: This is a java version of Bob Krovetz' kstem stemmer</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: CIIR University of Massachusetts Amherst (http://ciir.cs.umass.edu) </p>
 * @author Sergio Guzman-Lara
 * @version 1.1
 */

import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.lang.MString;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 This class implements the Kstem algorithm
 */
public final class KStem {
  static private final int MaxWordLen = 100;

  private static class DictEntry {
    boolean exception;
    MString root;

    public DictEntry(String root, boolean isException) {
      this(new MString(root), isException);
    }
    public DictEntry(MString root, boolean isException) {
      this.root = root;
      this.exception = isException;
    }

    public DictEntry() {
      this.root = null;
      this.exception = false;
    }
  }
  private static HashMap<MString,DictEntry> dict_ht = null;
  //private int MaxCacheSize;
  //private HashMap stem_ht = null;
  private MString word;
  private int j; /* index of final letter in stem (within word) */

  private int k; /* INDEX of final letter in word.
  You must add 1 to k to get the current length of word.
  When you want the length of word, use the method
  wordLength, which returns (k+1). */

  private final char finalChar() {
    return word.charAt(k);
  }

  private final char penultChar() {
    return word.charAt(k - 1);
  }

  private final boolean isVowel(int index) {
    return !isCons(index);
  }

  private final boolean isCons(int index) {
    char ch;

    ch = word.charAt(index);

    if ((ch == 'a') || (ch == 'e') || (ch == 'i') || (ch == 'o') || (ch == 'u')) {
      return false;
    }
    if ((ch != 'y') || (index == 0)) {
      return true;
    } else {
      return (!isCons(index - 1));
    }
  }

  private static synchronized void initializeDictHash() {
    DictEntry defaultEntry;

    if (dict_ht != null) {
      return;
    }
    dict_ht = new HashMap<>();
    defaultEntry = new DictEntry();

    try (LinesIterable lines = LinesIterable.of(IO.resourceReader("/krovetz_data.tsv"))) {
      for (String line : lines) {
        String col[] = line.split("\t");
        if(col.length == 1) {
          dict_ht.put(new MString(col[0]), defaultEntry);
        } else if(col.length == 2) {
          dict_ht.put(new MString(col[0]), new DictEntry(col[1], false));
        } else if(col.length == 3) {
          dict_ht.put(new MString(col[0]), new DictEntry(col[1], true));
        }
      }
    } catch (IOException e) {
      throw new RuntimeException("Error with resource: /krovetz_data.tsv.gz; can't use KStem.", e);
    }
  }

  private final boolean isAlpha(char ch) {
    if ((ch >= 'a') && (ch <= 'z')) {
      return true;
    }
    if ((ch >= 'A') && (ch <= 'Z')) {
      return true;
    }
    return false;
  }

  /* length of stem within word */
  private final int stemLength() {
    return j + 1;
  }

  private final boolean endsIn(String s) {
    boolean match;
    int sufflength = s.length();

    int r = word.length() - sufflength; /* length of word before this suffix */
    if (sufflength > k) {
      return false;
    }

    match = true;
    for (int r1 = r, i = 0; (i < sufflength) && (match); i++, r1++) {
      if (s.charAt(i) != word.charAt(r1)) {
        match = false;
      }
    }

    if (match) {
      j = r - 1;  /* index of the character BEFORE the posfix */
    } else {
      j = k;
    }
    return match;
  }

  private final DictEntry wordInDict() {
    return dict_ht.get(word);
  }


  /* Convert plurals to singular form, and '-ies' to 'y' */
  private final boolean plural() {
    if (finalChar() == 's') {
      if (endsIn("ies")) {
        word.setLength(j + 3);
        k--;
        if (lookup(word)) /* ensure calories -> calorie */ {
          return true;
        }
        k++;
        word.append('s');
        setSuffix("y");
      } else if (endsIn("es")) {
        /* try just removing the "s" */
        word.setLength(j + 2);
        k--;

        /* note: don't check for exceptions here.  So, `aides' -> `aide',
        but `aided' -> `aid'.  The exception for double s is used to prevent
        crosses -> crosse.  This is actually correct if crosses is a plural
        noun (a type of racket used in lacrosse), but the verb is much more
        common */

        if ((j > 0) && (lookup(word))
            && !((word.charAt(j) == 's')
            && (word.charAt(j - 1) == 's'))) {
          return true;
        }

        /* try removing the "es" */

        word.setLength(j + 1);
        k--;
        if (lookup(word)) {
          return true;
        }

        /* the default is to retain the "e" */
        word.append('e');
        k++;
        return true;
      } else {
        if (word.length() > 3 && penultChar() != 's'
            && !endsIn("ous")) {
          /* unless the word ends in "ous" or a double "s", remove the final "s" */

          word.setLength(k);
          k--;
          return true;
        }
      }
    }
    return false;
  }

  private final void setSuffix(String s) {
    setSuff(s, s.length());
  }

  /* replace old suffix with s */
  private final void setSuff(String s, int len) {
    word.setLength(j + 1);
    for (int l = 0; l < len; l++) {
      word.append(s.charAt(l));
    }
    k = j + len;
  }

  /* Returns true if s is found in the dictionary */
  private static boolean lookup(MString s) {
    return dict_ht.containsKey(s);
  }

  /* convert past tense (-ed) to present, and `-ied' to `y' */
  private final boolean pastTense() {
    /* Handle words less than 5 letters with a direct mapping
    This prevents (fled -> fl).  */

    if (word.length() <= 4) {
      return false;
    }

    if (endsIn("ied")) {
      word.setLength(j + 3);
      k--;
      if (lookup(word)) /* we almost always want to convert -ied to -y, but */ {
        return true;
      }
      k++;                         /* I don't know any long words that this applies to, */
      word.append('d');            /* but just in case...                              */
      setSuffix("y");
      return true;
    }

    /* the vowelInStem() is necessary so we don't stem acronyms */
    if (endsIn("ed") && vowelInStem()) {
      /* see if the root ends in `e' */
      word.setLength(j + 2);
      k = j + 1;

      DictEntry entry = wordInDict();
      if (entry != null) {
        if (!entry.exception) /* if it's in the dictionary and not an exception */ {
          return true;
        }
      }

      /* try removing the "ed" */
      word.setLength(j + 1);
      k = j;
      if (lookup(word)) {
        return true;
      }


      /* try removing a doubled consonant.  if the root isn't found in
      the dictionary, the default is to leave it doubled.  This will
      correctly capture `backfilled' -> `backfill' instead of
      `backfill' -> `backfille', and seems correct most of the time  */

      if (doubleC(k)) {
        word.setLength(k);
        k--;
        if (lookup(word)) {
          return true;
        }
        word.append(word.charAt(k));
        k++;
        return true;
      }

      /* if we have a `un-' prefix, then leave the word alone  */
      /* (this will sometimes screw up with `under-', but we   */
      /*  will take care of that later)                        */

      if ((word.charAt(0) == 'u') && (word.charAt(1) == 'n')) {
        word.append('e');
        word.append('d');
        k = k + 2;
        return false;
      }


      /* it wasn't found by just removing the `d' or the `ed', so prefer to
      end with an `e' (e.g., `microcoded' -> `microcode'). */

      word.setLength(j + 1);
      word.append('e');
      k = j + 1;
      return true;
    }
    return false;
  }

  /* return TRUE if word ends with a double consonant */
  private final boolean doubleC(int i) {
    if (i < 1) {
      return false;
    }

    if (word.charAt(i) != word.charAt(i - 1)) {
      return false;
    }
    return (isCons(i));
  }

  private final boolean vowelInStem() {
    for (int i = 0; i < stemLength(); i++) {
      if (isVowel(i)) {
        return true;
      }
    }
    return false;
  }

  /* handle `-ing' endings */
  private final boolean aspect() {
    /* handle short words (aging -> age) via a direct mapping.  This
    prevents (thing -> the) in the version of this routine that
    ignores inflectional variants that are mentioned in the dictionary
    (when the root is also present) */

    if (word.length() <= 5) {
      return false;
    }

    /* the vowelinstem() is necessary so we don't stem acronyms */
    if (endsIn("ing") && vowelInStem()) {

      /* try adding an `e' to the stem and check against the dictionary */
      word.setCharAt(j + 1, 'e');
      word.setLength(j + 2);
      k = j + 1;

      DictEntry entry = wordInDict();
      if (entry != null) {
        if (!entry.exception) /* if it's in the dictionary and not an exception */ {
          return true;
        }
      }

      /* adding on the `e' didn't work, so remove it */
      word.setLength(k);
      k--;           /* note that `ing' has also been removed */

      if (lookup(word)) {
        return true;
      }

      /* if I can remove a doubled consonant and get a word, then do so */
      if (doubleC(k)) {
        k--;
        word.setLength(k + 1);
        if (lookup(word)) {
          return true;
        }
        word.append(word.charAt(k)); /* restore the doubled consonant */

        /* the default is to leave the consonant doubled            */
        /*  (e.g.,`fingerspelling' -> `fingerspell').  Unfortunately */
        /*  `bookselling' -> `booksell' and `mislabelling' -> `mislabell'). */
        /*  Without making the algorithm significantly more complicated, this */
        /*  is the best I can do */
        k++;
        return true;
      }

      /* the word wasn't in the dictionary after removing the stem, and then
      checking with and without a final `e'.  The default is to add an `e'
      unless the word ends in two consonants, so `microcoding' -> `microcode'.
      The two consonants restriction wouldn't normally be necessary, but is
      needed because we don't try to deal with prefixes and compounds, and
      most of the time it is correct (e.g., footstamping -> footstamp, not
      footstampe; however, decoupled -> decoupl).  We can prevent almost all
      of the incorrect stems if we try to do some prefix analysis first */

      if ((j > 0) && isCons(j) && isCons(j - 1)) {
        k = j;
        word.setLength(k + 1);
        return true;
      }

      word.setLength(j + 1);
      word.append('e');
      k = j + 1;
      return true;
    }
    return false;
  }

  /* this routine deals with -ity endings.  It accepts -ability, -ibility,
  and -ality, even without checking the dictionary because they are so
  productive.  The first two are mapped to -ble, and the -ity is remove
  for the latter */
  private final boolean ityEndings() {
    int old_k = k;

    if (endsIn("ity")) {
      word.setLength(j + 1);          /* try just removing -ity */
      k = j;
      if (lookup(word)) {
        return true;
      }
      word.append('e');             /* try removing -ity and adding -e */
      k = j + 1;
      if (lookup(word)) {
        return true;
      }
      word.setCharAt(j + 1, 'i');
      word.append("ty");
      k = old_k;
      /* the -ability and -ibility endings are highly productive, so just accept them */
      if ((j > 0) && (word.charAt(j - 1) == 'i') && (word.charAt(j) == 'l')) {
        word.setLength(j - 1);
        word.append("le");   /* convert to -ble */
        k = j;
        return true;
      }


      /* ditto for -ivity */
      if ((j > 0) && (word.charAt(j - 1) == 'i') && (word.charAt(j) == 'v')) {
        word.setLength(j + 1);
        word.append('e');         /* convert to -ive */
        k = j + 1;
        return true;
      }
      /* ditto for -ality */
      if ((j > 0) && (word.charAt(j - 1) == 'a') && (word.charAt(j) == 'l')) {
        word.setLength(j + 1);
        k = j;
        return true;
      }

      /* if the root isn't in the dictionary, and the variant *is*
      there, then use the variant.  This allows `immunity'->`immune',
      but prevents `capacity'->`capac'.  If neither the variant nor
      the root form are in the dictionary, then remove the ending
      as a default */

      if (lookup(word)) {
        return true;
      }

      /* the default is to remove -ity altogether */
      word.setLength(j + 1);
      k = j;
      return true;
    }
    return false;
  }

  /* handle -ence and -ance */
  private final boolean nceEndings() {
    int old_k = k;
    char word_char;

    if (endsIn("nce")) {
      if (!((word.charAt(j) == 'e') || (word.charAt(j) == 'a'))) {
        return false;
      }
      word_char = word.charAt(j);
      word.setLength(j);
      word.append('e');     /* try converting -e/ance to -e (adherance/adhere) */
      k = j;
      if (lookup(word)) {
        return true;
      }
      word.setLength(j); /* try removing -e/ance altogether (disappearance/disappear) */
      k = j - 1;
      if (lookup(word)) {
        return true;
      }
      word.append(word_char);  /* restore the original ending */
      word.append("nce");
      k = old_k;
    }
    return false;
  }

  /* handle -ness */
  private final boolean nessEndings() {
    if (endsIn("ness")) {     /* this is a very productive endings, so just accept it */
      word.setLength(j + 1);
      k = j;
      if (word.charAt(j) == 'i') {
        word.setCharAt(j, 'y');
      }
      return true;
    }
    return false;
  }

  /* handle -ism */
  private final boolean ismEndings() {
    if (endsIn("ism")) {    /* this is a very productive ending, so just accept it */
      word.setLength(j + 1);
      k = j;
      return true;
    }
    return false;
  }

  /* this routine deals with -ment endings. */
  private final boolean mentEndings() {
    int old_k = k;

    if (endsIn("ment")) {
      word.setLength(j + 1);
      k = j;
      if (lookup(word)) {
        return true;
      }
      word.append("ment");
      k = old_k;
    }
    return false;
  }

  /* this routine deals with -ize endings. */
  private final boolean izeEndings() {
    int old_k = k;

    if (endsIn("ize")) {
      word.setLength(j + 1);       /* try removing -ize entirely */
      k = j;
      if (lookup(word)) {
        return true;
      }
      word.append('i');

      if (doubleC(j)) {      /* allow for a doubled consonant */
        word.setLength(j);
        k = j - 1;
        if (lookup(word)) {
          return true;
        }
        word.append(word.charAt(j - 1));
      }

      word.setLength(j + 1);
      word.append('e');        /* try removing -ize and adding -e */
      k = j + 1;
      if (lookup(word)) {
        return true;
      }
      word.setLength(j + 1);
      word.append("ize");
      k = old_k;
    }
    return false;
  }

  /* handle -ency and -ancy */
  private final boolean ncyEndings() {
    if (endsIn("ncy")) {
      if (!((word.charAt(j) == 'e') || (word.charAt(j) == 'a'))) {
        return true;
      }
      word.setCharAt(j + 2, 't');  /* try converting -ncy to -nt */
      word.setLength(j + 3);
      k = j + 2;

      if (lookup(word)) {
        return true;
      }

      word.setCharAt(j + 2, 'c');  /* the default is to convert it to -nce */
      word.append('e');
      k = j + 3;
      return true;
    }
    return false;
  }

  /* handle -able and -ible */
  private final boolean bleEndings() {
    int old_k = k;
    char word_char;

    if (endsIn("ble")) {
      if (!((word.charAt(j) == 'a') || (word.charAt(j) == 'i'))) {
        return true;
      }
      word_char = word.charAt(j);
      word.setLength(j);         /* try just removing the ending */
      k = j - 1;
      if (lookup(word)) {
        return true;
      }
      if (doubleC(k)) {          /* allow for a doubled consonant */
        word.setLength(k);
        k--;
        if (lookup(word)) {
          return true;
        }
        k++;
        word.append(word.charAt(k - 1));
      }
      word.setLength(j);
      word.append('e');   /* try removing -a/ible and adding -e */
      k = j;
      if (lookup(word)) {
        return true;
      }
      word.setLength(j);
      word.append("ate"); /* try removing -able and adding -ate */
      /* (e.g., compensable/compensate)     */
      k = j + 2;
      if (lookup(word)) {
        return true;
      }
      word.setLength(j);
      word.append(word_char);        /* restore the original values */
      word.append("ble");
      k = old_k;
    }
    return false;
  }

  /* handle -ic endings.   This is fairly straightforward, but this is
  also the only place we try *expanding* an ending, -ic -> -ical.
  This is to handle cases like `canonic' -> `canonical' */
  private final boolean icEndings() {
    if (endsIn("ic")) {
      word.setLength(j + 3);
      word.append("al");        /* try converting -ic to -ical */
      k = j + 4;
      if (lookup(word)) {
        return true;
      }

      word.setCharAt(j + 1, 'y');        /* try converting -ic to -y */
      word.setLength(j + 2);
      k = j + 1;
      if (lookup(word)) {
        return true;
      }

      word.setCharAt(j + 1, 'e');        /* try converting -ic to -e */
      if (lookup(word)) {
        return true;
      }

      word.setLength(j + 1); /* try removing -ic altogether */
      k = j;
      if (lookup(word)) {
        return true;
      }
      word.append("ic"); /* restore the original ending */
      k = j + 2;
    }
    return false;
  }

  /* handle some derivational endings */
  /* this routine deals with -ion, -ition, -ation, -ization, and -ication.  The
  -ization ending is always converted to -ize */
  private final boolean ionEndings() {
    int old_k = k;

    if (endsIn("ization")) {   /* the -ize ending is very productive, so simply accept it as the root */
      word.setLength(j + 3);
      word.append('e');
      k = j + 3;
      return true;
    }


    if (endsIn("ition")) {
      word.setLength(j + 1);
      word.append('e');
      k = j + 1;
      if (lookup(word)) /* remove -ition and add `e', and check against the dictionary */ {
        return true;
      }

      /* restore original values */
      word.setLength(j + 1);
      word.append("ition");
      k = old_k;
    }


    if (endsIn("ation")) {
      word.setLength(j + 3);
      word.append('e');
      k = j + 3;
      if (lookup(word)) /* remove -ion and add `e', and check against the dictionary */ {
        return true;
      }

      word.setLength(j + 1);
      word.append('e');   /* remove -ation and add `e', and check against the dictionary */
      k = j + 1;
      if (lookup(word)) {
        return true;
      }

      word.setLength(j + 1);/* just remove -ation (resignation->resign) and check dictionary */
      k = j;
      if (lookup(word)) {
        return true;
      }

      /* restore original values */
      word.setLength(j + 1);
      word.append("ation");
      k = old_k;
    }


    /* test -ication after -ation is attempted (e.g., `complication->complicate'
    rather than `complication->comply') */

    if (endsIn("ication")) {
      word.setLength(j + 1);
      word.append('y');
      k = j + 1;
      if (lookup(word)) /* remove -ication and add `y', and check against the dictionary */ {
        return true;
      }

      /* restore original values */
      word.setLength(j + 1);
      word.append("ication");
      k = old_k;
    }


    if (endsIn("ion")) {
      word.setLength(j + 1);
      word.append('e');
      k = j + 1;
      if (lookup(word)) /* remove -ion and add `e', and check against the dictionary */ {
        return true;
      }

      word.setLength(j + 1);
      k = j;
      if (lookup(word)) /* remove -ion, and if it's found, treat that as the root */ {
        return true;
      }

      /* restore original values */
      word.setLength(j + 1);
      word.append("ion");
      k = old_k;
    }

    return false;
  }

  /* this routine deals with -er, -or, -ier, and -eer.  The -izer ending is always converted to
  -ize */
  private final boolean erAndOrEndings() {
    int old_k = k;

    char word_char;                 /* so we can remember if it was -er or -or */

    if (endsIn("izer")) {          /* -ize is very productive, so accept it as the root */
      word.setLength(j + 4);
      k = j + 3;
      return true;
    }

    if (endsIn("er") || endsIn("or")) {
      word_char = word.charAt(j + 1);
      if (doubleC(j)) {
        word.setLength(j);
        k = j - 1;
        if (lookup(word)) {
          return true;
        }
        word.append(word.charAt(j - 1));       /* restore the doubled consonant */
      }


      if (word.charAt(j) == 'i') {         /* do we have a -ier ending? */
        word.setCharAt(j, 'y');
        word.setLength(j + 1);
        k = j;
        if (lookup(word)) /* yes, so check against the dictionary */ {
          return true;
        }
        word.setCharAt(j, 'i');             /* restore the endings */
        word.append('e');
      }


      if (word.charAt(j) == 'e') {         /* handle -eer */
        word.setLength(j);
        k = j - 1;
        if (lookup(word)) {
          return true;
        }
        word.append('e');
      }

      word.setLength(j + 2); /* remove the -r ending */
      k = j + 1;
      if (lookup(word)) {
        return true;
      }
      word.setLength(j + 1); /* try removing -er/-or */
      k = j;
      if (lookup(word)) {
        return true;
      }
      word.append('e');    /* try removing -or and adding -e */
      k = j + 1;
      if (lookup(word)) {
        return true;
      }
      word.setLength(j + 1);
      word.append(word_char);
      word.append('r');    /* restore the word to the way it was */
      k = old_k;
    }

    return false;
  }

  /* this routine deals with -ly endings.  The -ally ending is always converted to -al
  Sometimes this will temporarily leave us with a non-word (e.g., heuristically
  maps to heuristical), but then the -al is removed in the next step.  */
  private final boolean lyEndings() {
    int old_k = k;

    if (endsIn("ly")) {

      word.setCharAt(j + 2, 'e');             /* try converting -ly to -le */

      if (lookup(word)) {
        return true;
      }
      word.setCharAt(j + 2, 'y');

      word.setLength(j + 1);         /* try just removing the -ly */
      k = j;

      if (lookup(word)) {
        return true;
      }

      if ((j > 0) && (word.charAt(j - 1) == 'a') && (word.charAt(j) == 'l')) /* always convert -ally to -al */ {
        return true;
      }
      word.append("ly");
      k = old_k;

      if ((j > 0) && (word.charAt(j - 1) == 'a') && (word.charAt(j) == 'b')) {  /* always convert -ably to -able */
        word.setCharAt(j + 2, 'e');
        k = j + 2;
        return true;
      }

      if (word.charAt(j) == 'i') {        /* e.g., militarily -> military */
        word.setLength(j);
        word.append('y');
        k = j;
        if (lookup(word)) {
          return true;
        }
        word.setLength(j);
        word.append("ily");
        k = old_k;
      }

      word.setLength(j + 1); /* the default is to remove -ly */

      k = j;
      return true;
    }
    return false;
  }

  /* this routine deals with -al endings.  Some of the endings from the previous routine
  are finished up here.  */
  private final boolean alEndings() {
    int old_k = k;

    if (word.length() < 4) {
      return false;
    }
    if (endsIn("al")) {
      word.setLength(j + 1);
      k = j;
      if (lookup(word)) /* try just removing the -al */ {
        return true;
      }

      if (doubleC(j)) {            /* allow for a doubled consonant */
        word.setLength(j);
        k = j - 1;
        if (lookup(word)) {
          return true;
        }
        word.append(word.charAt(j - 1));
      }

      word.setLength(j + 1);
      word.append('e');              /* try removing the -al and adding -e */
      k = j + 1;
      if (lookup(word)) {
        return true;
      }

      word.setLength(j + 1);
      word.append("um");    /* try converting -al to -um */
      /* (e.g., optimal - > optimum ) */
      k = j + 2;
      if (lookup(word)) {
        return true;
      }

      word.setLength(j + 1);
      word.append("al");    /* restore the ending to the way it was */
      k = old_k;

      if ((j > 0) && (word.charAt(j - 1) == 'i') && (word.charAt(j) == 'c')) {
        word.setLength(j - 1); /* try removing -ical  */
        k = j - 2;
        if (lookup(word)) {
          return true;
        }

        word.setLength(j - 1);
        word.append('y');/* try turning -ical to -y (e.g., bibliographical) */
        k = j - 1;
        if (lookup(word)) {
          return true;
        }

        word.setLength(j - 1);
        word.append("ic"); /* the default is to convert -ical to -ic */
        k = j;
        return true;
      }

      if (word.charAt(j) == 'i') {        /* sometimes -ial endings should be removed */
        word.setLength(j); /* (sometimes it gets turned into -y, but we */
        k = j - 1;                  /* aren't dealing with that case for now) */
        if (lookup(word)) {
          return true;
        }
        word.append("ial");
        k = old_k;
      }
    }
    return false;
  }

  /* this routine deals with -ive endings.  It normalizes some of the
  -ative endings directly, and also maps some -ive endings to -ion. */
  private final boolean iveEndings() {
    int old_k = k;

    if (endsIn("ive")) {
      word.setLength(j + 1);     /* try removing -ive entirely */
      k = j;
      if (lookup(word)) {
        return true;
      }

      word.append('e');          /* try removing -ive and adding -e */
      k = j + 1;
      if (lookup(word)) {
        return true;
      }
      word.setLength(j + 1);
      word.append("ive");
      if ((j > 0) && (word.charAt(j - 1) == 'a') && (word.charAt(j) == 't')) {
        word.setCharAt(j - 1, 'e');       /* try removing -ative and adding -e */
        word.setLength(j);        /* (e.g., determinative -> determine) */
        k = j - 1;
        if (lookup(word)) {
          return true;
        }
        word.setLength(j - 1); /* try just removing -ative */
        if (lookup(word)) {
          return true;
        }

        word.append("ative");
        k = old_k;
      }

      /* try mapping -ive to -ion (e.g., injunctive/injunction) */
      word.setCharAt(j + 2, 'o');
      word.setCharAt(j + 3, 'n');
      if (lookup(word)) {
        return true;
      }

      word.setCharAt(j + 2, 'v');       /* restore the original values */
      word.setCharAt(j + 3, 'e');
      k = old_k;
    }
    return false;
  }

  /** Create a KrovetzStemmer
   */
  public KStem() {
//    MaxCacheSize = DEFAULT_CACHE_SIZE;
    if (dict_ht == null) {
      initializeDictHash();
    }
  }

  /** Returns the stem of a word.
   *  @param term The word to be stemmed.
   *  @return The stem form of the term.
   */
  public final boolean stemTerm(MString term) {
//    if (stem_ht == null) {
//      initializeStemHash();
//    }

    word = term;
    k = term.length() - 1;
    /* If the word is too long or too short, or not
    entirely alphabetic, just lowercase copy it
    into stem and return */
    if ((k <= 1) || (k >= MaxWordLen - 1)) {
      return false;
    }

    /* This while loop will never be executed more than one time;
    it is here only to allow the break statement to be used to escape
    as soon as a word is recognized */

    DictEntry entry;

    boolean changed = false;
    while (true) {
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= plural();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= pastTense();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= aspect();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= ityEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= nessEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= ionEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= erAndOrEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= lyEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= alEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= iveEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= izeEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= mentEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= bleEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= ismEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= icEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= ncyEndings();
      entry = wordInDict();
      if (entry != null) {
        break;
      }
      changed |= nceEndings();
      entry = wordInDict();
      break;
    }


    /* try for a direct mapping (allows for cases like `Italian'->`Italy' and
    `Italians'->`Italy')
     */
    if (entry != null) {
      if (entry.root != null) {
        word.copyFrom(entry.root);
        return true;
      } else {
        //return word;
        return changed;
      }
    } else {
      return changed;
      //return word;
    }
  }

  public static void main(String[] args) throws IOException {
    initializeDictHash();
    try (PrintWriter out = IO.openPrintWriter("krovetz_data.tsv.gz")) {
      dict_ht.forEach((key, entry) -> {
        if(entry.root == null) {
          out.println(key);
        } else if(entry.exception) {
          out.println(key+"\t"+entry.root+"\t1");
        } else {
          out.println(key+"\t"+entry.root);
        }
      });
    }
  }
}

