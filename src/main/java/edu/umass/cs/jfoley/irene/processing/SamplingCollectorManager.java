package edu.umass.cs.jfoley.irene.processing;

import ciir.jfoley.chai.collections.TopKHeap;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Scorer;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

/**
 * The following manager collects at most "requested" hits, but only scoring at sampleRate.
 * e.g. Collect 1000 results, but save time by only evaluating every 10 matching documents: requested=1000, sampleRate=0.1. Since we're uniform sampling from matches, you may not get 1000 hits, even if that's the expected value of results.
 * @author jfoley
 */
public class SamplingCollectorManager extends IreneProcessingModel {
  private final double sampleRate;

  public SamplingCollectorManager(int requested, double sampleRate) {
    super(requested);
    this.sampleRate = sampleRate;
    assert(sampleRate > 0 && sampleRate <= 1) : "Sample Rate must be larger than zero to make sense, sr="+sampleRate;
  }

  @Override
  public void score(Scorer scorer, int doc, TopKHeap<ScoreDoc> output) throws IOException {
    // don't do slow random generation if not necessary.
    if(sampleRate >= 1) {
      output.offer(new ScoreDoc(doc, scorer.score()));
      return;
    }
    final ThreadLocalRandom rand = ThreadLocalRandom.current();
    if(rand.nextDouble() <= sampleRate) {
      output.offer(new ScoreDoc(doc, scorer.score()));
    }
  }
}
