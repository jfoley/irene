package edu.umass.cs.jfoley.irene;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley
 */
public class AnalyzerUtil {
  public static List<String> tokenize(Analyzer analyzer, String input) throws IOException {
    List<String> tokens = new ArrayList<>();
    TokenStream body = analyzer.tokenStream("body", input);
    CharTermAttribute charTermAttr = body.addAttribute(CharTermAttribute.class);

    body.reset();
    while(body.incrementToken()) {
      String term = charTermAttr.toString();
      tokens.add(term);
    }
    body.close();

    return tokens;
  }
}
