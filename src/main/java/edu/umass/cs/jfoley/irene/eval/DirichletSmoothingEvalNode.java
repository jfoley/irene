package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import org.apache.lucene.search.Explanation;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;

/**
 * @author jfoley
 */
@QueryOperator("dirichlet")
@TermDataNeeded(DataNeeded.COUNTS)
public class DirichletSmoothingEvalNode extends SingleChildQueryEvalNode<CountEvalNode> {
  public static final double DefaultMu = 1500;
  final double mu;
  final double background;
  private final long collectionFreq;
  private final double collectionLength;
  public static boolean normify = false;

  public DirichletSmoothingEvalNode(CountEvalNode child, double mu) {
    super(child);
    IreneCountStatistics stats = child.getCountStatistics();

    this.collectionFreq = stats.collectionFrequency;
    this.collectionLength = stats.collectionLength;
    background = mu * Math.max(collectionFreq, 0.5) / collectionLength;
    this.mu = mu;
  }

  @Override
  public float score(int doc) throws IOException {
    int count = child.count(doc);
    int length = child.length(doc);
    double numerator = count + background;
    double denominator = length + mu;

    // if lucenify:
    //if(normify) {
      //double bottom = Math.log(SmallFloat.byte315ToFloat(SmallFloat.floatToByte315((float) (1.0f / denominator))));
      //return (float) (Math.log(numerator) + bottom);
    //}

    return (float) (Math.log(numerator / denominator));
  }

  @Override
  public int count(int doc) throws IOException {
    return child.count(doc);
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    float score = score(doc);
    String desc = Parameters.parseArray("count", child.count(doc), "length", child.length(doc), "mu", mu, "clen", collectionLength, "cf", collectionFreq).toString();
    return Explanation.match(score, desc, child.explain(doc));
  }
}
