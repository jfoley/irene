package edu.umass.cs.jfoley.irene.expr;

import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Weight;

import java.io.IOException;

/**
 * @author jfoley.
 */
public class QueryUsage {
  public boolean needsScores;
  Query luceneQuery;

  public QueryUsage(Query q, boolean needsScores) {
    this.luceneQuery = q;
    this.needsScores = needsScores;
  }

  public Weight createWeight(IndexSearcher searcher) throws IOException {
    return luceneQuery.createWeight(searcher, needsScores);
  }
}
