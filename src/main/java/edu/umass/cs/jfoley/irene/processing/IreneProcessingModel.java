package edu.umass.cs.jfoley.irene.processing;

import ciir.jfoley.chai.collections.TopKHeap;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * @author jfoley
 */
public abstract class IreneProcessingModel implements CollectorManager<IreneProcessingModel.HeapCollector, TopDocs> {
  public static final Comparator<ScoreDoc> cmp = (lhs, rhs) -> Float.compare(lhs.score, rhs.score);
  public final int requested;

  public IreneProcessingModel(int requested) {
    this.requested = requested;
  }

  /**
   * Implement this function to score individual documents. All parameters are provided for thread-safety. Synchronize access to all other information.
   * @param scorer to calculate a score if necessary.
   * @param doc the document number.
   * @param output the accumulating heap for this thread.
   */
  public abstract void score(Scorer scorer, int doc, TopKHeap<ScoreDoc> output) throws IOException;

  @Override
  public HeapCollector newCollector() throws IOException {
    return new HeapCollector(this, requested);
  }

  @Override
  public TopDocs reduce(Collection<HeapCollector> collectors) throws IOException {
    TopKHeap<ScoreDoc> finalHeap = new TopKHeap<>(requested, cmp);
    for (HeapCollector collector : collectors) {
      for (ScoreDoc scoreDoc : collector.heap.getUnsortedList()) {
        finalHeap.offer(scoreDoc);
      }
    }

    if(finalHeap.isEmpty()) {
      return new TopDocs(0, new ScoreDoc[0], -Float.MAX_VALUE);
    }

    // final merge:
    ScoreDoc[] arr = new ScoreDoc[finalHeap.size()];
    List<ScoreDoc> sorted = finalHeap.getSorted();
    for (int i = 0; i < sorted.size(); i++) {
      ScoreDoc scoreDoc = sorted.get(i);
      arr[i] = scoreDoc;
    }
    return new TopDocs(Math.toIntExact(finalHeap.getTotalSeen()), arr, sorted.get(0).score);
  }

  static class HeapCollector implements Collector {
    private final TopKHeap<ScoreDoc> heap;
    private final IreneProcessingModel model;

    HeapCollector(IreneProcessingModel model, int requested) {
      this.model = model;
      this.heap = new TopKHeap<>(requested, cmp);
    }

    @Override
    public LeafCollector getLeafCollector(LeafReaderContext context) throws IOException {
      final int docBase = context.docBase;
      return new LeafCollector() {
        Scorer scorer;
        @Override
        public void setScorer(Scorer scorer) throws IOException {
          this.scorer = scorer;
        }

        @Override
        public void collect(int doc) throws IOException {
          model.score(scorer, doc+docBase, heap);
        }
      };
    }

    @Override
    public boolean needsScores() {
      return false;
    }
  }
}
