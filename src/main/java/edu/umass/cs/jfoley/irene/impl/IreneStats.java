package edu.umass.cs.jfoley.irene.impl;

import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.TermStatistics;
import org.apache.lucene.search.similarities.Similarity;

/**
 * @author jfoley
 */
public class IreneStats extends Similarity.SimWeight {
  public final CollectionStatistics collectionStatistics;
  public final TermStatistics[] termStatistics;

  public IreneStats(CollectionStatistics collectionStatistics, TermStatistics[] termStatistics) {
    this.collectionStatistics = collectionStatistics;
    this.termStatistics = termStatistics;
  }

  public String getField() {
    return collectionStatistics.field();
  }

  @Override
  public float getValueForNormalization() {
    return 1.0f;
  }

  @Override
  public void normalize(float queryNorm, float topLevelBoost) {
  }
}
