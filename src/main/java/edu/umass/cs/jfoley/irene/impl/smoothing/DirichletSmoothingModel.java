package edu.umass.cs.jfoley.irene.impl.smoothing;

import edu.umass.cs.jfoley.irene.eval.DirichletSmoothingEvalNode;
import edu.umass.cs.jfoley.irene.eval.QueryEvalNode;
import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;

/**
 * @author jfoley
 */
public class DirichletSmoothingModel implements SmoothingModel {
  public double mu;

  public DirichletSmoothingModel() {
    this(DirichletSmoothingEvalNode.DefaultMu);
  }
  public DirichletSmoothingModel(double mu) {
    this.mu = mu;
  }

  @Override
  public String toString() {
    return "DirichletSmoothing(mu="+mu+")";
  }

  @Override
  public QueryEvalNode makeScorer(LeafEvalNode leafNode) {
    return new DirichletSmoothingEvalNode(leafNode, mu);
  }
}
