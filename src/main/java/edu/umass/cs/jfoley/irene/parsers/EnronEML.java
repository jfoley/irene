package edu.umass.cs.jfoley.irene.parsers;

import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.inputs.InputContainer;
import ciir.jfoley.chai.io.inputs.InputFinder;
import ciir.jfoley.chai.io.inputs.InputStreamable;
import ciir.jfoley.chai.string.StrUtil;
import ciir.jfoley.chai.time.Debouncer;
import edu.umass.cs.jfoley.irene.IreneEnglishAnalyzer;
import edu.umass.cs.jfoley.irene.impl.IreneSimilarity;
import gnu.trove.map.hash.TIntIntHashMap;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.parser.AbstractContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.james.mime4j.stream.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.json.JSONParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author jfoley
 */
public class EnronEML {

  /**
   * Class to handle callbacks from MIME4j parser.
   */
  private static class EnronContentHandler extends AbstractContentHandler {
    Parameters fields = Parameters.create();
    List<String> multipartText = new ArrayList<>();

    @Override
    public void field(Field field) throws MimeException {
      String key = field.getName();
      String value = field.getBody();
      fields.put(key, value);
    }

    @Override
    public void body(BodyDescriptor bodyDescriptor, InputStream inputStream) throws MimeException, IOException {

      String cd = fields.get("Content-Disposition", (String) null);
      if(cd != null && cd.startsWith("attachment; filename=")) {
        JSONParser parser = new JSONParser(new StringReader(cd), cd);
        String fileName = parser.parseString();
        String extension = StrUtil.takeAfterLast(fileName, '.').toLowerCase();
        fields.put("attachment", fileName);
        fields.put("attachment-extension", extension);
      }

      switch (bodyDescriptor.getMediaType()) {
        case "video":
        case "image":
        case "audio":
          return;
      }

      String mimeType = bodyDescriptor.getMimeType();
      switch (mimeType) {
        case "text/plain":
          multipartText.add(IO.slurp(new QuotedPrintableInputStream(inputStream)));
          break;
        case "text/xml":
        case "text/html": {
          // parse HTML
          Document parse = Jsoup.parse(new QuotedPrintableInputStream(inputStream), "UTF-8", "/");
          multipartText.add(parse.text());
        } break;
        case "application/vnd.ms-excel":
          fields.extendList("excel", true);
          break;
        case "text/css":
        case "application/x-javascript":
        case "application/x-mspublisher":
        case "application/msword":
        case "application/vnd.ms-project":
        case "application/rtf":
        case "application/zip":
        case "application/pdf":
        case "application/vnd.ms-powerpoint":
        case "application/vnd.ms-word":
        case "text/x-vcard":
        case "application/x-pkcs12":
          break;
        case "text/csv":
        case "application/octet-stream":
          if(cd != null && cd.startsWith("attachment; filename=")) {
            JSONParser parser = new JSONParser(new StringReader(cd), cd);
            String fileName = parser.parseString();
            String extension = StrUtil.takeAfterLast(fileName, '.').toLowerCase();
            switch (extension) {
              case "xls":
                fields.extendList("excel", true);
                break;
              case "csv":
                fields.extendList("csv", fileName);
                break;
              case "eml":
                fields.extendList("embedded-message", fileName);
                break;
              default:
                if(StrUtil.looksLikeInt(extension)) {
                  break;
                }
            }
            break;
          }
          break;
        default:
          System.out.println("\tBody Detected: "+bodyDescriptor.getMimeType()+" "+bodyDescriptor.getTransferEncoding());
      }

    }
  }

  static final String licenseStars = "***********";
  static final String license = "EDRM Enron Email Data Set has been produced in EML, PST and NSF format by ZL Technologies, Inc. This Data Set is licensed under a Creative Commons Attribution 3.0 United States License <http://creativecommons.org/licenses/by/3.0/us/> . To provide attribution, please cite to \"ZL Technologies, Inc. (http://www.zlti.com).\"";

  static final SimpleDateFormat datePattern = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss ZZZZZ", Locale.US);
  static {
    datePattern.setLenient(true);
  }

  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    InputFinder finder = InputFinder.Default();

    List<? extends InputContainer> containers = finder.findAllInputs("/mnt/scratch3/jfoley/enron_spreadsheets/enron_emls.zip");
    int i = 0;

    int SampleSize = 800000;
    //int SampleSize = 80;

    TIntIntHashMap counts = new TIntIntHashMap();


    try (final FSDirectory dir = FSDirectory.open(Paths.get(argp.get("output", "/mnt/scratch3/jfoley/enron.irene")))) {
      final IndexWriterConfig cfg = new IndexWriterConfig(new IreneEnglishAnalyzer());
      cfg.setSimilarity(new IreneSimilarity());
      cfg.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

      try (IndexWriter writer = new IndexWriter(dir, cfg)) {

        Debouncer msg = new Debouncer();
        for (InputContainer container : containers) {
          for (InputStreamable is : container.getInputs()) {
            EnronContentHandler ech = new EnronContentHandler();
            try {
              try (InputStream reader = is.getInputStream()) {
                MimeStreamParser mimeParser = new MimeStreamParser();
                mimeParser.setContentHandler(ech);
                mimeParser.parse(reader);
              } catch (MimeException e) {
                e.printStackTrace();
                continue;
              }

              int numExcel = 0;
              if (ech.fields.containsKey("excel")) {
                numExcel = ech.fields.getAsList("excel").size();
                counts.adjustOrPutValue(numExcel, 1, 1);
              } else {
                counts.adjustOrPutValue(0, 1, 1);
              }
              //System.out.println(ech.fields.toPrettyString());
              //System.out.println(ech.multipartText);

              StringBuilder body = new StringBuilder();
              for (String text : ech.multipartText) {
                int pos = text.lastIndexOf(license);
                if (pos > 0) {
                  int p2 = text.lastIndexOf(licenseStars, pos);
                  if (p2 > 0) {
                    pos = p2;
                  }
                  body.append(StrUtil.slice(text, 0, pos));
                }
              }
              //System.out.println(body.toString());

              String id = StrUtil.takeBetween(ech.fields.getString("Message-ID"), "<", "@Felienne-Pro>").toLowerCase();

              long epochSecond = 0;
              try {
                epochSecond = datePattern.parse(ech.fields.getString("Date")).toInstant().getEpochSecond();
              } catch (Exception e) {
                e.printStackTrace();
              }
              //System.out.println(ech.fields);

              org.apache.lucene.document.Document doc = new org.apache.lucene.document.Document();
              doc.add(new StringField("id", id, Store.YES));
              doc.add(new TextField("body", body.toString(), Store.YES));
              doc.add(new TextField("from", ech.fields.get("From", ""), Store.YES));
              doc.add(new TextField("to", ech.fields.get("To", ""), Store.YES));
              doc.add(new TextField("cc", ech.fields.get("Cc", ""), Store.YES));
              doc.add(new TextField("subject", ech.fields.get("Subject", ""), Store.YES));
              doc.add(new IntField("storedExcelCount", numExcel, Store.YES));
              doc.add(new NumericDocValuesField("excel", numExcel));
              doc.add(new StringField("attachmentExtension", ech.fields.get("attachment-extension", "N/A"), Store.YES));
              doc.add(new StringField("priority", ech.fields.get("X-Priority", "3 (Normal)"), Store.YES));
              doc.add(new StringField("attachment", ech.fields.get("attachment", "N/A"), Store.YES));
              doc.add(new LongField("storedTimeStamp", epochSecond, Store.YES));
              doc.add(new NumericDocValuesField("timeStamp", epochSecond));

              writer.addDocument(doc);

              i++;
              if (msg.ready()) {
                System.err.println(msg.estimate(i, SampleSize));
              }
              if (i > SampleSize) {
                break;
              }
            } catch (Throwable err) {
              System.err.println(ech.fields);
              err.printStackTrace(System.err);
            }
          }
          if (i > SampleSize) {
            break;
          }
        }
      }
    }

    System.out.println(counts);

    // done
  }
}
