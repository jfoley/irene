package edu.umass.cs.jfoley.irene.expr.macro;

import edu.umass.cs.jfoley.irene.expr.QExpr;
import edu.umass.cs.jfoley.irene.expr.types.InputTypeChecker;
import edu.umass.cs.jfoley.irene.expr.types.OperatorType;
import gnu.trove.set.hash.TIntHashSet;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley
 */
public class InsertScorersMacro extends Macro {
  private final String scorerToInsert;

  public InsertScorersMacro(Parameters cfg) {
    super(cfg);
    this.scorerToInsert = cfg.get("scorer", "dirichlet");
  }

  @Nullable
  @Override
  public QExpr transform(QExpr input) {
    InputTypeChecker checker = factory.getInputRequirements(input);
    List<OperatorType> inputTypes = new ArrayList<>();
    for (QExpr child : input.children) {
      inputTypes.add(factory.getOutputType(child));
    }
    TIntHashSet needsScorer = new TIntHashSet();
    InputTypeChecker.TypeCheckResult result = checker.satisfies(inputTypes);
    if(result instanceof InputTypeChecker.TypeCheckMismatches) {
      for (InputTypeChecker.TypeMismatch error : ((InputTypeChecker.TypeCheckMismatches) result).errors) {
        if(error.found.hasCounts() && error.required.hasScores()) {
          needsScorer.add(error.position);
        }
      }
    }

    // if nothing needs patching, return early, recursing.
    if(needsScorer.isEmpty()) {
      return input.copy(transform(input.children));
    }

    // copy children, inserting scorer as appropriate:
    List<QExpr> newChildren = new ArrayList<>();
    for (int i = 0; i < input.children.size(); i++) {
      // recurse on child:
      QExpr child = transformNonEmpty(input.children.get(i));
      if(needsScorer.contains(i)) {
        final String newScorer = child.config.get("scorer", input.config.get("scorer", scorerToInsert));
        newChildren.add(new QExpr(newScorer, child));
      } else {
        newChildren.add(child);
      }
    }

    return input.copy(newChildren);
  }
}
