package edu.umass.cs.jfoley.irene.eval;

import ciir.jfoley.chai.collections.util.ListFns;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;

import java.io.IOException;
import java.util.List;

/**
 * @author jfoley
 */
@QueryOperator("countsum")
public class CountSum implements QueryEvalNode {
  private final List<QueryEvalNode> children;
  private long numHits;

  public CountSum(List<QueryEvalNode> children) {
    this.children = children;
    this.numHits = 0L;
    for (QueryEvalNode child : children) {
      numHits = Math.max(child.estimateDF(), numHits);
    };
    this.children.sort(mostToLeastFrequent);
  }

  @Override
  public float score(int doc) throws IOException {
    return count(doc);
  }

  @Override
  public int count(int doc) throws IOException {
    int count = 0;
    for (QueryEvalNode child : children) {
      count += child.count(doc);
    }
    //System.err.println("CountSum! doc: "+doc+" count: "+count);
    return count;
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    int ct = count(doc);
    if(ct > 0) {
      List<Explanation> childExpl = ListFns.map(children, x -> {
        try { return x.explain(doc); } catch (IOException e) { throw new RuntimeException(e); }
      });

      return Explanation.match(ct, "CountSum", childExpl);
    }
    return Explanation.noMatch("CountSum");
  }

  @Override
  public boolean matches(int id) throws IOException {
    //System.out.println("CountSum::matches("+id+")");
    for (QueryEvalNode child : children) {
      //System.out.println("CountSum["+i+"]::matches(" + id + ")="+child.matches(id)+" child.explain="+child.explain(i));
      if (child.matches(id)) return true;
    }
    return false;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createOrMover(children);
  }

  @Override
  public long estimateDF() {
    return numHits;
  }
}
