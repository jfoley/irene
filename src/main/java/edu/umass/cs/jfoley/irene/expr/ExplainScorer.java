package edu.umass.cs.jfoley.irene.expr;

import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.Scorer;

import java.io.IOException;

/**
 * @author jfoley
 */
public abstract class ExplainScorer extends Scorer {
  protected ExplainScorer() {
    super(null);
  }

  public void setMinimumRequiredScore(float score) { }

  public abstract Explanation explain(int doc) throws IOException;
}
