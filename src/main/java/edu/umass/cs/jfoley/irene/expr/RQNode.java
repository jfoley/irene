package edu.umass.cs.jfoley.irene.expr;

import ciir.jfoley.chai.collections.util.ListFns;
import org.lemurproject.galago.utility.Parameters;

import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley
 */
public interface RQNode {

  enum Type {
    COUNTS,
    POSITIONS,
    SCORES,
  }

  Type getType();
  Parameters toJSON();

  @interface QArg { }

  class TermNode implements RQNode {
    final String field;
    final String term;

    public TermNode(String field, String term) {
      this.term = term;
      this.field = field;
    }

    @Override
    public Type getType() {
      return Type.POSITIONS;
    }

    public Parameters toJSON() {
      return Parameters.parseArray(
          "kind", "TermNode",
          "term", term,
          "field", field
      );
    }
  }

  interface SmoothingModel {
    Parameters toJSON();
  }
  class LinearSmoothing implements SmoothingModel {
    @QArg double lambda = 0.8;

    @Override
    public Parameters toJSON() {
      return Parameters.parseArray(
          "kind", "LinearSmoothing",
          "lambda", lambda
      );
    }
  }
  class DirichletSmoothing implements SmoothingModel {
    @QArg double mu = 1500;

    @Override
    public Parameters toJSON() {
      return Parameters.parseArray(
          "kind", "DirichletSmoothing",
          "mu", mu
      );
    }
  }

  class QueryLikelihoodNode implements RQNode {
    @QArg SmoothingModel smoothing = new DirichletSmoothing();
    List<RQNode> children = new ArrayList<>();

    @Override
    public Type getType() {
      return Type.SCORES;
    }

    @Override
    public Parameters toJSON() {
      return Parameters.parseArray(
          "kind", "QueryLikelihoodNode",
          "smoothing", smoothing.toJSON(),
          "children", ListFns.map(children, RQNode::toJSON)
      );
    }
  }

  class OrderedWindowNode implements RQNode {
    @QArg int width = 1;
    List<RQNode> children = new ArrayList<>();

    @Override
    public Type getType() { return Type.POSITIONS; }

    @Override
    public Parameters toJSON() {
      return Parameters.parseArray(
          "kind", "OrderedWindowNode",
          "width", width,
          "children", ListFns.map(children, RQNode::toJSON)
      );
    }
  }

  public static final class GNode {
    String operator;
    Parameters parameters;
    List<GNode> children;

    public GNode(String operator, Parameters parameters) {
      this(operator, parameters, new ArrayList<>());
    }
    public GNode(String operator, Parameters parameters, List<GNode> children) {
      this.operator = operator;
      this.parameters = parameters;
      this.children = children;
    }

    public static GNode text(String token) {
      return new GNode("text", Parameters.parseArray("token", token));
    }
    public static GNode term(String field, String token) {
      return new GNode("text", Parameters.parseArray("token", token, "field", field));
    }

  }

  public static void main(String[] args) throws ScriptException {
    OrderedWindowNode phrase = new OrderedWindowNode();
    phrase.children.add(new TermNode("body", "the"));
    phrase.children.add(new TermNode("body", "president"));
    QueryLikelihoodNode ql = new QueryLikelihoodNode();
    ql.children.add(phrase);

    System.err.println(ql.toJSON().toPrettyString());

    List<String> validQueries = new ArrayList<>();
    validQueries.add("#sdm(this is a query)");
    validQueries.add("#ql({smoothing:dirichlet} #od(the president))");
    validQueries.add("#ql(#od(the president))");


  }
}
