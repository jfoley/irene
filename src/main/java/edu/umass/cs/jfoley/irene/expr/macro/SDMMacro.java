package edu.umass.cs.jfoley.irene.expr.macro;

import edu.umass.cs.jfoley.irene.data.Stopwords;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author jfoley
 */
public class SDMMacro extends Macro {
  private final boolean stopwords;
  private final Set<String> stopList;

  public SDMMacro(Parameters cfg) {
    super(cfg);
    this.stopwords = cfg.get("stopwords", false);
    this.stopList = Stopwords.getInqueryList();
  }

  @Nullable
  @Override
  public QExpr transform(QExpr input) {
    if(Objects.equals(input.operator, "sdm")) {
      return makeSDM(input.children);
    }
    List<QExpr> newChildren = transform(input.children);
    return input.copy(newChildren);
  }

  private QExpr makeSDM(List<QExpr> children) {
    if(children.size() == 0) {
      return null;
    } else if(children.size() == 1) {
      return new QExpr("combine", children.get(0));
    }

    QExpr topLevelCombine = new QExpr("combine");

    double unigramWeight = cfg.get("uniw", 0.8);
    double orderedWeight = cfg.get("odw", 0.15);
    double unorderedWeight = cfg.get("uww", 0.05);
    int windowLimit = cfg.get("windowLimit", 2);

    String odOp = cfg.get("sdm.od.op", "od");
    int odWidth = cfg.get("sdm.od.width", 1);

    String uwOp = cfg.get("sdm.uw.op", "uw");
    int uwWidth = cfg.get("sdm.uw.width", 4);

    ArrayList<QExpr> unigram = new ArrayList<>(children.size());
    for (QExpr child : children) {
      if(stopwords && child.operator.equals("text")) {
        String text = factory.getTextOrNull(child);
        if(stopList.contains(text)) continue;
      }
      unigram.add(child.copy());
    }
    if(stopwords && unigram.isEmpty()) {
      unigram.addAll(children);
    }

    // ordered and unordered can go at the same time
    ArrayList<QExpr> ordered = new ArrayList<>();
    ArrayList<QExpr> unordered = new ArrayList<>();

    for (int n = 2; n <= windowLimit; n++) {
      for (int i = 0; i < (children.size() - n + 1); i++) {
        List<QExpr> seq = children.subList(i, i + n);

        QExpr odq = new QExpr(odOp, seq);
        odq.config.put("width", odWidth);

        // I don't know why Galago does this extra multiplication by seq.size... but it does.
        QExpr uwq = new QExpr(uwOp, seq);
        uwq.config.put("width", uwWidth*seq.size());

        ordered.add(odq);
        unordered.add(uwq);
      }
    }

    topLevelCombine.config.put("0", unigramWeight);
    topLevelCombine.config.put("1", orderedWeight);
    topLevelCombine.config.put("2", unorderedWeight);

    topLevelCombine.addChild(new QExpr("combine", unigram));
    topLevelCombine.addChild(new QExpr("combine", ordered));
    topLevelCombine.addChild(new QExpr("combine", unordered));

    return topLevelCombine;
  }
}
