package edu.umass.cs.jfoley.irene;

import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;

/**
 * @author jfoley
 */
public class CLIQuery {
  public static String readString(String prompt) throws IOException {
    System.out.print(prompt);
    StringBuilder sb = new StringBuilder();
    while(true) {
      int ch = System.in.read();
      if(ch == -1) break;
      if(ch == '\n' || ch == '\r') break;
      sb.append((char) ch);
    }
    return sb.toString();
  }

  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    try (IreneIndex index = new IreneIndex(argp.get("index", "/mnt/scratch3/jfoley/inex-pages.irene"))) {
      while(true) {
        String input = readString("q>");
        if(input.isEmpty()) break;
          long start = System.currentTimeMillis();
          QExpr query = index.simpleQuery(argp.get("model", "combine"), input);
          TopDocs results = index.search(query, 1000);
          long end = System.currentTimeMillis();
        System.err.println("Found " + results.totalHits +" in "+(end - start)+"ms.");
        ScoreDoc[] scoreDocs = results.scoreDocs;
        for (int i = 0; i < Math.min(10, scoreDocs.length); i++) {
          ScoreDoc scoreDoc = scoreDocs[i];
          int docId = scoreDoc.doc;
          Document doc = index.searcher.doc(docId);

          System.out.println("\t" + doc.get("id") + " " + scoreDoc.score);
          //System.out.println("\t" + AnalyzerUtil.tokenize(new IreneEnglishAnalyzer(), doc.get("body")));
          System.out.println("\t" + doc.get("body"));
          //System.out.println("\t" + index.searcher.explain(query, docId));
        }
      }
    }

  }
}
