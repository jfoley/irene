package edu.umass.cs.jfoley.irene.eval.opt;

import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Faster than #combine(), if you're doing dirichlet
 *
 * List&lt;OptChild&gt; is slower than the leafs/weights/backgrounds arrays by about 1 ms per query; this adds up to 400 or so milliseconds across Robust queries issued 9-10 times.
 * @author jfoley.
 */
@QueryOperator("optbm25")
@TermDataNeeded(DataNeeded.COUNTS)
public class SortedMaxscoreBM25 extends OptEvalNode {
  public final List<LeafEvalNode> children;
  private final float[] bestAfter;
  private final float[] minUntil;
  private final double b;
  private final double k;
  private long numHits;
  private final int N;
  protected int split;
  private int useful;
  public final List<OptBM25Child> cnodes;

  public final LeafEvalNode[] leafs;
  public final float[] weights;

  public SortedMaxscoreBM25(IreneIndex index, double b, double k, List<LeafEvalNode> children, float[] weights, String sort) {
    try {
      this.b = b;
      this.k = k;
      this.N = children.size();
      this.useful = N;
      this.children = new ArrayList<>(children);
      // TODO sort children a la BinImp
      this.numHits = 0L;
      // figure out current position and cost:
      int id = NO_MORE_DOCS;
      long maxCost = 0L;
      for (LeafEvalNode child : children) {
        id = Math.min(child.currentDocument(), id);
        maxCost = Math.max(child.estimateDF(), maxCost);
      }
      this.numHits = maxCost;
      this.current = id;

      cnodes = new ArrayList<>(N);
      for (int i = 0; i < N; i++) {
        LeafEvalNode child = this.children.get(i);
        OptBM25Child node = new OptBM25Child(index, b, k, weights[i], child);
        cnodes.add(node);
      }

      switch (sort) {
        case "max":
          cnodes.sort((lhs, rhs) -> -Double.compare(lhs.max, rhs.max));
          break;
        case "df":
          cnodes.sort(Comparator.comparingLong(lhs -> lhs.stats.documentFrequency));
          break;
        case "galago":
          cnodes.sort(Comparator.comparingDouble(lhs -> lhs.diff));
          break;
      }

      this.weights = new float[N];
      leafs = new LeafEvalNode[N];
      for (int i = 0; i < N; i++) {
        OptBM25Child c = cnodes.get(i);
        leafs[i] = c.node;
        this.weights[i] = c.weight;
      }

      float[] bestScore = new float[N];
      minUntil = new float[N];
      bestAfter = new float[N];
      float minSum = 0;

      for (int i = 0; i < N; i++) {
        OptBM25Child child = cnodes.get(i);
        bestScore[i] = (float) child.max;

        minSum += child.min;
        minUntil[i] = minSum;
      }

      float rvrSum = 0;
      for (int n = N-1; n >= 0; n--) {
        bestAfter[n] = rvrSum;
        rvrSum += bestScore[n];
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  // doesn't make any sense...
  public boolean hasNoMatches() { return children.size() == 0; }

  public static final float NegativeInfinity = -Float.MAX_VALUE;
  @Override
  public float score(int doc) throws IOException {
    if(current == doc) {
      // IF we reach here; Score OR unconditionally:
      float score = 0;
      for (int i = 0; i < split; i++) {
        score += cnodes.get(i).score(doc);
      }
      if(split > 0 && score + bestAfter[split-1] < heapMinimumScore) {
        return NegativeInfinity;
      }
      // Score rest conditionally only:
      for (int i = split; i<N; i++) {
        score += cnodes.get(i).score(doc);
        if(score + bestAfter[i] < heapMinimumScore) {
          return NegativeInfinity;
        }
      }
      return score;
    }
    return NegativeInfinity;
  }


  @Override
  public int count(int doc) throws IOException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    heapMinimumScore = NegativeInfinity;
    List<Explanation> children = new ArrayList<>(N);
    List<String> descs = new ArrayList<>(N);
    float myscore = 0;
    for (int i = 0; i < N; i++) {
      OptBM25Child optChild = cnodes.get(i);
      IreneCountStatistics stats = optChild.stats;
      int where = optChild.node.currentDocument();
      int length = optChild.node.length(doc);
      int count = optChild.node.count(doc);
      float weight = optChild.weight;
      Parameters descP = Parameters.parseArray("_doc", where, "weight", weight, "count", count, "length", length, "b", b, "k", k, "clen", stats.collectionLength, "cf", stats.collectionFrequency, "avgDL", stats.averageDocumentLength(), "idf", optChild.idf);
      double numerator = count * (k+1);
      double denominator = count + (k* (1-b + (b*length / optChild.avgDL)));
      double score = numerator / denominator;
      descP.put("wscore", weight * score);
      descP.put("score", score);
      myscore += weight * score;

      String desc = descP.toString()+"\n";
      descs.add(desc);
      children.add(optChild.node.explain(doc));
    }
    return Explanation.match(myscore, myscore+"\t"+descs.toString(), children);
  }

  private float heapMinimumScore = -Float.MAX_VALUE;
  @Override
  public void setMinimumRequiredScore(float theta) {
    heapMinimumScore = theta;

    this.split = 0;
    useful = N;
    for (int i = 0; i < N; i++) {
      double est = minUntil[i] + bestAfter[i];
      if(est < theta) {
        split = i+1;
        useful = split;
        break;
      }
    }
  }

  @Override
  public boolean matches(int id) throws IOException {
    if(current < id) {
      current = advance(id);
    }
    return current == id;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createOrMover(children);
  }

  @Override
  public long estimateDF() {
    return numHits;
  }

  /** as an iterator! */
  int current = 0;

  @Override
  public int docID() {
    return current;
  }

  @Override
  public int nextDoc() throws IOException {
    return advance(current+1);
  }

  @Override
  public int advance(int target) throws IOException {
    int nextMin = NO_MORE_DOCS;
    for (int i = 0; i < useful; i++) {
      LeafEvalNode child = leafs[i];
      int where = child.currentDocument();
      if (where < target) {
        where = child.advance(target);
      }
      //nextMin = Math.min(where, nextMin);
      nextMin = where < nextMin ? where : nextMin;
      if (nextMin == target) break;
    }

    current = nextMin;
    return nextMin;
  }

  @Override
  public long cost() {
    return numHits;
  }
}
