package edu.umass.cs.jfoley.irene.eval.opt;

import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Faster than #combine(), if you're doing dirichlet
 * @author jfoley.
 */
@QueryOperator("sdql")
@TermDataNeeded(DataNeeded.COUNTS)
public class WANDDQL extends OptEvalNode {
  private final double mu;
  public final List<LeafEvalNode> children;
  private final float[] bestAfter;
  private final float[] bestScore;
  private final float[] bestUntil;
  private long numHits;
  private final int N;
  protected int split;
  public final List<OptChild> cnodes;

  public final LeafEvalNode[] leafs;
  public final float[] weights;
  public final float[] backgrounds;

  public WANDDQL(IreneIndex index, double mu, List<LeafEvalNode> children, float[] weights) {
    try {
      this.mu = mu;
      this.N = children.size();
      this.children = new ArrayList<>(children);
      // TODO sort children a la BinImp
      this.numHits = 0L;
      // figure out current position and cost:
      int id = NO_MORE_DOCS;
      long maxCost = 0L;
      for (LeafEvalNode child : children) {
        id = Math.min(child.currentDocument(), id);
        maxCost = Math.max(child.estimateDF(), maxCost);
      }
      this.numHits = maxCost;
      this.current = id;

      cnodes = new ArrayList<>(N);
      for (int i = 0; i < N; i++) {
        LeafEvalNode child = this.children.get(i);
        OptChild node = new OptChild(index, mu, weights[i], child);
        cnodes.add(node);
      }

      cnodes.sort((lhs, rhs) -> -Double.compare(lhs.max, rhs.max));

      backgrounds = new float[N];
      this.weights = new float[N];
      leafs = new LeafEvalNode[N];
      for (int i = 0; i < N; i++) {
        OptChild c = cnodes.get(i);
        leafs[i] = c.node;
        this.weights[i] = c.weight;
        backgrounds[i] = (float) c.background;
      }

      bestScore = new float[N];
      bestUntil = new float[N];
      bestAfter = new float[N];
      float fwdSum = 0;

      for (int i = 0; i < N; i++) {
        OptChild child = cnodes.get(i);
        bestScore[i] = (float) child.max;

        fwdSum += child.max;
        bestUntil[i] = fwdSum;
      }

      float rvrSum = 0;
      for (int n = N-1; n >= 0; n--) {
        bestAfter[n] = rvrSum;
        rvrSum += bestScore[n];
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  // doesn't make any sense...
  public boolean hasNoMatches() { return children.size() == 0; }

  public static final float NegativeInfinity = -Float.MAX_VALUE;
  @Override
  public float score(int doc) throws IOException {
    if(current == doc) {
      // IF we reach here; Score AND unconditionally:
      float score = 0;
      for (int i = 0; i < split; i++) {
        LeafEvalNode child = leafs[i];
        int count = child.count(doc);
        int length = child.length(doc);
        score += weights[i] * Math.log((count + backgrounds[i]) / (mu + length));
      }
      if(split > 0 && score + bestAfter[split-1] < heapMinimumScore) {
        return NegativeInfinity;
      }
      // Score rest conditionally only:
      for (int i = split; i<N; i++) {
        LeafEvalNode child = leafs[i];
        int count = child.count(doc);
        int length = child.length(doc);
        score += weights[i] * Math.log((count + backgrounds[i]) / (mu + length));
        if(score + bestAfter[i] < heapMinimumScore) {
          return NegativeInfinity;
        }
      }
      return score;
    }
    return NegativeInfinity;
  }


  @Override
  public int count(int doc) throws IOException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    heapMinimumScore = NegativeInfinity;
    List<Explanation> children = new ArrayList<>(N);
    List<String> descs = new ArrayList<>(N);
    float myscore = 0;
    for (int i = 0; i < N; i++) {
      OptChild optChild = cnodes.get(i);
      IreneCountStatistics stats = optChild.stats;
      int where = optChild.node.currentDocument();
      int length = optChild.node.length(doc);
      int count = optChild.node.count(doc);
      float weight = optChild.weight;
      Parameters descP = Parameters.parseArray("_doc", where, "weight", weight, "count", count, "length", length, "mu", mu, "clen", stats.collectionLength, "cf", stats.collectionFrequency, "background", optChild.background);
      double score = Math.log(count + optChild.background) - Math.log(mu + length);
      descP.put("wscore", weight * score);
      descP.put("score", score);
      myscore += weight * score;

      String desc = descP.toString()+"\n";
      descs.add(desc);
      children.add(optChild.node.explain(doc));
    }
    return Explanation.match(myscore, myscore+"\t"+descs.toString(), children);
  }

  private float heapMinimumScore = -Float.MAX_VALUE;

  public static double min(float[] xs, int start, int end) {
    double min = xs[start];
    for (int i = start; i < end; i++) {
      if(xs[i] < min) {
        min = xs[i];
      }
    }
    return min;
  }

  @Override
  public void setMinimumRequiredScore(float theta) {
    heapMinimumScore = theta;

    this.split = 0;

    // Okay: Removing Order from WAND:
    // The unobserved set must be less than theta
    //   -> some of the observed must occur
    // AND the sum of all but the weakest in the observed set cannot be more than theta
    //   -> all of them must be required to get as close as possible to theta
    // AND the weakest of the observed set must be greater than what's remaining
    //   -> the sum of the unobserved can't replace any item in the observed
    //   -> all of the observed must occur

    for (int i = 0; i < N; i++) {
      double remaining = bestAfter[i];
      //System.out.printf("WAND[%d].Remaining: %1.3f\n", i, remaining);
      if(remaining < theta) {
        double weakest = min(bestScore, 0, i+1);
        double observed = bestUntil[i];
        boolean necessary = ((observed - weakest + bestAfter[i]) < theta);
        if ((remaining < weakest) && necessary) {
          split = i + 1;
        }
      }
    }
  }

  @Override
  public boolean matches(int id) throws IOException {
    if(current < id) {
      current = advance(id);
    }
    return current == id;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createOrMover(children);
  }

  @Override
  public long estimateDF() {
    return numHits;
  }

  /** as an iterator! */
  int current = 0;

  @Override
  public int docID() {
    return current;
  }

  @Override
  public int nextDoc() throws IOException {
    return advance(current+1);
  }

  @Override
  public int advance(int target) throws IOException {
    if(split > 0) {
      // AND movement:
      current = target;

      while(true) {
        boolean match = true;
        for (int i = 0; i < split; i++) {
          int where = leafs[i].currentDocument();
          if(where < current) {
            where = leafs[i].advance(current);
            if(where == NO_MORE_DOCS) return NO_MORE_DOCS;
          }
          if(where > current) {
            current = where;
            match = false;
            break; // to continue while
          }
        }

        if(match) {
          return current;
        }
      }
    } else {
      // OR movement:
      int nextMin = NO_MORE_DOCS;
      for (int i = 0; i < N; i++) {
        LeafEvalNode child = leafs[i];
        int where = child.currentDocument();
        if (where < target) {
          where = child.advance(target);
        }
        nextMin = Math.min(where, nextMin);
        if (nextMin == target) break;
      }

      current = nextMin;
      return nextMin;
    }
  }

  @Override
  public long cost() {
    return numHits;
  }
}
