package edu.umass.cs.jfoley.irene.expr.types;

/**
 * @author jfoley
 */
public enum OperatorType {
  BOOLS,
  COUNTS,
  POSITIONS,
  SCORES
  ;

  public boolean hasPositions() {
    return this == POSITIONS;
  }
  public boolean hasCounts() {
    return this == POSITIONS || this == COUNTS;
  }
  public boolean hasBooleans() {
    return this == BOOLS;
  }
  public boolean hasScores() {
    return this == SCORES;
  }
}
