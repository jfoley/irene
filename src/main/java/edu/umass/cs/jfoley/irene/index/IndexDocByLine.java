package edu.umass.cs.jfoley.irene.index;

import ciir.jfoley.chai.io.LinesIterable;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;

/**
 * @author jfoley
 */
public class IndexDocByLine {
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);

    long total = argp.get("total", 0);

    try (LinesIterable sdocs = LinesIterable.fromFile(argp.get("input", "/mnt/scratch/jfoley/searchie/doc_per_line"));
         IreneIndexer writer = IndexingParams.start().withPath(argp.get("output", "/mnt/scratch/jfoley/searchie/conll-full.irene")).create().build()) {
      for (String text : sdocs) {
        String id = Integer.toString(sdocs.getLineNumber());

        Document doc = new Document();
        doc.add(new StringField("id", id, Field.Store.YES));
        doc.add(new TextField("body", text, Field.Store.YES));
        writer.pushDocument(doc);
        writer.printMessageIfReady(System.err, total);
      }
    }
  }
}

