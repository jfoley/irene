package edu.umass.cs.jfoley.irene.index;

import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.string.StrUtil;
import ciir.jfoley.chai.time.Debouncer;
import edu.umass.cs.jfoley.irene.IreneEnglishAnalyzer;
import edu.umass.cs.jfoley.irene.impl.IreneSimilarity;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.jsoup.Jsoup;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.Level;

import static org.lemurproject.galago.core.tools.apps.ThreadedBatchSearch.logger;

/**
 * @author jfoley
 */
public class IndexJSONL {
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);

    long total = argp.get("total", 3153724L);
    long count = 0;
    Debouncer msg = null;

    try (final FSDirectory dir = FSDirectory.open(Paths.get(argp.get("output", "/mnt/scratch3/jfoley/clue-controversy.irene")))) {
      final IndexWriterConfig cfg = new IndexWriterConfig(new IreneEnglishAnalyzer());
      try (LinesIterable sdocs = LinesIterable.fromFile(argp.get("input", "/home/jfoley/code/controversyNews/scripts/clueweb/clue09b.seed.jsonl.gz"))) {
        cfg.setSimilarity(new IreneSimilarity());
        System.out.println("Similarity: "+cfg.getSimilarity());
        cfg.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

        try (IndexWriter writer = new IndexWriter(dir, cfg)) {
          msg = new Debouncer();

          for (String sdoc : sdocs) {
            try {
              Parameters jsdoc = Parameters.parseString(sdoc);
              count++;

              String raw_html = jsdoc.getString("content");
              org.jsoup.nodes.Document jsoup = Jsoup.parse(raw_html);

              String text = StrUtil.collapseSpecialMarks(jsoup.text());
              String title = jsoup.select("title").text();

              Document doc = new Document();
              doc.add(new StringField("id", jsdoc.getString("id"), Field.Store.YES));
              doc.add(new TextField("title", title, Field.Store.YES));
              doc.add(new TextField("body", text, Field.Store.YES));

              writer.addDocument(doc);

              if (msg.ready()) {
                System.err.println("L.Indexing: " + title + " " + count + " so far... " + msg.estimate(count, total));
              }
            } catch (Exception e) {
              logger.log(Level.WARNING, "Document-Exception", e);
            }
          }
        }

      }
    }

    System.err.println("L.Indexing.Done " + msg.estimate(count, count));
  }
}
