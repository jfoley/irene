package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author jfoley
 */
@QueryOperator("require")
public class RequireEval implements QueryEvalNode {
  private final QueryEvalNode condition;
  private final QueryEvalNode scorer;

  @Override
  public void setMinimumRequiredScore(float score) {
    scorer.setMinimumRequiredScore(score);
  }

  public RequireEval(QueryEvalNode condition, QueryEvalNode scorer) {
    this.condition = condition;
    this.scorer = scorer;
  }

  @Override
  public float score(int doc) throws IOException {
    return scorer.score(doc);
  }

  @Override
  public int count(int doc) throws IOException {
    return scorer.count(doc);
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    if(!condition.matches(doc)) {
      return Explanation.noMatch("ScoreWhen", condition.explain(doc));
    } else {
      float score = scorer.score(doc);
      return Explanation.match(score, "ScoreWhen", scorer.explain(doc));
    }
  }

  private int __matching = -1;

  @Override
  public boolean matches(int id) throws IOException {
    if(__matching == id) return true;
    boolean matched = condition.matches(id) && scorer.matches(id);
    if(matched) {
      __matching = id;
      return true;
    }
    return false;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createAndMover(Arrays.asList(condition, scorer));
  }

  @Override
  public long estimateDF() {
    return Math.min(condition.estimateDF(), scorer.estimateDF());
  }
}
