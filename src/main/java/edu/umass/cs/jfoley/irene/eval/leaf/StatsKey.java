package edu.umass.cs.jfoley.irene.eval.leaf;

import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.index.Term;

import java.util.List;

/**
 * @author jfoley
 */
public interface StatsKey {
  int hashCode();
  boolean equals(Object other);
  QExpr getAsQExpr();

  class OrderedWindowStatsKey implements StatsKey {
    final List<Term> terms;
    final int width;
    final int _hashCode;

    public OrderedWindowStatsKey(List<Term> terms, int width) {
      this.terms = terms;
      this.width = width;
      this._hashCode = terms.hashCode() ^ width;
    }

    @Override
    public int hashCode() {
      return _hashCode;
    }

    @Override
    public boolean equals(Object other) {
      if(other instanceof OrderedWindowStatsKey) {
        OrderedWindowStatsKey rhs = (OrderedWindowStatsKey) other;
        return this.width == rhs.width && this.terms.equals(rhs.terms);
      }
      return false;
    }

    @Override
    public QExpr getAsQExpr() {
      QExpr phrase = QExpr.create("od");
      for (Term term : terms) {
        QExpr q = new QExpr("text");
        q.config.set(QExpr.DEFAULT_KEY, term.text());
        q.config.set("field", term.field());
        phrase.addChild(q);
      }
      return phrase;
    }

    @Override
    public String toString() {
      return getAsQExpr().toString();
    }
  }

  class UnorderedWindowStatsKey implements StatsKey {
    final List<Term> terms;
    final int width;
    final int _hashCode;

    public UnorderedWindowStatsKey(List<Term> terms, int width) {
      this.terms = terms;
      this.width = width;
      this._hashCode = terms.hashCode() ^ width;
    }


    @Override
    public int hashCode() {
      return _hashCode;
    }

    @Override
    public boolean equals(Object other) {
      if(other instanceof UnorderedWindowStatsKey) {
        UnorderedWindowStatsKey rhs = (UnorderedWindowStatsKey) other;
        return this.width == rhs.width && this.terms.equals(rhs.terms);
      }
      return false;
    }

    @Override
    public QExpr getAsQExpr() {
      QExpr phrase = QExpr.create("uw");
      for (Term term : terms) {
        QExpr q = new QExpr("text");
        q.config.set(QExpr.DEFAULT_KEY, term.text());
        q.config.set("field", term.field());
        phrase.addChild(q);
      }
      return phrase;
    }

    @Override
    public String toString() {
      return getAsQExpr().toString();
    }
  }

  class SynonymStatsKey implements StatsKey {
    private final List<Term> terms;
    private final int _hashCode;

    public SynonymStatsKey(List<Term> terms) {
      this.terms = terms;
      this._hashCode = terms.hashCode();
    }

    @Override
    public int hashCode() {
      return _hashCode;
    }

    @Override
    public boolean equals(Object other) {
      if(other instanceof SynonymStatsKey) {
        SynonymStatsKey rhs = (SynonymStatsKey) other;
        return this.terms.equals(rhs.terms);
      }
      return false;
    }
    @Override
    public QExpr getAsQExpr() {
      QExpr compute = new QExpr("countsum");
      for (Term term : terms) {
        compute.addChild(QExpr.fieldMatch(term.field(), term.text()));
      }
      return compute;
    }
    @Override
    public String toString() {
      return getAsQExpr().toString();
    }
  }

}
