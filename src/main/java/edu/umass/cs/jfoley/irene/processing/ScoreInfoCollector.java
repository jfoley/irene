package edu.umass.cs.jfoley.irene.processing;

import ciir.jfoley.chai.math.StreamingStats;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.CollectorManager;
import org.apache.lucene.search.LeafCollector;
import org.apache.lucene.search.Scorer;

import java.io.IOException;
import java.util.Collection;

/**
 * @author jfoley.
 */
public class ScoreInfoCollector implements CollectorManager<ScoreInfoCollector.LeafScoreInfoCollector, StreamingStats> {

  @Override
  public LeafScoreInfoCollector newCollector() throws IOException {
    return new LeafScoreInfoCollector();
  }

  @Override
  public StreamingStats reduce(Collection<LeafScoreInfoCollector> collectors) throws IOException {
    StreamingStats stats = new StreamingStats();
    for (LeafScoreInfoCollector collector : collectors) {
      stats.add(collector.scoreInfos);
    }
    return stats;
  }

  public static class LeafScoreInfoCollector implements Collector {
    StreamingStats scoreInfos = new StreamingStats();

    @Override
    public LeafCollector getLeafCollector(LeafReaderContext context) throws IOException {
      return new LeafCollector() {
        public Scorer scorer;

        @Override
        public void setScorer(Scorer scorer) throws IOException {
          this.scorer = scorer;
        }

        @Override
        public void collect(int doc) throws IOException {
          float score = scorer.score();
          scoreInfos.push(score);
        }
      };
    }

    @Override
    public boolean needsScores() {
      return false;
    }
  }
}
