package edu.umass.cs.jfoley.irene.repr;

import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;

import java.util.List;

/**
 * @author jfoley.
 */
public class TermVector {
  public TObjectDoubleHashMap<String> weights;
  public double totalWeight;

  public static <T> double sum(TObjectDoubleHashMap<T> weights) {
    double[] ys = weights.values();
    double sum = 0;
    for (double y : ys) {
      sum += y;
    }
    return sum;
  }

  public TermVector(TObjectDoubleHashMap<String> weights) {
    this(weights, sum(weights));
  }

  public TermVector copy() {
    return new TermVector(new TObjectDoubleHashMap<>(weights), totalWeight);
  }

  public void normalize() {
    weights.transformValues((wt) -> wt / totalWeight);
    totalWeight = 1;
  }

  public TermVector(TObjectDoubleHashMap<String> weights, double totalWeight) {
    this.weights = weights;
    this.totalWeight = totalWeight;
  }

  public static TermVector languageModel(List<String> terms) {
    int len = terms.size();
    TObjectDoubleHashMap<String> wts = new TObjectDoubleHashMap<>(len/2);

    // count up terms:
    for (String term : terms) {
      wts.adjustOrPutValue(term, 1, 1);
    }

    // divide by length:
    wts.transformValues((count) -> count / len);
    return new TermVector(wts, len);
  }

  public static TermVector bm25(IreneIndex index, List<String> terms, double b, double k) {
    int len = terms.size();
    TObjectIntHashMap<String> counts = new TObjectIntHashMap<>(len/2);
    for (String term : terms) {
      counts.adjustOrPutValue(term, 1, 1);
    }
    return bm25(index, counts, len, b, k);
  }

  public static TermVector bm25(IreneIndex index, TObjectIntHashMap<String> counts, double length, double b, double k) {
    TObjectDoubleHashMap<String> scores = new TObjectDoubleHashMap<>(counts.size());
    counts.forEachEntry((term, count) -> {
      IreneCountStatistics stats = index.getTermStatistics(term);
      long documentCount = stats.totalDocumentCount;
      long df = stats.documentFrequency;
      double idf = Math.log(documentCount / (df + 0.5));
      double numerator = count * (k+1);
      double denominator = count + (k* (1- b + (b*length / stats.averageDocumentLength())));

      scores.put(term, idf * (numerator / denominator));
      return true;
    });

    return new TermVector(scores);
  }

  public double get(String term) {
    return weights.get(term);
  }

  @Override
  public String toString() {
    return this.weights.toString();
  }

  public double getTotalWeight() {
    return totalWeight;
  }

  public TObjectDoubleHashMap<String> unwrap() {
    return weights;
  }
}
