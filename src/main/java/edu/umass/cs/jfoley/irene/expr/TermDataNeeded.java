package edu.umass.cs.jfoley.irene.expr;

import edu.umass.cs.jfoley.irene.impl.DataNeeded;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author jfoley
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface TermDataNeeded {
  DataNeeded value();
}
