package edu.umass.cs.jfoley.irene.index;

import ciir.jfoley.chai.time.Debouncer;
import edu.umass.cs.jfoley.irene.IreneEnglishAnalyzer;
import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.impl.IreneSimilarity;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nonnull;
import java.io.Closeable;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author jfoley
 */
public class IreneIndexer implements Closeable {
  public final Directory directory;
  private final IndexWriterConfig cfg;
  public final IndexWriter writer;
  private final Debouncer msg = new Debouncer();
  private final AtomicLong processed = new AtomicLong(0);

  public IreneIndexer(String path) throws IOException {
    this(path, false);
  }
  public IreneIndexer(String path, boolean append) throws IOException {
    directory = FSDirectory.open(Paths.get(path));
    cfg = new IndexWriterConfig(new IreneEnglishAnalyzer());
    cfg.setSimilarity(new IreneSimilarity());
    cfg.setOpenMode(append ? IndexWriterConfig.OpenMode.CREATE_OR_APPEND : IndexWriterConfig.OpenMode.CREATE);
    writer = new IndexWriter(directory, cfg);
  }
  public IreneIndexer(String path, boolean append, Analyzer analyzer) throws IOException {
    this(FSDirectory.open(Paths.get(path)), append, analyzer);
  }
  public IreneIndexer(Directory dir, boolean append, Analyzer analyzer) throws IOException {
    directory = dir;
    cfg = new IndexWriterConfig(analyzer);
    cfg.setSimilarity(new IreneSimilarity());
    cfg.setOpenMode(append ? IndexWriterConfig.OpenMode.CREATE_OR_APPEND : IndexWriterConfig.OpenMode.CREATE);
    writer = new IndexWriter(directory, cfg);
  }

  public IreneIndexer(IndexingParams params) throws IOException {
    directory = params.getIOSystem();
    cfg = new IndexWriterConfig(params.getAnalyzer());
    cfg.setSimilarity(params.getSimilarity());
    cfg.setOpenMode(params.getOpenMode());
    writer = new IndexWriter(directory, cfg);
  }

  public void write(IreneIndex other) throws IOException {
    writer.addIndexes(other.dir);
    processed.addAndGet(other.getTotalDocuments());
  }

  public Debouncer getMessage() { return msg; }
  public long numProcessed() { return processed.get(); }

  public boolean printMessageIfReady(PrintStream where, long total) {
    if(msg.ready()) {
      where.println("# Indexer: "+msg.estimateStr(numProcessed(), total));
      return true;
    }
    return false;
  }

  /**
   * {@link IndexWriter#commit}
   * @throws IOException if lucene encounters a disk error
   */
  public void commit() throws IOException {
    writer.commit();
  }

  public void pushDocument(IndexableField... fields) {
    try {
      processed.incrementAndGet();
      writer.addDocument(Arrays.asList(fields));

    } catch (IOException e) {
      throw new IndexingException(e);
    }
  }
  public void pushDocument(@Nonnull Iterable<IndexableField> doc) {
    try {
      processed.incrementAndGet();
      writer.addDocument(doc);
    } catch (IOException e) {
      throw new IndexingException(e);
    }
  }

  public IreneIndex open() throws IOException {
    return open(Parameters.create());
  }
  public IreneIndex open(Parameters indexP) throws IOException {
    IreneIndex index = new IreneIndex(directory, indexP);
    index.writer = this;
    return index;
  }

  @Override
  public void close() throws IOException {
    writer.close();
    long count = processed.get();
    System.err.println("# Indexing Complete to : "+directory.toString()+": "+msg.estimate(count, count));
    directory.close();
  }
}
