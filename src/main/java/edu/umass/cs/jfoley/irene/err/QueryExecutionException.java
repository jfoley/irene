package edu.umass.cs.jfoley.irene.err;

/**
 * @author jfoley
 */
public class QueryExecutionException extends RuntimeException {
  public QueryExecutionException(Throwable e) {
    super(e);
  }
}
