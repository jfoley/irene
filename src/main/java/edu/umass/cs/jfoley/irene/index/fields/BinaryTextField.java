package edu.umass.cs.jfoley.irene.index.fields;

import ciir.jfoley.chai.string.StrUtil;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Describes a field for, e.g. tags or many boolean features, where the frequency of occurrence is totally uninteresting.
 * @author jfoley
 */
public class BinaryTextField extends Field {
  public static final FieldType BINARY_FIELD_TYPE = new FieldType();
  static {
    BINARY_FIELD_TYPE.setStored(true);
    BINARY_FIELD_TYPE.setTokenized(true);
    BINARY_FIELD_TYPE.setIndexOptions(IndexOptions.DOCS);
    BINARY_FIELD_TYPE.freeze();
  }

  public BinaryTextField(String field, String text) {
    super(field, text, BINARY_FIELD_TYPE);
  }
  public BinaryTextField(String field, Collection<String> flags) {
    //TODO: implement a Collection<String> reader:
    super(field, StrUtil.join(new ArrayList<>(flags)), BINARY_FIELD_TYPE);
  }

}
