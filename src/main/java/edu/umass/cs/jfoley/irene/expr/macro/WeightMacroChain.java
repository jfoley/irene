package edu.umass.cs.jfoley.irene.expr.macro;

import edu.umass.cs.jfoley.irene.expr.QExpr;
import gnu.trove.list.array.TFloatArrayList;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

/**
 * @author jfoley
 */
public class WeightMacroChain {
  private final List<Macro> macros;
  private final Set<String> applicableNodes;
  private final Parameters cfg;

  // Flyweight marker to see if this node has been touched already:
  public static class Completed { private Completed() { } }
  public static final Completed MARKER = new Completed();

  @Nonnull
  public static MacroChain init(Parameters cfg) {
    Set<String> applicableNodes = new HashSet<>();
    // TODO read from cfg...
    applicableNodes.add("combine");
    applicableNodes.add("wsum");
    applicableNodes.add("dql");
    applicableNodes.add("optbm25");

    WeightMacroChain wmc = new WeightMacroChain(cfg, applicableNodes);
    return new MacroChain(wmc.cfg, wmc.macros);
  }

  private WeightMacroChain(Parameters cfg, Set<String> applicableNodes) {
    this.cfg = cfg;
    this.macros = Arrays.asList(
        new NormCombineOperations(cfg),
        new FlattenCombineNodes(cfg)
    );
    this.applicableNodes = applicableNodes;
  }

  /**
   * Annotation for debugging the FlattenCombineNodes step.
   */
  public static class FlattenedFrom {
    final QExpr previousExpr;

    public FlattenedFrom(QExpr previousExpr) {
      this.previousExpr = previousExpr;
    }
  }

  /**
   * This macro removes extraneous combines.
   * #combine( #combine( a b ) #combine( c d f ) #combine( e ) )  ->
   * #combine( a b c d f e )
   *
   * Note that because the default norm=true on combine, the weighting is implied by the combine structure: e is equally as important as (ab) and (cdf), and the flatten will take that into account because of previous weight-rescaling.
   *
   * Weights on each of the components are rescaled previously in {@link NormCombineOperations}.
   *
   * *Comment mostly stolen from Galago's FlattenCombineTraversal, but implementation was clean-room.
   * *Made comment more clear as to true behavior.
   */
  public class FlattenCombineNodes extends Macro {
    public FlattenCombineNodes(Parameters cfg) {
      super(cfg);
    }

    @Nonnull
    @Override
    public QExpr transform(QExpr input) {
      if(input.operator.equals("combine")) {
        List<QExpr> newChildren = new ArrayList<>(input.children.size());
        TFloatArrayList newWeights = new TFloatArrayList(input.children.size());

        //System.out.println(input.toJSON().toPrettyString());
        float[] weights = input.getTransformedWeightVector();
        List<QExpr> children = input.children;

        // for all children, transform them, and flatten if possible:
        for (int i = 0; i < children.size(); i++) {
          float parentWeight = weights[i];

          QExpr child = children.get(i);
          QExpr xchild = transform(child);
          if (xchild.operator.equals("combine")) {
            // can flatten this child:
            float[] childWeights = xchild.getTransformedWeightVector();
            for (int j = 0; j < xchild.children.size(); j++) {
              newWeights.add(childWeights[j] * parentWeight);
              newChildren.add(xchild.children.get(j).copy());
            }
          } else { // not nested, keep existing weights:
            newWeights.add(parentWeight);
            newChildren.add(xchild);
          }
        }

        // construct a wider combine (with flattened children) and set the new weights on top:
        QExpr output = input.copy(newChildren);
        assert(newWeights.size() == newChildren.size());
        for (int i = 0; i < newWeights.size(); i++) {
          output.config.set(Integer.toString(i), newWeights.get(i));
        }
        // keep track, for debugging purposes, of the original query
        output.setUserData(FlattenedFrom.class, new FlattenedFrom(input.copy()));
        return output;
      }
      return input;
    }
  }

  /**
   * @author jfoley
   */
  public class NormCombineOperations extends Macro {

    public NormCombineOperations(Parameters cfg) {
      super(cfg);
    }

    @Nullable
    @Override
    public QExpr transform(QExpr input) {
      QExpr copy = input.copy(this.transform(input.children));
      Parameters cfg = copy.config;

      // normalize weights if we haven't yet:
      if(applicableNodes.contains(copy.operator) && !copy.hasUserData(Completed.class)) {
        int N = copy.children.size();
        boolean normalize = cfg.get("norm", true);

        double[] weights = new double[N];
        double sum = 0f;
        for (int i = 0; i < N; i++) {
          QExpr child = copy.children.get(i);
          String key = Integer.toString(i);

          boolean parentWeighted = cfg.containsKey(key);
          boolean childWeighted = child.config.containsKey("weight");

          if(parentWeighted && childWeighted) {
            throw new IllegalArgumentException("Can't specify weights in parent and in child! QExpr="+this);
          }

          double weight = 1.0;

          if(parentWeighted) {
            weight = cfg.getDouble(key);
          } else if(childWeighted) {
            weight = child.config.getDouble("weight");
            // delete weight key, in child and store it in the parent now that we're using it here.
            cfg.set(key, weight);
            child.config.remove("weight");
          }

          // look for numerical property first, and weight= on child second.
          weights[i] = (float) weight;
          sum += weight;
        }

        if(normalize) {
          for (int i = 0; i < N; i++) {
            weights[i] /= sum;
          }
        }

        // overwrite weights with normalized, if necessary.
        for (int i = 0; i < weights.length; i++) {
          cfg.set(Integer.toString(i), weights[i]);
        }

        // done norming (maybe don't need this in the future...)
        cfg.set("norm", false);
        copy.setUserData(Completed.class, MARKER);
      }

      return copy;
    }

  }


  public static class CombineDirichletToDQL extends Macro {
    public CombineDirichletToDQL(Parameters cfg) {
      super(cfg);
    }

    @Nullable
    @Override
    public QExpr transform(QExpr input) {
      List<QExpr> newChildren = transform(input.children);
      QExpr copy = input.copy(newChildren);

      if("combine".equals(copy.operator)) {
        // and all children are dirichlet:
        boolean allDirichlet = true;
        List<QExpr> counts = new ArrayList<>(copy.children.size());
        for (QExpr child : copy.children) {
          if(child.operator.equals("dirichlet")) {
            QExpr grandchild = child.children.get(0);
            assert(grandchild.operator.equals("text"));
            counts.add(grandchild.copy());
          } else {
            allDirichlet = false;
            break;
          }
        }
        if(allDirichlet) {
          // cut the middlemen:
          copy = input.copy(counts);
          copy.operator = "dql";
          assert(counts.get(0).operator.equals("text"));
        }
      }

      return copy;
    }
  }
}
