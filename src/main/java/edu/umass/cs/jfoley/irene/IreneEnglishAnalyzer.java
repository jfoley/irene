package edu.umass.cs.jfoley.irene;

import ciir.jfoley.chai.lang.MString;
import edu.umass.cs.jfoley.irene.util.KStem;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.KeywordAttribute;

import java.io.IOException;
import java.util.Objects;

/**
 * @author jfoley
 */
public class IreneEnglishAnalyzer extends Analyzer {
  public static final boolean fastKStem = true;

  public static class KrovetzStemFilter extends TokenFilter {
    private final KStem stemmer = new KStem();
    private final org.lemurproject.galago.krovetz.KStem sKStem = new org.lemurproject.galago.krovetz.KStem();
    private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
    private final KeywordAttribute keywordAttr = addAttribute(KeywordAttribute.class);
    protected KrovetzStemFilter(TokenStream input) {
      super(input);
    }

    private final MString stringBufferView = new MString();

    @Override
    public final boolean incrementToken() throws IOException {
      if (!input.incrementToken()) return false;

      if(fastKStem) {
        stringBufferView.setBuffer(termAtt.buffer(), termAtt.length());

        // calculate stemming:
        boolean changed = stemmer.stemTerm(stringBufferView);

        if ((!keywordAttr.isKeyword()) && changed) {
          termAtt.setLength(0);
          termAtt.append(stringBufferView);
        }
        return true;
      } else {
        String tmpStr = new String(termAtt.buffer(), 0, termAtt.length());
        String stem = sKStem.stemTerm(tmpStr);
        if ((!keywordAttr.isKeyword()) && Objects.equals(stem, tmpStr)) {
          termAtt.setLength(0);
          termAtt.append(stem);
        }
        return true;
      }
    }
  }

  @Override
  protected TokenStreamComponents createComponents(String fieldName) {
    final Tokenizer source = new StandardTokenizer();
    TokenStream result = new StandardFilter(source);
    if(fastKStem) {
      result = new LowerCaseFilter(result);
    }
    // TODO, before next release, switch to Lucene's KStemFilter!
    //result = new KStemFilter(result);
    result = new KrovetzStemFilter(result);
    return new TokenStreamComponents(source, result);
  }
}
