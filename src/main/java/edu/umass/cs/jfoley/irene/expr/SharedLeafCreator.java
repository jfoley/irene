package edu.umass.cs.jfoley.irene.expr;

import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneTermInfo;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Scorer;

import java.io.IOException;
import java.util.*;

/**
 * @author jfoley.
 */
public class SharedLeafCreator implements LeafCreator {
  public final Map<Term, LeafEvalNode> leafMap;
  private final QueryContext qctx;
  public final List<LuceneScorerRequest> scorers;
  IdentityHashMap<LuceneScorerRequest, Scorer> generatedScorers;

  public SharedLeafCreator(QueryContext context) {
    this.qctx = context;
    leafMap = new HashMap<>();
    scorers = new ArrayList<>();
    generatedScorers = new IdentityHashMap<>();
  }

  @Override
  public void withTermRequests(List<IreneQueryLanguage.TermRequest> termsRequested, IndexSearcher searcher, LeafReaderContext context) {

    HashMap<Term, DataNeeded> sharedRequests = new HashMap<>();
    for (IreneQueryLanguage.TermRequest termRequest : termsRequested) {
      // use the bigger "usage" DOCS, COUNTS, POS, etc.
      sharedRequests.compute(termRequest.term,
          (oldTerm, oldUsage) -> DataNeeded.max(termRequest.forWhat, oldUsage));
    }

    List<IreneTermInfo> requested = new ArrayList<>(sharedRequests.size());
    for (Map.Entry<Term, DataNeeded> kv : sharedRequests.entrySet()) {
      Term term = kv.getKey();
      DataNeeded needed = kv.getValue();
      requested.add(qctx.getTermInfo(term, needed));
    }

    for (IreneTermInfo ireneTermInfo : requested) {
      LeafEvalNode leafEvalNode = this.qctx.create(ireneTermInfo);
      leafMap.put(ireneTermInfo.getTerm(), leafEvalNode);
    }

  }

  @Override
  public LeafEvalNode requestTerm(IreneQueryLanguage.TermRequest input) {
    LeafEvalNode node = leafMap.get(input.term);
    return Objects.requireNonNull(node,
        "Couldn't find LeafEvaluator for term=" + input.term + " in haystack: " + leafMap);
  }

  @Override
  public Scorer getScorer(LuceneScorerRequest queryInfo) {
    try {
      Scorer scorer = generatedScorers.get(queryInfo);
      if(scorer == null) {
        scorer = queryInfo.getWeight().scorer(qctx.context);
        generatedScorers.put(queryInfo, scorer);
      }
      return scorer;
    } catch (IOException e) {
      throw new RuntimeException("Couldn't create a scorer from a weight for LSR: "+queryInfo, e);
    }
  }

  @Override
  public void withScorerRequests(List<LuceneScorerRequest> requests) {
    this.scorers.addAll(requests);
    for (LuceneScorerRequest scorer : this.scorers) {
      getScorer(scorer);
    }
  }

  @Override
  public List<DocIdSetIterator> getAllIterators() {
    List<DocIdSetIterator> movers = new ArrayList<>();
    for (LeafEvalNode leafEvalNode : leafMap.values()) {
      movers.add(leafEvalNode.getMovementInfo().mover);
    }
    for (Scorer scorer : generatedScorers.values()) {
      movers.add(scorer);
    }
    return movers;
  }
}
