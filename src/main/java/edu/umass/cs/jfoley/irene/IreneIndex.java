package edu.umass.cs.jfoley.irene;

import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.collections.list.IntList;
import ciir.jfoley.chai.math.StreamingStats;
import ciir.jfoley.chai.random.ReservoirSampler;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import edu.umass.cs.jfoley.irene.err.QueryExecutionException;
import edu.umass.cs.jfoley.irene.eval.leaf.StatsKey;
import edu.umass.cs.jfoley.irene.expr.IreneQueryLanguage;
import edu.umass.cs.jfoley.irene.expr.IreneQueryModel;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import edu.umass.cs.jfoley.irene.index.IreneIndexer;
import edu.umass.cs.jfoley.irene.processing.AllMatchesCollectorManager;
import edu.umass.cs.jfoley.irene.processing.ScoreInfoCollector;
import edu.umass.cs.jfoley.irene.processing.SmartSearchCollectorManager;
import edu.umass.cs.jfoley.irene.processing.StatisticsCollector;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.BytesRef;
import org.lemurproject.galago.utility.Parameters;

import javax.annotation.Nullable;
import java.io.Closeable;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.*;
import java.util.stream.IntStream;

/**
 * @author jfoley
 */
public class IreneIndex implements Closeable {
  public final Directory dir;
  public IndexReader reader;
  /** Note that a writer may not necessarily be present. */
  @Nullable
  public IreneIndexer writer;
  public IndexSearcher searcher;
  public Analyzer analyzer = new PerFieldAnalyzerWrapper(new IreneEnglishAnalyzer());
  private IreneQueryLanguage featureFactory;

  // TODO, configure this somehow.
  /**
   * Store count statistics so that we're not constantly recomputing phrases when people ask for similar queries over and over.
   */
  public final Cache<StatsKey, ForkJoinTask<IreneCountStatistics>> countStatisticsCache = Caffeine.newBuilder().maximumSize(100_000).build();

  public IreneIndex(String path) throws IOException {
    this(path, Parameters.create());
  }
  public IreneIndex(String path, Parameters argp) throws IOException {
    this(FSDirectory.open(Paths.get(path)), argp);
  }
  // create an in-memory index
  public IreneIndex(Directory dir, Parameters argp) throws IOException {
    this.dir = dir;
    reader = DirectoryReader.open(dir);
    searcher = new IndexSearcher(reader, ForkJoinPool.commonPool());
    featureFactory = new IreneQueryLanguage(this, argp);

    // at least for default field, compute vocab size now:
    //long getVocabSize = getVocabularySize(featureFactory.defaultField);
    //System.out.println("|V| "+featureFactory.defaultField+"\t"+getVocabSize);
  }

  public static IreneIndex memory() throws IOException {
    return new IreneIndex(new RAMDirectory(), Parameters.create());
  }

  /**
   * Reopen an index from disk.
   * @throws IOException
   */
  public void refresh(boolean perfectStats) throws IOException {
    this.searcher = null;
    this.reader.close();

    // If we don't need perfect stats, we can skip invalidating the caches...
    if(perfectStats) {
      this.countStatisticsCache.invalidateAll();
    }
    this.reader = DirectoryReader.open(dir);
    this.searcher = new IndexSearcher(reader, ForkJoinPool.commonPool());
  }

  @Override
  public void close() throws IOException {
    reader.close();
    dir.close();
  }

  public String getField(int doc, String fieldName) {
    try {
      return searcher.doc(doc, Collections.singleton(fieldName)).get(fieldName);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public IndexableField getRawField(int doc, String fieldName) {
    try {
      return searcher.doc(doc, Collections.singleton(fieldName)).getField(fieldName);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Nullable
  public Document doc(int num) {
    try {
      return searcher.doc(num);
    } catch (IllegalArgumentException iae) {
      return null;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public int documentByName(String name) {
    return documentByName(name, "id");
  }
  public int documentByName(String name, String field) {
    BooleanQuery q = new BooleanQuery.Builder().add(new TermQuery(new Term(field, name)), BooleanClause.Occur.MUST).build();
    try {
      ScoreDoc[] docs = searcher.search(q, 100).scoreDocs;
      if(docs.length == 0) {
        return -1;
      }
      return docs[0].doc;
    } catch (IOException e) {
      // TODO log
      return -1;
    }
  }

  public List<String> pullTerms(int docId) throws IOException {
    return AnalyzerUtil.tokenize(this.analyzer, getField(docId, "body"));
  }

  public void printTrecResults(PrintStream out, String qid, String systemName, TopDocs docs) throws IOException {
    ScoreDoc[] scoreDocs = docs.scoreDocs;
    for (int i = 0; i < scoreDocs.length; i++) {
      ScoreDoc scoreDoc = scoreDocs[i];
      int docId = scoreDoc.doc;
      String name = getField(docId, "id");
      out.printf("%s Q0 %s %d %10.8f %s\n", qid, name, i+1, scoreDoc.score, systemName);
    }
  }

  public TopDocs search(QExpr expr, int requested) {
    return search(prepare(featureFactory.applyMacros(expr)), requested);
  }

  public ReservoirSampler<Pair<Integer, Float>> sample(QExpr expr, Random rand, int requested) {
    IreneQueryModel qf = this.prepare(expr);
    ReservoirSampler<Pair<Integer, Float>> sampledResults = new ReservoirSampler<>(rand, requested);
    if(requested <= 0) {
      return sampledResults;
    }
    try {
      searcher.search(qf, new Collector() {
        @Override
        public LeafCollector getLeafCollector(LeafReaderContext context) throws IOException {
          final int docBase = context.docBase;
          return new LeafCollector() {
            Scorer scorer;
            @Override
            public void setScorer(Scorer scorer) throws IOException {
              this.scorer = scorer;
            }

            @Override
            public void collect(int doc) throws IOException {
              float score = scorer.score();
              synchronized (sampledResults) {
                sampledResults.lazyAdd(() -> Pair.of(docBase+doc, score));
                //sampledResults.add(Pair.of(docBase+doc, score));
              }
            }
          };
        }

        @Override
        public boolean needsScores() {
          return true;
        }
      });
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return sampledResults;
  }

  public TopDocs search(IreneQueryModel qmodel, int requested) {
    qmodel.setIndex(this);
    try {
      return searcher.search(qmodel, requested);
    } catch (IOException e) {
      throw new QueryExecutionException(e);
    }
  }
  public int count(IreneQueryModel qmodel) {
    qmodel.setIndex(this);
    try {
      return searcher.count(qmodel);
    } catch (IOException e) {
      throw new QueryExecutionException(e);
    }
  }

  public TopDocs smartSearch(QExpr expr, int requested) throws IOException {
    final IreneQueryModel qmodel = prepare(expr);
    return searcher.search(qmodel, new SmartSearchCollectorManager(requested));
  }

  public IreneCountStatistics getTermStatistics(String field, String term) {
    try {
      CollectionStatistics stats = searcher.collectionStatistics(field);
      Term lterm = new Term(field, term);
      TermContext ctx = TermContext.build(searcher.getTopReaderContext(), lterm);
      TermStatistics termStats = searcher.termStatistics(lterm, ctx);

      return new IreneCountStatistics(stats, termStats, getVocabularySize(field));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
  public IreneCountStatistics getTermStatistics(String term) {
    return getTermStatistics(featureFactory.defaultField, term);
  }

  public IreneCountStatistics collectCountStatistics(StatsKey statsKey) {
    //System.err.println("collectCountStatistics: "+statsKey);
    try {
      // return a future, and immediately wait on it if someone beat us to it:
      final ForkJoinTask<IreneCountStatistics> statsFuture = countStatisticsCache.get(statsKey,
          missing -> ForkJoinPool.commonPool().submit(() -> computeCountStatistics(statsKey.getAsQExpr()))
      );
      return statsFuture.get();
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException(statsKey.toString(), e);
    }
  }

  public static final boolean multithreadStats = true;

  public IreneCountStatistics computeCountStatistics(QExpr expr) {
    IreneQueryModel model = prepare(expr);
    model.throwOnCachedStats = true;
    model.allowCachedStats = false;
    Set<String> fields = model.findFieldsNeeded();
    if(fields.size() == 0) throw new IllegalArgumentException("Query "+expr+" is basically meaningless, has no fields, which means it has no terms...");
    if(fields.size() > 1) throw new IllegalArgumentException("Cannot compute statistics across multiple fields...");
    // TODO: assert(model.isCountExpr());
    String field = fields.iterator().next();

    try {
      return searcher.search(model, new StatisticsCollector(field));
    } catch (IOException e) {
      throw new QueryExecutionException(e);
    }
  }

  public StreamingStats getStats(QExpr scorer) {
    return this.collect(scorer, new ScoreInfoCollector());
  }

  public IreneQueryLanguage getFeatureFactory() {
    return featureFactory;
  }

  public QExpr transform(QExpr query) {
    return featureFactory.applyMacros(query);
  }

  /** Return a simple query of the given "model" from an input text (use the default analyzer to turn it into terms) */
  public QExpr simpleQuery(String model, String field, String qtext) throws IOException {
    List<String> terms = AnalyzerUtil.tokenize(this.analyzer, qtext);
    QExpr expr = new QExpr(model);
    for (String term : terms) {
      expr.addChild(QExpr.fieldMatch(field, term));
    }
    return featureFactory.applyMacros(expr);
  }

  public QExpr simpleQuery(String model, String qtext) throws IOException {
    return simpleQuery(model, featureFactory.defaultField, qtext);
  }


  public int getTotalDocuments() {
    return this.reader.numDocs();
  }

  public List<String> getFieldTerms(int documentNumber, String fieldName) throws IOException {
    return AnalyzerUtil.tokenize(analyzer, getField(documentNumber, fieldName));
  }

  public int count(QExpr qExpr) {
    return count(prepare(qExpr));
  }

  public Explanation explain(QExpr qExpr, int doc) throws IOException {
    return searcher.explain(prepare(qExpr), doc);
  }

  public IntList matching(QExpr expr) {
    IreneQueryModel qf = prepare(expr);
    try {
      return searcher.search(qf, new AllMatchesCollectorManager());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public IntStream allDocs() {
    return IntStream.range(0, reader.maxDoc());
  }

  public Terms getTerms(String field) throws IOException {
    return MultiFields.getTerms(reader, field);
  }

  public <T> T collect(QExpr expr, CollectorManager<?,T> manager) {
    try {
      return searcher.search(prepare(expr), manager);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  ConcurrentHashMap<String, Future<Long>> vocabSizeByField = new ConcurrentHashMap<>();

  public long getVocabularySize(String field) {
    Future<Long> resp = vocabSizeByField.computeIfAbsent(field, missing -> ForkJoinPool.commonPool().submit(() -> {
      final Terms tokens = getTerms(missing);
      // fast token count if possible:
      long numTokens = tokens.size();


      // slow token count if not:
      if(numTokens == -1) {
        final TermsEnum iterator = tokens.iterator();
        long count = 0;
        while(true) {
          BytesRef term = iterator.next();
          if (term == null) break;
          count++;
        }
        numTokens = count;
      }

      return numTokens;
    }));

    try {
      return resp.get();
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException("GetVocabularySize: "+field, e);
    }
  }

  public IreneQueryModel prepare(QExpr qExpr) {
    return new IreneQueryModel(this, qExpr);
  }

  public List<String> tokenize(String text) {
    try {
      return AnalyzerUtil.tokenize(analyzer, text);
    } catch (IOException err) {
      throw new RuntimeException(err);
    }
  }

}
