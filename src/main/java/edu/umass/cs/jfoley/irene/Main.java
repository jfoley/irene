package edu.umass.cs.jfoley.irene;

import ciir.jfoley.chai.time.Debouncer;
import edu.umass.cs.jfoley.irene.impl.IreneSimilarity;
import org.apache.lucene.benchmark.byTask.feeds.DocData;
import org.apache.lucene.benchmark.byTask.feeds.NoMoreDataException;
import org.apache.lucene.benchmark.byTask.feeds.TrecContentSource;
import org.apache.lucene.benchmark.byTask.utils.Config;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {
  public static final Logger logger = Logger.getLogger(Main.class.getName());

  public static class IndexAquaint {
    public static void main(String[] args) throws IOException {
      Main.main(new String[] {"--input=/mnt/scratch/jfoley/robust04raw", "--output=/mnt/scratch3/jfoley/aquaint.irene", "--total=1088468"});
    }
  }
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);

    logger.setUseParentHandlers(false);
    FileHandler lfh = new FileHandler("indexing-errors.log");
    SimpleFormatter formatter = new SimpleFormatter();
    lfh.setFormatter(formatter);
    logger.addHandler(lfh);

    TrecContentSource source = new TrecContentSource();
    Properties props = new Properties();
    props.setProperty("tests.verbose", "false"); // don't print config when running?
    props.setProperty("content.source.excludeIteration", "true"); // skip adding iteration to names
    props.setProperty("content.source.forever", "false"); // lucene keeps reading indefinitely unless this is set to false, since it's trec parsers are for benchmarking...
    props.setProperty("docs.dir", argp.get("input", "/mnt/scratch/jfoley/robust04raw"));
    source.setConfig(new Config(props));

    long total = argp.get("total", 520000);
    int count = 0;
    Debouncer msg = null;

    try (final Directory dir = FSDirectory.open(Paths.get(argp.get("output", "/mnt/scratch3/jfoley/robust.timing.irene")))) {
      final IndexWriterConfig cfg = new IndexWriterConfig(new IreneEnglishAnalyzer());
      cfg.setSimilarity(new IreneSimilarity());
      System.out.println("Similarity: "+cfg.getSimilarity());
      cfg.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

      try (IndexWriter writer = new IndexWriter(dir, cfg)) {
        msg = new Debouncer();

        while (true) {
          DocData doc = new DocData();
          try {
            doc = source.getNextDocData(doc);
          } catch (NoMoreDataException e) {
            break;
          } catch (Exception e) {
            logger.log(Level.WARNING, "Document Skipped because of Parsing Error.", e);
            continue;
          }
          Document ldoc = createDocumentFromTREC(doc);
          writer.addDocument(ldoc);

          count++;
          if (msg.ready()) {
            if(total == 0) {
              System.err.println("L.Indexing: " + doc.getName() + " " + count + " so far... " + msg.estimate(count));
            } else {
              System.err.println("L.Indexing: " + doc.getName() + " " + count + " so far... " + msg.estimate(count, total));
            }
          }
        }
        writer.commit();
      }
    }

    System.err.println("L.Indexing.Done " + msg.estimate(count, count));
  }

  private static Document createDocumentFromTREC(DocData doc) {
    Document out = new Document();
    out.add(new StringField("id", doc.getName(), Field.Store.YES));
    String title = doc.getTitle();
    if(title != null) {
      out.add(new TextField("title", doc.getBody(), Field.Store.YES));
    }
    out.add(new TextField("body", doc.getBody(), Field.Store.YES));
    return out;
  }
}
