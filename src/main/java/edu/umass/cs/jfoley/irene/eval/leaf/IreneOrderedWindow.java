package edu.umass.cs.jfoley.irene.eval.leaf;

import ciir.jfoley.chai.collections.util.ListFns;
import edu.umass.cs.jfoley.irene.eval.CountEvalNode;
import edu.umass.cs.jfoley.irene.eval.phrase.PositionsIterator;
import edu.umass.cs.jfoley.irene.expr.IreneQueryModel;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley
 */
@QueryOperator(value={"od", "ordered"})
@TermDataNeeded(DataNeeded.POSITIONS)
public class IreneOrderedWindow implements CountEvalNode {
  public static int maxFrequency = -1;
  private final List<LeafEvalNode> children;
  private final int width;
  private final IreneQueryModel index;
  private IreneCountStatistics stats = null;

  public static boolean estimateStats = false;

  private int calculatedDocument = -1;
  private int currentFrequency = 0;

  public IreneOrderedWindow(IreneQueryModel index, int width, List<LeafEvalNode> children) {
    this.index = index;
    this.width = width;
    this.children = children;
  }

  @Override
  public int length(int doc) {
    return children.get(0).length(doc);
  }

  public static IreneCountStatistics estimateMin(List<LeafEvalNode> children) {
    IreneCountStatistics stats = new IreneCountStatistics();
    stats.collectionFrequency = Long.MAX_VALUE;
    stats.documentFrequency = Long.MAX_VALUE;
    for (LeafEvalNode child : children) {
      IreneCountStatistics chStats = child.getCountStatistics();
      stats.collectionLength = chStats.collectionLength;
      stats.totalDocumentCount = chStats.totalDocumentCount;

      stats.collectionFrequency = Math.min(stats.collectionFrequency, chStats.collectionFrequency);
      stats.documentFrequency = Math.min(stats.documentFrequency, chStats.documentFrequency);
    }
    return stats;
  }

  @Override
  public IreneCountStatistics getCountStatistics() {
    if(stats == null) {
      if(estimateStats) {
        this.stats = estimateMin(children);
        return stats;
      } else {
        assert (index != null);
        stats = index.collectCountStatistics(
            new StatsKey.OrderedWindowStatsKey(
                ListFns.map(children, LeafEvalNode::getTerm),
                width));
      }
    }
    return stats;
  }

  static int countIter(List<PositionsIterator> arrayIterators, int width) throws IOException {
    int hits = 0;
    boolean notDone = true;
    while(notDone) {
      // find the start of the first word
      boolean invalid = false;

      // loop over all the rest of the words
      for (int i = 1; i < arrayIterators.size(); i++) {
        int end = arrayIterators.get(i - 1).currentPosition()+1;

        // try to move this iterator so that it's past the end of the previous word
        assert (arrayIterators.get(i) != null);
        assert (!arrayIterators.get(i).isDone());
        while (end > arrayIterators.get(i).currentPosition()) {
          notDone = arrayIterators.get(i).next();

          // if there are no more occurrences of this word,
          // no more ordered windows are possible
          if (!notDone) {
            return hits;
          }
        }

        if (arrayIterators.get(i).currentPosition() - end >= width) {
          invalid = true;
          break;
        }
      }

      // if it's a match, record it
      if (!invalid) {
        hits++;
        if(IreneOrderedWindow.maxFrequency > 0 && hits >= IreneOrderedWindow.maxFrequency) {
          return hits;
        }
      }

      // move the first iterator forward - we are double dipping on all other iterators.
      notDone = arrayIterators.get(0).next();
    }

    return hits;
  }

  private int countIntersection(int doc) throws IOException {
    List<PositionsIterator> posIters = new ArrayList<>();
    for (LeafEvalNode child : children) {
      if(!child.matches(doc)) {
        return 0;
      }
      int count = child.count(doc);
      if(count > 0) {
        posIters.add(child.positionsIter(doc));
      } else {
        return 0;
      }
    }

    return countIter(posIters, width);
  }

  @Override
  public int count(int doc) throws IOException {
    if(calculatedDocument == doc) {
      return currentFrequency;
    }
    currentFrequency = countIntersection(doc);
    calculatedDocument = doc;
    return currentFrequency;
  }

  @Override
  public float score(int doc) throws IOException {
    return count(doc);
    //throw new IllegalStateException("Don't call score() on an IreneOrderedWindow!");
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    int count = count(doc);
    if(count == 0) {
      return Explanation.noMatch("OrderedWindow no match: ");
    } else {
      List<Explanation> childExplains = new ArrayList<>();
      for (LeafEvalNode child : children) {
        childExplains.add(child.explain(doc));
      }
      return Explanation.match(count, "OrderedWindow match: ", childExplains);
    }
  }

  @Override
  public boolean matches(int id) throws IOException {
    for (LeafEvalNode child : this.children) {
      if(!child.matches(id)) return false;
    }
    return count(id) > 0;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createAndMover(this.children);
  }

  /**
   * TODO, this causes an deadlock in calculate statistics.
   * We want to grab statistics if they're available, not just return zero... but that causes a deadlock if we've created this query to literally calculate the statistics.
   * @see IreneUnorderedWindow#estimateDF()
   * @return an estimate of how many times we expect this to hit.
   */
  @Override
  public long estimateDF() {
    if(stats == null) {
      // Cheap estimate: take the min if we have any children:
      return IreneOrderedWindow.estimateMin(children).documentFrequency;
    }
    return stats.documentFrequency;
  }
}
