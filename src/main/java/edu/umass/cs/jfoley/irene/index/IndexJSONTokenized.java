package edu.umass.cs.jfoley.irene.index;

import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.string.StrUtil;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.util.List;

/**
 * Assume documents have been pre-tokenized for us.
 * @author jfoley.
 */
public class IndexJSONTokenized {
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);

    // Input file, maybe compressed.
    String input = argp.getAsString("input");
    // Index dir, where should we save this?
    String output = argp.getAsString("output");

    String idKey = argp.get("idKey", "id");
    List<String> tokenFields = argp.getAsList("fields", String.class);

    if (tokenFields.isEmpty()) {
      throw new IllegalArgumentException("--fields+something must be specified!");
    }

    long total = argp.get("total", 0);

    try (LinesIterable sdocs = LinesIterable.fromFile(input);
         IreneIndexer writer = IndexingParams.start().setDefaultAnalyzer(new WhitespaceAnalyzer()).withPath(output).create().build()) {
      for (String text : sdocs) {
        try {
          Parameters json = Parameters.parseString(text);
          final String id = json.getAsString(idKey);

          Document doc = new Document();
          doc.add(new StringField("id", id, Field.Store.YES));
          for (String field : tokenFields) {
            List<String> tokens = json.getAsList(field, String.class);
            doc.add(new TextField(field, StrUtil.join(tokens), Field.Store.YES));
          }
          writer.pushDocument(doc);
          writer.printMessageIfReady(System.err, total);
        } catch (Exception e) {
          // Couldn't parse a particular document.
          System.err.println("Error on L#"+sdocs.getLineNumber()+" "+e.getMessage());
        }
      }
    }
  }
}
