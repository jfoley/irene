package edu.umass.cs.jfoley.irene.eval.leaf;

import ciir.jfoley.chai.collections.list.IntList;
import edu.umass.cs.jfoley.irene.impl.IreneTermInfo;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.index.NumericDocValues;
import org.apache.lucene.index.PostingsEnum;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.Explanation;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * @author jfoley
 */
public class IreneIndexedTerm implements LeafEvalNode {
  public final IreneTermInfo stats;
  private final PostingsEnum postings;
  private final NumericDocValues lengths;

  // To make our IndexedTerm robust (read: sharable), we want to cache positions here.
  int positionsForDocument = -1;
  private IntList positions = new IntList();

  public IreneIndexedTerm(IreneTermInfo stats, NumericDocValues lengths, PostingsEnum postings) {
    this.stats = stats;
    this.lengths = lengths;
    this.postings = postings;
  }

  public IntList positions(int doc) throws IOException {
    if(positionsForDocument == doc) {
      return positions;
    }

    //System.err.println("pull-positions "+doc);

    // mark cached positions as from this document
    positionsForDocument = doc;
    // clear any old positions
    positions.clear();

    // move iter if need be
    // leave if nothing to pull.
    int count = count(doc);
    if(count == 0) {
      return positions;
    }

    // allocate enough space
    positions.reserve(count);
    positions.unsafeSetFill(count);

    try {
      // copy as fast as possible; JVM should SIMD this (hopefully, but Lucene has a loop dependency).
      int[] back = positions.unsafeArray();
      for (int i = 0; i < count; i++) {
        int pos = postings.nextPosition();
        if (pos < 0) return null;
        back[i] = pos;
      }
    } catch (AssertionError aer) {
      System.err.println("AssertionError: "+getTerm()+" "+count+" "+positions+"@"+doc+" "+postings.docID());
      throw aer;
    }

    return positions;
  }

  @Override
  public int frequency() throws IOException {
    if (postings.docID() == DocIdSetIterator.NO_MORE_DOCS) {
      return 0;
    }
    return postings.freq();
  }

  @Override
  public int currentDocument() throws IOException {
    return postings.docID();
  }

  @Override
  public int length(int doc) {
    try {
      //if(doc == 21106) { System.err.println("#length("+doc+")="+lengths.get(doc)+" obj="+lengths.toString()); }
      return (int) lengths.get(doc);
    } catch (IndexOutOfBoundsException ioob) {
      return 10;
    }
  }

  @Override
  public IreneTermInfo getTermInfo() {
    return stats;
  }

  @Override
  public int advance(int target) throws IOException {
    return postings.advance(target);
  }

  @Nonnull
  @Override
  public MoveInfo getMovementInfo() {
    return MoveInfo.wrapTerm(postings, estimateDF());
  }

  @Override
  public long estimateDF() {
    return stats.getTermStatistics().docFreq();
  }

  public void syncTo(int doc) throws IOException {
    if(postings.docID() < doc) {
      postings.advance(doc);
    }
  }

  @Override
  public int count(int doc) throws IOException {
    syncTo(doc);
    if(postings.docID() == doc) {
      return postings.freq();
    }
    return 0;
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    syncTo(doc);
    int count = count(doc);
    return Explanation.match(count, postings.getClass()+"\t"+stats.getTerm().toString()+"@"+doc+"="+postings.docID()+"\tlength="+length(doc));
  }
}
