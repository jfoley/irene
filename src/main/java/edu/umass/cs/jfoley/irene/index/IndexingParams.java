package edu.umass.cs.jfoley.irene.index;

import edu.umass.cs.jfoley.irene.IreneEnglishAnalyzer;
import edu.umass.cs.jfoley.irene.impl.IreneSimilarity;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author jfoley
 */
public class IndexingParams {
  private Analyzer defaultAnalyzer = new IreneEnglishAnalyzer();
  Map<String, Analyzer> perFieldAnalyzers = new HashMap<>();
  Directory directory = null;
  Similarity normsWriter = new IreneSimilarity();
  IndexWriterConfig.OpenMode openMode = null;

  @Nonnull
  public static IndexingParams start() {
    return new IndexingParams();
  }

  @Nonnull
  public IndexingParams withPath(String path) throws IOException {
    directory = FSDirectory.open(Paths.get(path));
    return this;
  }
  @Nonnull
  public IndexingParams withPath(File file) throws IOException {
    directory = FSDirectory.open(file.toPath());
    return this;
  }

  @Nonnull
  public IndexingParams create() {
    openMode = IndexWriterConfig.OpenMode.CREATE;
    return this;
  }

  @Nonnull
  public IndexingParams append() {
    openMode = IndexWriterConfig.OpenMode.CREATE_OR_APPEND;
    return this;
  }

  /**
   * In memory indexes are create, not append.
   * @return this for chaining.
   */
  @Nonnull
  public IndexingParams inMemory() {
    directory = new RAMDirectory();
    return this.create();
  }

  @Nonnull
  public IndexingParams withAnalyzer(String field, Analyzer analyzer) {
    perFieldAnalyzers.put(field, analyzer);
    return this;
  }

  @Nonnull
  public IreneIndexer build() throws IOException {
    return new IreneIndexer(this);
  }

  @Nonnull
  Directory getIOSystem() {
    return Objects.requireNonNull(directory, "Forgot to execute .withPath(...) or .inMemory()");
  }

  @Nonnull
  Analyzer getAnalyzer() {
    if(perFieldAnalyzers.isEmpty()) {
      return defaultAnalyzer;
    }
    return new PerFieldAnalyzerWrapper(defaultAnalyzer, perFieldAnalyzers);
  }

  @Nonnull
  Similarity getSimilarity() {
    return normsWriter;
  }

  @Nonnull
  IndexWriterConfig.OpenMode getOpenMode() {
    return Objects.requireNonNull(openMode, "Forgot to execute .create() or .append() to specify build style.");
  }

  public IndexingParams setDefaultAnalyzer(Analyzer defaultAnalyzer) {
    this.defaultAnalyzer = defaultAnalyzer;
    return this;
  }
}
