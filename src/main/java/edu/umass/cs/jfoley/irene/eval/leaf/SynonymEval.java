package edu.umass.cs.jfoley.irene.eval.leaf;

import ciir.jfoley.chai.collections.list.IntList;
import edu.umass.cs.jfoley.irene.eval.phrase.PositionsIterator;
import edu.umass.cs.jfoley.irene.expr.IreneQueryModel;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import edu.umass.cs.jfoley.irene.impl.IreneTermInfo;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.Explanation;
import org.roaringbitmap.IntConsumer;
import org.roaringbitmap.RoaringBitmap;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley
 */
@QueryOperator("syn")
public class SynonymEval implements LeafEvalNode {
  private final IreneQueryModel index;
  private final ArrayList<LeafEvalNode> terms;
  private final ArrayList<Term> childTerms;

  public SynonymEval(IreneQueryModel index, List<LeafEvalNode> terms) {
    this.index = index;
    this.terms = new ArrayList<>();
    this.childTerms = new ArrayList<>();
    for (LeafEvalNode term : terms) {
      if(!term.hasNoMatches()) {
        this.terms.add(term);
      }
      childTerms.add(term.getTermInfo().getTerm());
    }
  }

  @Override
  public int frequency() throws IOException {
    return count(currentDocument());
  }

  @Override
  public IntList positions(int doc) throws IOException {
    IntList output = new IntList();
    orPositions(doc).forEach((IntConsumer) output::push);
    return output;
  }

  public RoaringBitmap orPositions(int doc) throws IOException {
    RoaringBitmap bitmap = new RoaringBitmap();
    for (LeafEvalNode term : terms) {
      for(final PositionsIterator iter = term.positionsIter(doc); !iter.isDone(); iter.next()) {
        bitmap.add(iter.currentPosition());
      }
    }
    return bitmap;
  }

  @Override
  public PositionsIterator positionsIter(int doc) throws IOException {
    // TODO operate on RoaringBitmap directly
    return new PositionsIterator(positions(doc));
  }

  @Override
  public int currentDocument() throws IOException {
    int min = DocIdSetIterator.NO_MORE_DOCS;
    for (LeafEvalNode term : terms) {
      min = Math.min(term.currentDocument(), min);
    }
    return min;
  }

  @Override
  public void syncTo(int doc) throws IOException {
    for (LeafEvalNode term : terms) {
      term.syncTo(doc);
    }
  }

  @Override
  public int length(int doc) {
    return terms.get(0).length(doc);
  }

  @Override
  public int count(int doc) throws IOException {
    int count = 0;
    for (LeafEvalNode term : terms) {
      count += term.count(doc);
    }
    return count;
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    List<Explanation> cexp = new ArrayList<>();
    for (LeafEvalNode term : terms) {
      cexp.add(term.explain(doc));
    }

    if (count(doc) > 0) {
      return Explanation.match(count(doc), "#syn match", cexp);
    } else {
      return Explanation.noMatch("#syn miss", cexp);
    }
  }

  @Override
  public long estimateDF() {
    return terms.stream().mapToLong(LeafEvalNode::estimateDF).max().orElse(0L);
  }

  @Override
  public IreneTermInfo getTermInfo() {
    throw new UnsupportedOperationException();
  }

  @Override
  public IreneCountStatistics getCountStatistics() {
    return index.collectCountStatistics(new StatsKey.SynonymStatsKey(this.childTerms));
  }

  @Override
  public int advance(int target) throws IOException {
    int min = DocIdSetIterator.NO_MORE_DOCS;
    for (LeafEvalNode term : terms) {
      min = Math.min(term.advance(target), min);
    }
    return min;
  }

  @Nonnull
  @Override
  public MoveInfo getMovementInfo() {
    return createOrMover(terms);
  }
}
