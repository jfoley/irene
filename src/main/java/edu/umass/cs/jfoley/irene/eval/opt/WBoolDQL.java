package edu.umass.cs.jfoley.irene.eval.opt;

import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;

import java.io.IOException;
import java.util.List;

/**
 * Faster than #combine(), if you're doing dirichlet
 * @author jfoley.
 */
@QueryOperator("sdql")
@TermDataNeeded(DataNeeded.COUNTS)
public class WBoolDQL extends OptEvalNode {
  private final SortedMaxscoreDQL maxscore;
  private final WANDDQL wand;
  private OptEvalNode algo;

  public WBoolDQL(IreneIndex index, double mu, List<LeafEvalNode> children, float[] weights) {
    this.maxscore = new SortedMaxscoreDQL(index, mu, children, weights, "df");
    this.wand = new WANDDQL(index, mu, children, weights);
    this.algo = maxscore;
  }

  public boolean hasNoMatches() { return algo.hasNoMatches(); }

  @Override
  public float score(int doc) throws IOException {
    return algo.score(doc);
  }


  @Override
  public int count(int doc) throws IOException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    return algo.explain(doc);
  }

  @Override
  public void setMinimumRequiredScore(float theta) {
    // Use maxscore (with lowest DF items first unless WAND can construct an AND of at least 2 items)
    if(algo == maxscore) {
      maxscore.setMinimumRequiredScore(theta);
      int split = maxscore.split;
      if (split == 1) {
        wand.setMinimumRequiredScore(theta);
        if (wand.split > 1) {
          algo = wand;
        }
      }
    } else {
      algo.setMinimumRequiredScore(theta);
    }
  }

  @Override
  public boolean matches(int id) throws IOException {
    return algo.matches(id);
  }

  @Override
  public MoveInfo getMovementInfo() {
    return algo.getMovementInfo();
  }

  @Override
  public long estimateDF() {
    return algo.estimateDF();
  }

  @Override
  public int docID() {
    return algo.docID();
  }

  @Override
  public int nextDoc() throws IOException {
    return algo.nextDoc();
  }

  @Override
  public int advance(int target) throws IOException {
    return algo.advance(target);
  }

  @Override
  public long cost() {
    return algo.cost();
  }
}
