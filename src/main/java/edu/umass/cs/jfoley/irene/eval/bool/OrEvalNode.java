package edu.umass.cs.jfoley.irene.eval.bool;

import edu.umass.cs.jfoley.irene.eval.QueryEvalNode;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;

import java.io.IOException;
import java.util.List;

/**
 * @author jfoley.
 */
@QueryOperator(value = {"or", "any"})
public class OrEvalNode extends BoolEval {
  public OrEvalNode(List<QueryEvalNode> children) {
    super(children);
    this.children.sort(mostToLeastFrequent);
  }

  @Override
  public boolean matches(int id) throws IOException {
    for (QueryEvalNode child : children) {
      if(child.matches(id)) {
        return true;
      }
    }
    return false;
  }
}
