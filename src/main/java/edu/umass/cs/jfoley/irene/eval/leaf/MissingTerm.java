package edu.umass.cs.jfoley.irene.eval.leaf;

import ciir.jfoley.chai.collections.list.IntList;
import edu.umass.cs.jfoley.irene.impl.IreneTermInfo;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.index.NumericDocValues;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.Explanation;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * @author jfoley
 */
public class MissingTerm implements LeafEvalNode {
  private final IreneTermInfo stats;
  private final NumericDocValues lengths;

  public MissingTerm(IreneTermInfo stats, NumericDocValues lengths) {
    //System.err.println("MissingTerm("+stats.getTerm()+")");
    this.stats = stats;
    this.lengths = lengths;
  }

  @Override
  public int frequency() throws IOException {
    return 0;
  }

  @Override
  public IntList positions(int doc) throws IOException {
    return null;
  }

  @Override
  public int currentDocument() throws IOException {
    return DocIdSetIterator.NO_MORE_DOCS;
  }

  @Override
  public void syncTo(int doc) {

  }

  @Override
  public int length(int doc) {
    return (int) lengths.get(doc);
  }

  @Override
  public IreneTermInfo getTermInfo() {
    return stats;
  }

  @Override
  public int advance(int target) {
    return DocIdSetIterator.NO_MORE_DOCS;
  }

  @Nonnull
  @Override
  public MoveInfo getMovementInfo() {
    return MoveInfo.empty();
  }

  @Override
  public long estimateDF() {
    return 0;
  }

  @Override
  public int count(int doc) throws IOException {
    return 0;
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    return Explanation.match(0.0f, "NoSuchTerm-"+stats.getTerm().toString());
  }

  @Override
  public boolean hasNoMatches() {
    return true;
  }
}
