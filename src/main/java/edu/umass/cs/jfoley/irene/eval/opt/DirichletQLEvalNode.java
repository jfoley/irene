package edu.umass.cs.jfoley.irene.eval.opt;

import ciir.jfoley.chai.math.StreamingStats;
import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

/**
 * Faster than #combine(), if you're doing dirichlet
 * @author jfoley.
 */
@QueryOperator("dql")
@TermDataNeeded(DataNeeded.COUNTS)
public class DirichletQLEvalNode extends OptEvalNode {

  private final double mu;
  public final List<LeafEvalNode> children;
  private final float[] weights;
  private final float[] bestAfter;
  private final float[] minUntil;
  private final float[] backgrounds;
  private long numHits;
  private final int N;
  private int split;
  private int useful;

  public DirichletQLEvalNode(IreneIndex index, double mu, List<LeafEvalNode> children, float[] weights) {
    try {
      this.mu = mu;
      this.N = children.size();
      this.useful = N;
      this.children = new ArrayList<>(children);
      // TODO sort children a la BinImp
      this.weights = weights;
      this.numHits = 0L;
      // figure out current position and cost:
      int id = NO_MORE_DOCS;
      long maxCost = 0L;
      for (LeafEvalNode child : children) {
        id = Math.min(child.currentDocument(), id);
        maxCost = Math.max(child.estimateDF(), maxCost);
      }
      this.numHits = maxCost;
      this.current = id;

      backgrounds = new float[N];
      float[] bestScore = new float[N];
      minUntil = new float[N];
      bestAfter = new float[N];
      float minSum = 0;

      for (int i = 0; i < N; i++) {
        IreneCountStatistics stats = this.children.get(i).getCountStatistics();
        // TODO, if cf == 0, this shouldn't even be a node!
        long cf = stats.collectionFrequency;
        double clen = stats.collectionLength;
        backgrounds[i] = (float) (mu * (cf > 0 ? cf / clen : 0.5 / clen));

        StreamingStats scores;
        scores = OptChild.scoreStatsCache.get(this.children.get(i).getTerm(), missing ->
            ForkJoinPool.commonPool().submit(() ->
                index.getStats(new QExpr("dirichlet", new QExpr(missing))))
        ).get();

        bestScore[i] = weights[i] * (float) scores.getMax();

        // 0-scoring document of length 1...
        // Galago comments suggest this should be an overestimate:
        double min = weights[i] *  ((float) Math.log(backgrounds[i]) - Math.log(1 + mu));

        minSum += min;
        minUntil[i] = minSum;
      }

      float rvrSum = 0;
      for (int n = N-1; n >= 0; n--) {
        bestAfter[n] = rvrSum;
        rvrSum += bestScore[n];
      }
    } catch (IOException | InterruptedException | ExecutionException e) {
      throw new RuntimeException(e);
    }
  }

  // doesn't make any sense...
  public boolean hasNoMatches() { return children.size() == 0; }

  @Override
  public float score(int doc) throws IOException {
    if(current == doc) {
      // IF we reach here; Score unconditionally:
      float score = 0;
      for (int i = 0; i < split; i++) {
        LeafEvalNode child = children.get(i);
        int count = child.count(doc);
        int length = child.length(doc);
        score += weights[i] * Math.log((count + backgrounds[i]) / (mu + length));
      }
      if(split > 0 && score + bestAfter[split-1] < heapMinimumScore) {
        return OptChild.NegativeInfinity;
      }

      for (int i = split; (i<N); i++) {
        LeafEvalNode child = children.get(i);
        int count = child.count(doc);
        int length = child.length(doc);
        score += weights[i] * Math.log((count + backgrounds[i]) / (mu + length));
        if(score + bestAfter[i] < heapMinimumScore) {
          return OptChild.NegativeInfinity;
        }
      }
      return score;
    }
    return OptChild.NegativeInfinity;
  }


  @Override
  public int count(int doc) throws IOException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    //doc = -1;
    //score = 0;
    //match = false;

    heapMinimumScore = OptChild.NegativeInfinity;
    List<Explanation> children = new ArrayList<>(N);
    List<String> descs = new ArrayList<>(N);
      float myscore = 0;
      for (int i = 0; i < N; i++) {
        LeafEvalNode child = this.children.get(i);
        IreneCountStatistics stats = child.getCountStatistics();
        int where = child.currentDocument();
        int length = child.length(doc);
        int count = child.count(doc);
        Parameters descP = Parameters.parseArray("_doc", where, "weight", weights[i], "count", count, "length", child.length(doc), "mu", mu, "clen", stats.collectionLength, "cf", stats.collectionFrequency);
        double score = Math.log(count + backgrounds[i]) - Math.log(mu + length);
        descP.put("wscore", weights[i] * score);
        descP.put("score", score);
        myscore += weights[i] * score;

        String desc = descP.toString()+"\n";

        descs.add(desc);
        children.add(child.explain(doc));
      }
      return Explanation.match(myscore, myscore+"\t"+descs.toString(), children);
  }

  private float heapMinimumScore = -Float.MAX_VALUE;
  @Override
  public void setMinimumRequiredScore(float theta) {
    heapMinimumScore = theta;

    this.split = 0;
    useful = N;
    for (int i = 0; i < N; i++) {
      double est = minUntil[i] + bestAfter[i];
      if(est < theta) {
        split = i+1;
        useful = split;
        break;
      }
    }
  }

  @Override
  public boolean matches(int id) throws IOException {
    if(current < id) {
      current = advance(id);
    }
    return current == id;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createOrMover(children);
  }

  @Override
  public long estimateDF() {
    return numHits;
  }

  /** as an iterator! */
  int current = 0;

  @Override
  public int docID() {
    return current;
  }

  @Override
  public int nextDoc() throws IOException {
    return advance(current+1);
  }

  @Override
  public int advance(int target) throws IOException {
    int nextMin = NO_MORE_DOCS;
    for (int i = 0; i < useful; i++) {
      LeafEvalNode child = children.get(i);
      int where = child.currentDocument();
      if (where < target) {
        where = child.advance(target);
      }
      //nextMin = Math.min(where, nextMin);
      nextMin = where < nextMin ? where : nextMin;
      if (nextMin == target) break;
    }

    current = nextMin;
    return nextMin;
  }

  @Override
  public long cost() {
    return numHits;
  }
}
