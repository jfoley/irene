package edu.umass.cs.jfoley.irene.impl;

import edu.umass.cs.jfoley.irene.movement.MoveInfo;

import javax.annotation.Nonnull;

/**
 * @author jfoley
 */
public interface Movable {
  @Nonnull
  MoveInfo getMover();
}
