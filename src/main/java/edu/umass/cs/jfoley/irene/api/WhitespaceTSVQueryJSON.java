package edu.umass.cs.jfoley.irene.api;

import ciir.jfoley.chai.classifier.RankingMeasures;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.time.Debouncer;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author jfoley.
 */
public class WhitespaceTSVQueryJSON {
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    int depth = argp.get("requested", 30);
    String inputTSV = argp.getAsString("queries");
    String output = argp.getString("output");
    Cache<Integer, String> idToStr = Caffeine.newBuilder().maximumSize(200_000).build();
    long total = argp.get("total", 0L);

    Debouncer msg = new Debouncer();
    long completed = 0;
    try (IreneIndex index = new IreneIndex(argp.get("index", "robust.irene"));
         PrintWriter out = IO.openPrintWriter(output)) {
      for (String line : LinesIterable.fromFile(inputTSV).slurp()) {
        String[] parts = line.split("\t");
        if (parts.length != 2) {
          System.err.println("Unknown query line: ``" + line + "``");
          continue;
        }
        Parameters qid = Parameters.parseStringOrDie(parts[0]);
        String input = parts[1];

        QExpr qExpr = null;
        if (input.startsWith("{")) {
          try {
            qExpr = QExpr.parseJSON(Parameters.parseString(input));
          } catch (Exception e) {
            throw new RuntimeException(e);
          }
        } else {
          qExpr = index.simpleQuery(argp.get("model", "combine"), input);
        }

        Set<String> truth = new HashSet<>();
        if (qid.isList("truth")) {
          truth.addAll(qid.getAsList("truth", String.class));
        }

        TopDocs results = index.smartSearch(qExpr, depth);
        Parameters scoredResults = Parameters.create();
        ScoreDoc[] scoreDocs = results.scoreDocs;
        List<Pair<Boolean, Double>> pred = new ArrayList<>(depth);
        int minTruthRank = 0;
        for (int i = 0; i < scoreDocs.length; i++) {
          ScoreDoc scoreDoc = scoreDocs[i];
          int docId = scoreDoc.doc;
          String name = idToStr.get(docId, missing -> index.getField(missing, "id"));
          scoredResults.put(name, scoreDoc.score);
          pred.add(Pair.of(truth.contains(name), (double) scoreDoc.score));
          if (minTruthRank == 0 && truth.contains(name)) {
            minTruthRank = i+1;
          }
        }
        Parameters measures = Parameters.create();
        for (int i = 0; i < 5; i++) {
          int r = i+1;
          measures.put("P"+r, RankingMeasures.computePrec(pred, r));
        }
        measures.put("AP", RankingMeasures.computeAP(pred, truth.size()));
        if (pred.size() >= 2) {
          measures.put("AUC", RankingMeasures.computeAUC(pred));
        } else if (pred.size() == 1) {
          // put 1 if it was correct, 0 if not.
          measures.put("AUC", pred.get(0).left ? 1.0 : 0.0);
        } else {
          measures.put("AUC", 0.0);
        }

        if (minTruthRank == 0) {
          measures.put("RR", 0);
        } else {
          measures.put("RR", 1.0 / minTruthRank);
        }

        qid.put("results", scoredResults);
        qid.put("measures", measures);
        out.println(qid);
        completed++;

        if (msg.ready()) {
          System.err.println("# "+msg.estimate(completed, total));
        }
      }
    }
  }
}
