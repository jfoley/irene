package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.TermDataNeeded;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Don't use this, it's terrible for real documents, e.g. robust. Might be okay if you have tweets or other short documents. Not much faster than maxscore version of the other in current impl.
 * Robust: .243 vs. QL .253 and RM3 .294
 * @author jfoley.
 */
@QueryOperator("bdql")
@TermDataNeeded(DataNeeded.DOCS)
public class BinaryDirichletQLEvalNode implements QueryEvalNode {
  private final double mu;
  public final List<LeafEvalNode> children;
  private final float[] weights;
  private final float[] bestCaseRemaining;
  private final float[] backgrounds;
  private final float[] bestScore;
  private float minScore;
  private long numHits;
  private final int N;

  public BinaryDirichletQLEvalNode(IreneIndex index, double mu, List<LeafEvalNode> children, float[] weights) {

    this.mu = mu;
    this.N = children.size();
    this.children = new ArrayList<>(children);
    // TODO sort children a la BinImp
    this.weights = weights;
    this.numHits = 0L;
    for (LeafEvalNode child : this.children) {
      this.numHits = Math.max(child.estimateDF(), numHits);
    }

    backgrounds = new float[N];
    this.bestScore = new float[N];
    this.minScore = 0;
    for (int i = 0; i < N; i++) {
      IreneCountStatistics stats = this.children.get(i).getCountStatistics();
      // TODO, if cf == 0, this shouldn't even be a node!
      long cf = stats.collectionFrequency;
      double clen = stats.collectionLength;
      backgrounds[i] = (float) (mu * (cf > 0 ? cf / clen : 0.5 / clen));

      // "best" score for now is a long document where half the words are this term:
      // todo, use real stats:
      double avgdl = stats.averageDocumentLength();
      // Someday, find maxTF and length at that point:
      //bestLen = index.getExpensiveTermStats(children.get(i).getTerm());
      double bestLen = avgdl;
      bestScore[i] = weights[i] * ((float) (Math.log((1)+backgrounds[i]) - Math.log(bestLen+mu)));

      // calculate how much an average miss is scored as: (depends on doc length....)
      minScore += weights[i] * ((float) Math.log(0+backgrounds[i]) - Math.log(avgdl+avgdl+mu));
    }

    bestCaseRemaining = new float[N];
    for (int i = 0; i < N; i++) {
      float sum = 0;
      for (int j = i+1; j < N; j++) {
        sum += bestScore[j];
      }
      bestCaseRemaining[i] = sum;
    }
  }

  // doesn't make any sense...
  public boolean hasNoMatches() { return children.size() == 0; }

  @Override
  public float score(int doc) throws IOException {
    if(matches(doc)) {
      return score;
    }
    return -Float.MAX_VALUE;
  }


  @Override
  public int count(int doc) throws IOException {
    throw new UnsupportedOperationException();
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    //doc = -1;
    //score = 0;
    //match = false;

    heapMinimumScore = -Float.MAX_VALUE;
    List<Explanation> children = new ArrayList<>(N);
    List<String> descs = new ArrayList<>(N);
    if(matches(doc)) {
      double myscore = 0;
      for (int i = 0; i < N; i++) {
        LeafEvalNode child = this.children.get(i);
        IreneCountStatistics stats = child.getCountStatistics();
        int where = child.currentDocument();
        int length = child.length(doc);
        int count = child.count(doc);
        Parameters descP = Parameters.parseArray("_doc", where, "weight", weights[i], "count", count, "length", child.length(doc), "mu", mu, "clen", stats.collectionLength, "cf", stats.collectionFrequency);
        double score = Math.log(count + backgrounds[i]) - Math.log(mu + length);
        descP.put("wscore", weights[i] * score);
        descP.put("score", score);
        myscore += weights[i] * score;

        String desc = descP.toString()+"\n";

        descs.add(desc);
        children.add(child.explain(doc));
      }
      return Explanation.match(score, myscore+"\t"+descs.toString(), children);
    } else {
      return Explanation.noMatch("miss?");
    }
  }

  private float heapMinimumScore = -Float.MAX_VALUE;
  @Override
  public void setMinimumRequiredScore(float score) {
    heapMinimumScore = score;
  }

  // cache the score as you compute whether it matches:
  private int doc = -1;
  private float score = 0;
  private boolean match = false;

  @Override
  public boolean matches(int id) throws IOException {
    if(id == doc) { // optimize for being asked about the same document multiple times...
      return match;
    }

    // setup this document in our "cache"
    doc = id;
    score = 0;
    match = false;
    //expected.addAndGet(N);

    for (int i = 0; i < N; i++) {
      LeafEvalNode child = children.get(i);
      int count = child.matches(id) ? 1 : 0;
      int length = child.length(id);
      score += weights[i] * Math.log((count + backgrounds[i]) / (mu + length));
    }

    match = score >= heapMinimumScore;
    return match;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createOrMover(children);
  }

  @Override
  public long estimateDF() {
    return numHits;
  }
}

