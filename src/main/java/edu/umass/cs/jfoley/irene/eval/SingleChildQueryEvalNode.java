package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.movement.MoveInfo;

import java.io.IOException;

/**
 * @author jfoley
 */
public abstract class SingleChildQueryEvalNode<ChildType extends QueryEvalNode> implements QueryEvalNode {
  protected final ChildType child;

  protected SingleChildQueryEvalNode(ChildType child) {
    this.child = child;
  }

  @Override public boolean matches(int id) throws IOException { return child.matches(id); }
  @Override public MoveInfo getMovementInfo() {
    return child.getMovementInfo();
  }
  @Override public boolean hasNoMatches() { return child.hasNoMatches(); }
  @Override public long estimateDF() {
    return child.estimateDF();
  }
}
