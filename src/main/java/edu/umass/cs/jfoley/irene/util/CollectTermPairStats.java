package edu.umass.cs.jfoley.irene.util;

import ciir.jfoley.chai.collections.TopKHeap;
import ciir.jfoley.chai.collections.list.IntList;
import ciir.jfoley.chai.time.Debouncer;
import edu.umass.cs.jfoley.irene.IreneIndex;
import gnu.trove.map.hash.TObjectIntHashMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author jfoley
 */
public class CollectTermPairStats {
  public static class TermPair {
    private final int hash;
    String repr;
    public TermPair(String lhs, String rhs) {
      int cmp = lhs.compareTo(rhs);
      if(cmp <= 0) {
        repr = lhs+"_"+rhs;
      } else {
        repr = rhs+"_"+lhs;
      }
      hash = repr.hashCode();
    }
    public int hashCode() {
      return hash;
    }
    public boolean equals(Object other) {
      if(other instanceof TermPair) {
        TermPair rhs = (TermPair) other;
        return rhs.repr.equals(this.repr);
      }
      return false;
    }
  }

  public static void main(String[] args) throws IOException {
    IreneIndex index = new IreneIndex("/mnt/scratch3/jfoley/robust.irene");

    //ConcurrentHashMap<TermPair, Integer> stats = new ConcurrentHashMap<>(1_000_000);
    TObjectIntHashMap<TermPair> dfs = new TObjectIntHashMap<>(1_000_000);
    Debouncer msg = new Debouncer();

    AtomicLong done = new AtomicLong(0);
    long total = index.getTotalDocuments();
    index.allDocs().limit(1000).forEach(doc -> {
      try {
        TObjectIntHashMap<String> freqs = new TObjectIntHashMap<>();
        for (String token : index.getFieldTerms(doc, "body")) {
          freqs.adjustOrPutValue(token, 1, 1);
        }

        ArrayList<String> uterms = new ArrayList<>(freqs.keySet());
        IntList vfreqs = new IntList(uterms.size());
        for (String uterm : uterms) {
          vfreqs.push(freqs.get(uterm));
        }
        int vocabSize = uterms.size();
        for (int i = 0; i < vocabSize; i++) {
          String lhs = uterms.get(i);
          //int lfreq = vfreqs.getQuick(i);

          for (int j = i+1; j < vocabSize; j++) {
            String rhs = uterms.get(j);
            //int rfreq = vfreqs.getQuick(i);
            TermPair pair = new TermPair(lhs, rhs);
            dfs.adjustOrPutValue(pair, 1, 1);
            //stats.computeIfAbsent(pair, missing -> new StreamingStats()).push(Math.min(lfreq, rfreq));
          }
        }
        long proc = done.incrementAndGet();
        if(msg.ready()) {
          System.err.println(msg.estimate(proc, total));
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    });

    //stats.forEach((pair, pstats) -> {
      //System.out.println(pair.repr+" "+pstats);
    //});

    for (TopKHeap.Weighted<TermPair> pw : TopKHeap.takeTop(1000, dfs)) {
      System.out.println(pw.object.repr+"\t"+pw.weight);
    }

  }
}
