package edu.umass.cs.jfoley.irene;

import ciir.jfoley.chai.io.LinesIterable;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;

/**
 * @author jfoley
 */
public class TSVQueryToTrecrun {
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    String inputTSV = argp.getString("queries");
    int depth = argp.get("requested", 1000);

    QueryParser parser = new QueryParser("body", new EnglishAnalyzer(CharArraySet.EMPTY_SET));
    try (IreneIndex index = new IreneIndex(argp.get("index", "robust.lucene"))) {
      try {
        for (String line : LinesIterable.fromFile(inputTSV).slurp()) {
          String[] parts = line.split("\t");
          if(parts.length != 2) {
            System.err.println("Unknown query line: ``"+line+"``");
            continue;
          }
          String qid = parts[0];
          String input = parts[1];

          long start = System.currentTimeMillis();
          Query query = parser.parse(input);
          TopDocs results = index.searcher.search(query, depth);
          long end = System.currentTimeMillis();
          System.err.println(qid+" "+query.toString());
          System.err.println("  found " + results.totalHits+","+results.scoreDocs.length +" in "+(end - start)+"ms.");
          ScoreDoc[] scoreDocs = results.scoreDocs;

          for (int i = 0; i < scoreDocs.length; i++) {
            ScoreDoc scoreDoc = scoreDocs[i];
            int docId = scoreDoc.doc;
            Document doc = index.searcher.doc(docId);
            String name = index.getField(docId, "id");
            System.out.printf("%s Q0 %s %d %10.8f irene\n", qid, name, i+1, scoreDoc.score);
          }
        }
      } catch (ParseException e) {
        System.err.println(e.getMessage());
        e.printStackTrace(System.err);
      }
    }

  }
}
