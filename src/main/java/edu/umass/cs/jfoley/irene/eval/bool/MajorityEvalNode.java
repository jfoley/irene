package edu.umass.cs.jfoley.irene.eval.bool;

import edu.umass.cs.jfoley.irene.eval.QueryEvalNode;
import edu.umass.cs.jfoley.irene.expr.QueryOperator;

import java.io.IOException;
import java.util.List;

/**
 * @author jfoley.
 */
@QueryOperator("majority")
public class MajorityEvalNode extends BoolEval {
  public MajorityEvalNode(List<QueryEvalNode> children) {
    super(children);
  }

  @Override
  public boolean matches(int id) throws IOException {
    int yes = 0;
    for (QueryEvalNode child : children) {
      if(!child.matches(id)) {
        yes++;
      }
    }
    final int no = N - yes;
    return (yes > no);
  }
}
