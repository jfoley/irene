package edu.umass.cs.jfoley.irene.eval.leaf;

import ciir.jfoley.chai.collections.list.IntList;
import edu.umass.cs.jfoley.irene.eval.CountEvalNode;
import edu.umass.cs.jfoley.irene.eval.phrase.PositionsIterator;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import edu.umass.cs.jfoley.irene.impl.IreneTermInfo;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.TermStatistics;

import java.io.IOException;

/**
 * @author jfoley
 */
public interface LeafEvalNode extends CountEvalNode {
  /**
   * Get the frequency within current document.
   */
  int frequency() throws IOException;

  /**
   * Returns the positions list or null if not indexed; empty if not a match.
   * @param doc the document identifier to find.
   * @return a list of positions, or null
   * @throws IOException
   */
  IntList positions(int doc) throws IOException;
  default PositionsIterator positionsIter(int doc) throws IOException {
    return new PositionsIterator(positions(doc));
  }

  /**
   * @return the current document identifier, useful for checking for matches.
   * @throws IOException
   */
  int currentDocument() throws IOException;

  /**
   * Whether the current evaluator has a "match" for this document.
   * @param doc
   * @return
   * @throws IOException
   */
  default boolean matches(int doc) throws IOException {
    syncTo(doc);
    return currentDocument() == doc;
  }

  void syncTo(int doc) throws IOException;

  @Override
  default float score(int doc) throws IOException {
    syncTo(doc);
    return count(doc);
  }

  int length(int doc);
  IreneTermInfo getTermInfo();

  @Override
  default IreneCountStatistics getCountStatistics() {
    IreneTermInfo info = getTermInfo();
    CollectionStatistics cstats = info.getCollectionStatistics();
    TermStatistics tstats = info.getTermStatistics();

    // return statistics object:
    return new IreneCountStatistics(cstats, tstats, info.vocabularySize);
  }

  default Term getTerm() {
    return getTermInfo().getTerm();
  }

  /**
   * Note that you probably want to call syncTo unless you really know what you're doing; this is undefined if target (parameter) is equal to current (currentDocument())
   * @see org.apache.lucene.search.DocIdSetIterator#advance(int) for semantics
   * @param target the destination to move to
   * @return the actual document the iterator lands on
   */
  int advance(int target) throws IOException;
}
