package edu.umass.cs.jfoley.irene.index;

import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.time.Debouncer;
import edu.umass.cs.jfoley.irene.IreneEnglishAnalyzer;
import edu.umass.cs.jfoley.irene.impl.IreneSimilarity;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by jfoley on 4/23/16.
 */
public class IndexNewsRSS {
    public static final Logger logger = Logger.getLogger(IndexNewsRSS.class.getName());
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        logger.setUseParentHandlers(false);
        FileHandler lfh = new FileHandler("indexing-errors.log");
        SimpleFormatter formatter = new SimpleFormatter();
        lfh.setFormatter(formatter);
        logger.addHandler(lfh);

        long total = argp.get("total", 224616);
        long count = 0;
        Debouncer msg = null;

        // setup custom analyzers
        Map<String, Analyzer> analyzers = new HashMap<>();
        analyzers.put("categories", new WhitespaceAnalyzer());
        PerFieldAnalyzerWrapper analyzer = new PerFieldAnalyzerWrapper(new IreneEnglishAnalyzer(), analyzers);

        try (final Directory dir = FSDirectory.open(Paths.get(argp.get("output", "news_rss.irene")))) {
            final IndexWriterConfig cfg = new IndexWriterConfig(analyzer);
            cfg.setSimilarity(new IreneSimilarity());
            System.out.println("Similarity: "+cfg.getSimilarity());
            cfg.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

            try (LinesIterable sdocs = LinesIterable.fromFile(argp.get("input", "news.json.gz"))) {
                try (IndexWriter writer = new IndexWriter(dir, cfg)) {
                    msg = new Debouncer();

                    for (String sdoc : sdocs) {
                        try {
                            Parameters jsdoc = Parameters.parseString(sdoc);
                            count++;

                            long epochSecond = jsdoc.getLong("date") / 1000L;

                            Document doc = new Document();
                            doc.add(new TextField("categories", jsdoc.getString("categories"), Field.Store.YES));
                            doc.add(new TextField("categoriesText", jsdoc.getString("categories"), Field.Store.NO));
                            doc.add(new StringField("id", jsdoc.getString("id"), Field.Store.YES));
                            doc.add(new TextField("title", jsdoc.getString("title"), Field.Store.YES));
                            doc.add(new TextField("body", jsdoc.getString("description"), Field.Store.YES));
                            doc.add(new StringField("uri", jsdoc.getString("uri").toUpperCase(), Field.Store.YES));
                            doc.add(new StringField("source", jsdoc.getString("source"), Field.Store.YES));
                            doc.add(new LongField("storedTimeStamp", epochSecond, Field.Store.YES));
                            doc.add(new NumericDocValuesField("timeStamp", epochSecond));

                            writer.addDocument(doc);

                            if (msg.ready()) {
                                System.err.println("L.Indexing: " + jsdoc.getString("title") + " " + count + " so far... " + msg.estimate(count, total));
                            }
                        } catch (Exception e) {
                            logger.log(Level.WARNING, "Document-Exception", e);
                        }
                    }
                }

            }
        }

        System.err.println("L.Indexing.Done " + msg.estimate(count, count));
    }
}
