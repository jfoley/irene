package edu.umass.cs.jfoley.irene.expr.ast;

import edu.umass.cs.jfoley.irene.eval.BinaryImportanceEval;
import edu.umass.cs.jfoley.irene.eval.GalagoBM25EvalNode;
import edu.umass.cs.jfoley.irene.eval.GalagoCombine;
import edu.umass.cs.jfoley.irene.eval.RequireEval;
import edu.umass.cs.jfoley.irene.eval.bool.AndEvalNode;
import edu.umass.cs.jfoley.irene.eval.bool.MajorityEvalNode;
import edu.umass.cs.jfoley.irene.eval.bool.OrEvalNode;
import edu.umass.cs.jfoley.irene.eval.leaf.IreneOrderedWindow;
import edu.umass.cs.jfoley.irene.eval.leaf.IreneUnorderedWindow;
import edu.umass.cs.jfoley.irene.expr.types.InputTypeChecker;
import edu.umass.cs.jfoley.irene.expr.types.OperatorType;

import java.util.*;

/**
 * @author jfoley
 */
public class IreneLanguageInfo {
  private final HashMap<String, IreneQueryNodeInfo> operators = new HashMap<>();
  public List<IreneQueryNodeInfo> definedNodes;
  public IreneLanguageInfo() {
    definedNodes = new ArrayList<>();
    definedNodes.add(new IreneQueryNodeInfo.Simple(
        "combine",
        Collections.emptySet(),
        GalagoCombine.class,
        new InputTypeChecker.OneOrMore(InputTypeChecker.scores),
        OperatorType.SCORES
    ));
    definedNodes.add(new IreneQueryNodeInfo.Simple(
        "require",
        Collections.emptySet(),
        RequireEval.class,
        new InputTypeChecker.XThenY(InputTypeChecker.bools, InputTypeChecker.scores),
        OperatorType.SCORES
    ));
    definedNodes.add(new IreneQueryNodeInfo.Simple(
        "importance",
        Collections.emptySet(),
        BinaryImportanceEval.class,
        new InputTypeChecker.KOrMore(InputTypeChecker.bools, 2),
        OperatorType.SCORES
    ));
    definedNodes.add(new IreneQueryNodeInfo.Simple(
        "and",
        Collections.emptySet(),
        AndEvalNode.class,
        new InputTypeChecker.KOrMore(InputTypeChecker.bools, 2),
        OperatorType.BOOLS
    ));
    definedNodes.add(new IreneQueryNodeInfo.Simple(
        "or",
        Collections.emptySet(),
        OrEvalNode.class,
        new InputTypeChecker.KOrMore(InputTypeChecker.bools, 2),
        OperatorType.BOOLS
    ));
    definedNodes.add(new IreneQueryNodeInfo.Simple(
        "majority",
        Collections.emptySet(),
        MajorityEvalNode.class,
        new InputTypeChecker.KOrMore(InputTypeChecker.bools, 3),
        OperatorType.BOOLS
    ));

    definedNodes.add(new IreneQueryNodeInfo.Simple(
        "od",
        Arrays.asList("ordered"),
        IreneOrderedWindow.class,
        new InputTypeChecker.OneOrMore(InputTypeChecker.positions),
        OperatorType.COUNTS
    ));
    definedNodes.add(new IreneQueryNodeInfo.Simple(
        "uw",
        Arrays.asList("unordered"),
        IreneUnorderedWindow.class,
        new InputTypeChecker.OneOrMore(InputTypeChecker.positions),
        OperatorType.COUNTS
    ));
    definedNodes.add(new IreneQueryNodeInfo.Simple(
        "bm25",
        Collections.emptySet(),
        GalagoBM25EvalNode.class,
        new InputTypeChecker.ExactlyN(InputTypeChecker.counts, 1),
        OperatorType.SCORES
    ));

    for (IreneQueryNodeInfo definedNode : definedNodes) {
      final String name = definedNode.getCanonicalName();
      operators.put(name, definedNode);
    }
  }
}
