package edu.umass.cs.jfoley.irene.eval.opt;

import ciir.jfoley.chai.math.StreamingStats;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import org.apache.lucene.index.Term;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

/**
 * @author jfoley
 */
public final class OptChild {
  public static final float NegativeInfinity = -Float.MAX_VALUE;
  /**
   * Todo, this is a hack.
   */
  public static Cache<Term, Future<StreamingStats>> scoreStatsCache = Caffeine.newBuilder().maximumSize(100_000).build();
  public final float weight;
  public final LeafEvalNode node;
  public final StreamingStats scores;
  public final IreneCountStatistics stats;
  public final double mu;
  public final double background;
  public final double max;
  public final double min;
  public double diff;

  public OptChild(IreneIndex index, double mu, float weight, LeafEvalNode node) {
    this.mu = mu;
    this.weight = weight;
    this.node = node;
    this.stats = node.getCountStatistics();
    try {
      this.scores = scoreStatsCache.get(node.getTerm(), missing ->
          ForkJoinPool.commonPool().submit(() -> index.getStats(new QExpr("dirichlet", new QExpr(missing)).setConfig("mu", mu))

          )).get();
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException(e);
    }

    long cf = stats.collectionFrequency;
    double clen = stats.collectionLength;
    this.background = (mu * (cf > 0 ? cf / clen : 0.5 / clen));
    this.max = scores.getMax() * weight;

    // 0-scoring document of length 1...
    // Galago comments suggest this should be an overestimate:
    this.min = weight * ((float) Math.log(background) - Math.log(1 + mu));
    diff = max - min;
  }

  public final double score(int doc) throws IOException {
    int count = node.count(doc);
    int length = node.length(doc);
    return weight * Math.log((count + background) / (mu + length));
  }
}
