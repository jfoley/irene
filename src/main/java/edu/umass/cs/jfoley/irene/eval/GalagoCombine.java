package edu.umass.cs.jfoley.irene.eval;

import edu.umass.cs.jfoley.irene.expr.QueryOperator;
import edu.umass.cs.jfoley.irene.expr.macro.WeightMacroChain;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.Explanation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jfoley
 * @see WeightMacroChain.NormCombineOperations for where norm=true is resolved.
 */
@QueryOperator("combine")
public final class GalagoCombine implements QueryEvalNode {
  private final List<QueryEvalNode> children;
  private final float[] weights;
  private final ArrayList<QueryEvalNode> matchChildren;
  private long numHits;
  private int N;

  public GalagoCombine(List<QueryEvalNode> unigrams, float[] weights) {
    this.children = unigrams;
    this.N = children.size();
    this.weights = weights; // weights assigned from IreneQueryLanguage

    this.numHits = 0L;
    for (QueryEvalNode child : children) {
      this.numHits = Math.max(child.estimateDF(), numHits);
    }

    // speed up or slightly by having most frequent matchers first:
    this.matchChildren = new ArrayList<>(children.size());
    for (QueryEvalNode child : children) {
      if(child.hasNoMatches()) continue;
      matchChildren.add(child);
    }
    this.matchChildren.sort(mostToLeastFrequent);
  }

  @Override
  public float score(int doc) throws IOException {
    float sum = 0;
    for (int i = 0; i < N; i++) {
      sum += weights[i] * children.get(i).score(doc);
    }
    return sum;
  }

  @Override
  public int count(int doc) throws IOException {
    throw new RuntimeException("GalagoCombine.count called.");
  }

  @Override
  public Explanation explain(int doc) throws IOException {
    List<Explanation> cExplain = new ArrayList<>();
    for (QueryEvalNode child : children) {
      cExplain.add(child.explain(doc));
    }
    return Explanation.match(1.0f, "GalagoCombine", cExplain);
  }

  @Override
  public boolean matches(int id) throws IOException {
    for (QueryEvalNode child : matchChildren) {
      if(child.matches(id)) return true;
    }
    return false;
  }

  @Override
  public MoveInfo getMovementInfo() {
    return createOrMover(matchChildren);
  }

  @Override
  public long estimateDF() {
    return numHits;
  }
}
