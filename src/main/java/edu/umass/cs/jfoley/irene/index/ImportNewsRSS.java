package edu.umass.cs.jfoley.irene.index;

import ciir.jfoley.chai.collections.util.ListFns;
import ciir.jfoley.chai.io.Directory;
import ciir.jfoley.chai.io.IO;
import ciir.jfoley.chai.io.LinesIterable;
import ciir.jfoley.chai.string.StrUtil;
import ciir.jfoley.chai.time.Debouncer;
import org.jsoup.Jsoup;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by jfoley on 4/23/16.
 */
public class ImportNewsRSS {


    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        Directory input = Directory.Read(argp.get("input", "/home/jfoley/data/newsrss"));
        String outName = argp.get("output", "news.json.gz");

        Set<String> allowed = new HashSet<>();
        allowed.addAll(Arrays.asList(
                "http://news.google.com/?output=rss",
                "http://rss.news.yahoo.com/rss/topstories",
                "http://www.guardian.co.uk/rssfeed/0,,1,00.xml",
                "http://feeds.bbci.co.uk/news/world/rss.xml",
                "http://rss.cnn.com/rss/cnn_topstories.rss",
                "http://feeds.huffingtonpost.com/huffingtonpost/LatestNews",
                "http://feeds.abcnews.com/abcnews/topstories",
                "http://feeds.feedburner.com/TheAtlantic",
                "http://newsrss.bbc.co.uk/rss/newsonline_world_edition/front_page/rss.xml",
                "http://rssfeeds.usatoday.com/usatoday-NewsTopStories",
                "http://rss.time.com/web/time/rss/top/index.xml",
                "http://www.slate.com/rss",
                "http://www.nytimes.com/services/xml/rss/nyt/HomePage.xml",
                "http://feeds.reuters.com/reuters/topNews?irpc=69",
                "http://www.guardian.co.uk/world/usa/rss"
        ));

        Set<String> skippedSoFar = new HashSet<>();

        Debouncer msg = new Debouncer();
        int id = 0;
        try (PrintWriter output = IO.openPrintWriter(outName)) {
            for (File file : input.children()) {
                System.err.println(file);
                if (file.getName().endsWith(".jsonl.gz")) {
                    try (LinesIterable lines = LinesIterable.fromFile(file)) {
                        for (String line : lines) {
                            try {
                                int current = id++;
                                Parameters rawDoc = Parameters.parseString(line);

                                if(msg.ready()) {
                                    System.err.println(rawDoc.getString("title") +" "+ msg.estimate(id));
                                }

                                String source = rawDoc.getString("source");
                                if (!allowed.contains(source)) {
                                    if (!skippedSoFar.contains(source)) {
                                        skippedSoFar.add(source);
                                        System.err.println("Skipping from source: " + source);
                                    }
                                    continue;
                                }

                                String cats = StrUtil.join(ListFns.map(rawDoc.getAsList("categories", Parameters.class),
                                        p -> StrUtil.compactSpaces(p.get("name", "")).replaceAll("\\s+", "_")));

                                Parameters jsDoc = Parameters.create();
                                jsDoc.put("id", "NRSS_" + current);
                                jsDoc.put("categories", cats);
                                jsDoc.put("date", rawDoc.getLong("date"));
                                jsDoc.put("uri", rawDoc.getString("uri"));
                                jsDoc.put("title", Jsoup.parse(rawDoc.getString("title")).text());
                                jsDoc.put("source", source);

                                String raw = rawDoc.getMap("description").get("value", "");
                                String cleaned = Jsoup.parse(raw).text();
                                jsDoc.put("description", cleaned);
                                output.println(jsDoc.toString());
                            } catch (Exception iae) {
                                continue;
                            }

                        }
                    }
                }
            }
        }
    }
}
