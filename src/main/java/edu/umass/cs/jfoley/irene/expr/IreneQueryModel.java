package edu.umass.cs.jfoley.irene.expr;

import edu.umass.cs.jfoley.irene.IreneIndex;
import edu.umass.cs.jfoley.irene.eval.QueryEvalNode;
import edu.umass.cs.jfoley.irene.eval.iters.OrDocIdSetIterator;
import edu.umass.cs.jfoley.irene.eval.leaf.StatsKey;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import edu.umass.cs.jfoley.irene.impl.IreneScorer;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.*;

/**
 * @author jfoley
 */
public class IreneQueryModel extends Query {
  protected IreneIndex index;
  IreneQueryLanguage availableFeatures;
  final QExpr originalQuery;
  public final QExpr query;
  /**
   * Sometimes sharing iterators is faster, sometimes its slower.
   * @see SharedLeafCreator
   * @see UnsharedLeafCreator
   */
  public boolean shouldShareIterators = false;

  /** This can be set to false to avoid caching. */
  public boolean allowCachedStats = true;
  public boolean throwOnCachedStats = false;

  public IreneQueryModel(IreneIndex index, QExpr originalQuery) {
    this.index = index;
    this.availableFeatures = index.getFeatureFactory();
    this.originalQuery = originalQuery;
    this.query = index.transform(originalQuery);
    try {
      this.availableFeatures.typeCheck(query);
    } catch (Throwable err) {
      System.err.println("Some kind of crash while handling query: "+originalQuery);
    }
  }

  /**
   * Movers are separate from scorers in this model.
   * @return a DocIdSetIterator.
   */
  protected DocIdSetIterator createMover(QueryContext context) {
    List<DocIdSetIterator> iters = context.getAllIterators();

    if(iters.size() == 0) return DocIdSetIterator.empty();
    if(iters.size() == 1) {
      return iters.get(0);
    }
    return new OrDocIdSetIterator(iters);
  }

  /**
   * @param searcher the searcher object
   * @param needsScores whether lucene thinks this query needs scores or is boolean.
   * @return a weight object that can create a scorer for every shard.
   * @throws IOException if something is not found or corrupted.
   */
  public Weight createWeight(IndexSearcher searcher, boolean needsScores) throws IOException {
    IreneQueryModel that = this;
    List<IreneQueryLanguage.TermRequest> termRequests = availableFeatures.collectTermRequests(query);

    List<LuceneScorerRequest> rawQueries = requestQueries();
    for (LuceneScorerRequest rawQuery : rawQueries) {
      rawQuery.setWeight(rawQuery.getQuery().createWeight(searcher, rawQuery.needsScores));
    }

    return new Weight(that) {
      @Override
      public void extractTerms(Set<Term> terms) {
        throw new UnsupportedOperationException();
      }

      @Override
      public Explanation explain(LeafReaderContext context, int doc) throws IOException {
        ExplainScorer scorer = scorer(context);
        scorer.advance(doc);
        return scorer.explain(doc);
      }

      @Override public float getValueForNormalization() throws IOException { return 1.0f; }
      @Override public void normalize(float norm, float topLevelBoost) { }

      @Override
      public ExplainScorer scorer(LeafReaderContext context) throws IOException {
        QueryContext queryContext = new QueryContext(that, searcher, context, that.shouldShareIterators);
        // setup:
        queryContext.withTermRequests(termRequests);
        queryContext.withScorerRequests(rawQueries);

        return that.createScorer(queryContext);
      }
    };
  }

  public void setIndex(IreneIndex index) {
    this.index = index;
  }


  public List<LuceneScorerRequest> requestQueries() {
    return availableFeatures.collectQueryUsage(query);
  }

  public Set<String> findFieldsNeeded() {
    Map<Term, DataNeeded> terms = new HashMap<>();
    availableFeatures.collectTerms(query, terms, DataNeeded.DOCS);
    HashSet<String> fields = new HashSet<>();
    for (Term term : terms.keySet()) {
      fields.add(term.field());
    }
    return fields;
  }

  public static boolean newMovementStyle = true;
  public static MoveInfo.SimplificationParams movementSimplificationParams = new MoveInfo.SimplificationParams(-1);

  protected ExplainScorer createScorer(QueryContext context) {
    final QueryEvalNode evaluator = availableFeatures.reify(query, context);

    if(query.config.get("scoreAll", false)) {
      return new IreneAllScorer(evaluator, index.reader.maxDoc());
    }
    if(query.config.get("considerAll", false)) {
      return new IreneScorer(evaluator, DocIdSetIterator.all(index.reader.maxDoc()));
    }

    //if(scorers.isEmpty() && NoSharingNeeded(evaluator)) {
    // TODO and !overlap:
    //return new IreneScorer(evaluator, evaluator.getMovementInfo());
    //}
    if(newMovementStyle) {
      final MoveInfo moveInfo = evaluator.getMovementInfo();
      //System.out.println("MOVE_INFO: "+moveInfo.toJSON().toPrettyString());
      MoveInfo simplified = moveInfo;
      // TODO bug in move simplification for require(fold=test, imdb)
      //MoveInfo simplified = moveInfo.simplify(movementSimplificationParams.newWithMaxDoc(context.getMaxDoc()));
      //System.out.println("MOVE_INFO_SIMPLIFIED: "+simplified.toJSON().toPrettyString());
      return new IreneScorer(evaluator, simplified.mover);
    }
    return new IreneScorer(evaluator, createMover(context));
  }


  @Override
  public String toString(String field) {
    return query.toString();
  }

  public IreneCountStatistics collectCountStatistics(StatsKey statsKey) {
    //System.err.println("ExprModel::collectCountStatistics "+statsKey+" "+ Parameters.parseArray("throwOnCachedStats", throwOnCachedStats, "allowCachedStats", allowCachedStats));
    if(throwOnCachedStats) {
      throw new RuntimeException("Attempt to recursively get stats!");
    }
    if(allowCachedStats) {
      return index.collectCountStatistics(statsKey);
    } else {
      return index.computeCountStatistics(statsKey.getAsQExpr());
    }
  }
}
