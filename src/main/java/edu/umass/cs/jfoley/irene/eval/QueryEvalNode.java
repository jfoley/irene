package edu.umass.cs.jfoley.irene.eval;

import ciir.jfoley.chai.collections.util.ListFns;
import edu.umass.cs.jfoley.irene.movement.MoveInfo;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

/**
 * @author jfoley
 */
public interface QueryEvalNode {
  default boolean hasNoMatches() { return false; }
  float score(int doc) throws IOException;
  int count(int doc) throws IOException;
  Explanation explain(int doc) throws IOException;

  /**
   * For MaxScore and WAND sort of algorithms: notify the topmost node that a certain score is now required.
   * @param score the score of the worst element in the heap.
   */
  default void setMinimumRequiredScore(float score) { }

  /**
   *  Fine-grained matching control.
   */
  boolean matches(int id) throws IOException;

  /**
   *  Return a mover, as precise as possible, not sure? Just return
   */
  MoveInfo getMovementInfo();

  default MoveInfo createAndMover(List<? extends QueryEvalNode> iters) {
    if(iters.size() == 0) MoveInfo.empty();
    if(iters.size() == 1) return iters.get(0).getMovementInfo();
    return MoveInfo.and(ListFns.map(iters, QueryEvalNode::getMovementInfo));
  }
  default MoveInfo createOrMover(List<? extends QueryEvalNode> iters) {
      if(iters.size() == 0) return MoveInfo.empty();
      if(iters.size() == 1) return iters.get(0).getMovementInfo();
      return MoveInfo.or(ListFns.map(iters, QueryEvalNode::getMovementInfo));
  }

  /** Guess the number of hits, returning 0 if you're not sure. */
  long estimateDF();

  Comparator<QueryEvalNode> mostToLeastFrequent = (lhs, rhs) -> -Long.compare(lhs.estimateDF(), rhs.estimateDF());
  Comparator<QueryEvalNode> leastToMostFrequent = (lhs, rhs) -> Long.compare(lhs.estimateDF(), rhs.estimateDF());
}
