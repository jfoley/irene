package edu.umass.cs.jfoley.irene.expr;

import edu.umass.cs.jfoley.irene.eval.leaf.LeafEvalNode;
import edu.umass.cs.jfoley.irene.impl.DataNeeded;
import edu.umass.cs.jfoley.irene.impl.IreneTermInfo;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.NumericDocValues;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermContext;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * @author jfoley.
 */
public class QueryContext {
  public final IreneQueryModel exprModel;
  public static boolean sharedLeafQuery = false;
  final LeafReaderContext context;
  private final IndexSearcher searcher;
  public LeafCreator leafCreator;

  /** Cached lengths */
  private HashMap<String, NumericDocValues> lengthsCache = new HashMap<>();

  public QueryContext(IreneQueryModel exprModel, IndexSearcher searcher, LeafReaderContext context, boolean sharedLeafQuery) {
    this.context = context;
    this.searcher = searcher;
    this.exprModel = exprModel;
    assert(exprModel.index != null);
    leafCreator = (sharedLeafQuery ? new SharedLeafCreator(this) : new UnsharedLeafCreator(this));
  }

  public NumericDocValues getLengths(String field) {
    return lengthsCache.computeIfAbsent(field, (missing) -> {
      try {
        return context.reader().getNormValues(field);
      } catch (IOException e) {
        throw new RuntimeException("Couldn't find norm-lengths for field: "+missing, e);
      }
    });
  }

  public LeafEvalNode create(IreneTermInfo info) {
    try {
      return info.create(context, getLengths(info.getTerm().field()));
    } catch (IOException e) {
      throw new RuntimeException("Term Creation Error.", e);
    }
  }

  public IreneTermInfo getTermInfo(Term term, DataNeeded needed) {
    try {
      assert(exprModel.index != null);
      assert(term.field() != null);
      long vocabularySize = exprModel.index.getVocabularySize(term.field());
      TermContext ctx = TermContext.build(searcher.getTopReaderContext(), term);
      CollectionStatistics collStats = searcher.collectionStatistics(term.field());
      TermStatistics termStats = searcher.termStatistics(term, ctx);
      return new IreneTermInfo(needed, term, ctx, collStats, termStats, vocabularySize);
    } catch (IOException e) {
      throw new RuntimeException("Failed to pull term: "+term+" needed: "+needed+"!", e);
    }
  }


  public void withTermRequests(List<IreneQueryLanguage.TermRequest> termRequests) {
    leafCreator.withTermRequests(termRequests, searcher, context);
  }

  public List<DocIdSetIterator> getAllIterators() {
    return leafCreator.getAllIterators();
  }

  public void withScorerRequests(List<LuceneScorerRequest> requests) {
    leafCreator.withScorerRequests(requests);
  }

  public Scorer getScorer(LuceneScorerRequest queryInfo) {
    return leafCreator.getScorer(queryInfo);
  }

  public int getMaxDoc() {
    return context.reader().maxDoc();
  }
}
