package edu.umass.cs.jfoley.irene.processing;

import ciir.jfoley.chai.lang.DoubleFns;
import edu.umass.cs.jfoley.irene.impl.IreneCountStatistics;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.Terms;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.CollectorManager;
import org.apache.lucene.search.LeafCollector;
import org.apache.lucene.search.Scorer;

import java.io.IOException;
import java.util.Collection;

/**
 * @author jfoley
 */
public class StatisticsCollector implements CollectorManager<StatisticsCollector.LeafStatsCollector, IreneCountStatistics> {
  public final String field;
  public final Object randomLock = new Object();

  public StatisticsCollector(String field) {
    this.field = field;
  }

  @Override
  public LeafStatsCollector newCollector() throws IOException {
    return new LeafStatsCollector(field);
  }

  @Override
  public IreneCountStatistics reduce(Collection<LeafStatsCollector> collectors) throws IOException {
    IreneCountStatistics finalStats = new IreneCountStatistics();
    for (LeafStatsCollector collector : collectors) {
      finalStats.add(collector.leafStats);
    }
    return finalStats;
  }

  public static class LeafStatsCollector implements Collector {
    final String field;
    IreneCountStatistics leafStats = new IreneCountStatistics();

    public LeafStatsCollector(String field) {
      this.field = field;
    }

    @Override
    public LeafCollector getLeafCollector(LeafReaderContext context) throws IOException {

      // copy static stats immediately
      Terms fieldStats = context.reader().fields().terms(field);
      leafStats.totalDocumentCount = fieldStats.getDocCount();
      leafStats.collectionLength = fieldStats.getSumTotalTermFreq();

      // compute dynamic stats:
      return new LeafCollector() {
        Scorer scorer;

        @Override
        public void setScorer(Scorer scorer) throws IOException {
          this.scorer = scorer;
        }

        @Override
        public void collect(int doc) throws IOException {
          // we assume this is going to be a count...
          float score = scorer.score();
          int count = (int) score;
          assert (DoubleFns.equals(count, score, 0.00001)) : "Expected scorer from computeCountStatistics to always return an integer. Got " + score + " from doc=" + doc;
          leafStats.documentFrequency++;
          leafStats.collectionFrequency += count;
        }
      };
    }

    @Override
    public boolean needsScores() {
      return false;
    }
  }
}
