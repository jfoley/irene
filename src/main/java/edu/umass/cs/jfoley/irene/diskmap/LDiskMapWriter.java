package edu.umass.cs.jfoley.irene.diskmap;

import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;

/**
 * A byte[] -> byte[] mapping writer.
 * @author jfoley
 */
public class LDiskMapWriter implements Closeable {
  final Directory dir;
  final IndexWriter writer;

  enum WRITE_MODE {
    OVERWRITE, APPEND;

    private IndexWriterConfig.OpenMode getLuceneMode() {
      switch (this) {
        case OVERWRITE: return IndexWriterConfig.OpenMode.CREATE;
        case APPEND: return IndexWriterConfig.OpenMode.CREATE_OR_APPEND;
        default: throw new IllegalArgumentException("WRITE_MODE: "+this);
      }
    }
  }

  public LDiskMapWriter(File dir) throws IOException {
    this(dir, WRITE_MODE.OVERWRITE);
  }
  public LDiskMapWriter(File dir, WRITE_MODE mode) throws IOException {
    this(FSDirectory.open(dir.toPath()), mode);
  }

  public LDiskMapWriter(Directory dir, WRITE_MODE mode) throws IOException {
    this.dir = dir;
    final IndexWriterConfig cfg = new IndexWriterConfig(new WhitespaceAnalyzer());
    cfg.setOpenMode(mode.getLuceneMode());
    this.writer = new IndexWriter(dir, cfg);
  }

  public void put(byte[] id, byte[] value) throws IOException {
    Document doc = new Document();
    doc.add(new StringField("id", new BytesRef(id), Field.Store.NO));
    doc.add(new StoredField("value", value));
    writer.addDocument(doc);
  }

  @Override
  public void close() throws IOException {
    writer.commit();

    writer.close();
    dir.close();
  }
}
