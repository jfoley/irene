package edu.umass.cs.jfoley.irene.index;

import edu.umass.cs.jfoley.irene.IreneIndex;
import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;

/**
 * @author jfoley
 */
public class MergeIndex {
  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);
    try (IreneIndexer output = new IreneIndexer(argp.getString("output"))) {
      for (String input : argp.getAsList("input", String.class)) {
        System.err.println("#Adding: "+input);
        try (IreneIndex index = new IreneIndex(input)) {
          output.write(index);
        }
      }
    }
  }
}
