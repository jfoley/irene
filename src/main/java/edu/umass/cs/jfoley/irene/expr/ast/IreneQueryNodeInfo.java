package edu.umass.cs.jfoley.irene.expr.ast;

import edu.umass.cs.jfoley.irene.eval.QueryEvalNode;
import edu.umass.cs.jfoley.irene.expr.QExpr;
import edu.umass.cs.jfoley.irene.expr.types.InputTypeChecker;
import edu.umass.cs.jfoley.irene.expr.types.OperatorType;
import org.apache.lucene.search.Query;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author jfoley
 */
public interface IreneQueryNodeInfo {
  Set<String> getOperatorNames();
  String getCanonicalName();
  Class<? extends QueryEvalNode> getEvalNodeClass(QExpr input);
  InputTypeChecker getInputRequirements(QExpr input);
  OperatorType getOutputType(QExpr input);
  @Nullable default Query getLuceneQuery(QExpr input) { return null; }
  @Nullable default TermRequest requestedTerms(QExpr input) { return null; }

  class Simple implements IreneQueryNodeInfo {
    final String canonicalName;
    final HashSet<String> alternateNames;
    final Class<? extends QueryEvalNode> klazz;
    final InputTypeChecker typeChecker;
    final OperatorType outputType;

    public Simple(String canonicalName, Collection<? extends String> alternateNames, Class<? extends QueryEvalNode> klazz, InputTypeChecker typeChecker, OperatorType outputType) {
      this.canonicalName = canonicalName;
      this.alternateNames = new HashSet<>(alternateNames);
      this.klazz = klazz;
      this.typeChecker = typeChecker;
      this.outputType = outputType;
    }

    @Override
    public Set<String> getOperatorNames() {
      return null;
    }

    @Override
    public String getCanonicalName() {
      return null;
    }

    @Override public Class<? extends QueryEvalNode> getEvalNodeClass(QExpr input) { return klazz; }
    @Override public InputTypeChecker getInputRequirements(QExpr input) { return typeChecker; }
    @Override public OperatorType getOutputType(QExpr input) { return outputType; }
  }
}
